def avancer(direction, x, y):
    """
    Renvoie la nouvelle position de la balle une fois
    qu'elle a avancé. On rappelle que l'axe des ordonnées
    est orientés vers le bas en informatique.
    """
    if direction == "bas":
        y = y + 10
    elif direction == "gauche":
        x = x - 10
    elif direction == "droite":
        x = x + 10
    else:
        y = y - 10
    return(x,y)
    
