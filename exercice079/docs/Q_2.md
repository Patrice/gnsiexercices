    >>> from exercice import *

    >>> e = Noeud(Noeud(None, 3, None), '*', Noeud(None, 8, None))
    >>> expression_infixe(e)
    '(3*8)'
    >>> e = Noeud(Noeud(None, 7, None), '+', Noeud(None, 3, None))
    >>> expression_infixe(e)
    '(7+3)'

 
