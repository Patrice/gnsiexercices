Ce dossier est une base d'exercices de NSI (épreuves pratiques, écrites, TP) générées et utilisables grâce au projet [gnsi](https://forge.aeif.fr/patrice_le_borgne/gnsi). Voir la documentation pour comprendre l'usage qui en est fait.

Dans chaque dossier d'exercice, on trouve les sources markdown, les sources latex et des tests pour tester le code.

Personnellement j'utilise `emacs + org-roam` pour classer mes exercices et m'y retrouver. Je réfléchis à créer un référencement et un classement utilisables par tout le monde. Toutefois, en attendant, il faudra se contenter du fichier pdf qui contint tous les exercices de la base.
