\hypertarget{exercice}{%
\section{Exercice}\label{exercice}}

\emph{Cet exercice porte sur la programmation orientée objets, les tris,
les algorithmes gloutons, la récursivité et les assertions.}

\emph{Cet exercice est composé de trois parties dont les deux dernières
sont indépendantes entre elles.}

Dans cet exercice, l'entête des fonctions est décrit avec le type des
objets en paramètre et le type de l'objet renvoyé. Ainsi la fonction
puissance qui prend un paramètre flottant \texttt{x} et un entier
\texttt{n} puis qui renvoie le flottant \texttt{x**n}, a pour entête
\texttt{puissance(x:\ float,\ n:\ int)\ -\textgreater{}\ float}

Une entreprise transporte des marchandises. Elle souhaite maximiser son
profit en optimisant le remplissage de ses moyens de transport. On
considère qu'un moyen de transport est limité par son volume (exprimé en
litres). Chaque marchandise est caractérisée par son prix (en euros) et
son volume indivisible (en litres).

Supposons qu'on ait trois marchandises caractérisées par les couples
(prix, volume) suivants : \(m_1 = (100, 10)\), \(m_2 = (100, 10)\) et
\(m_3 = (250, 20)\). Si le moyen de transport peut encore charger 25
litres, il vaut mieux charger la marchandise numéro 3 qui rapporte 250 €
à l'entreprise plutôt que charger les marchandises numéros 2 et 3 qui
rapportent 200 € au total pour le même espace utilisé.

\bigskip

\textbf{Partie A -- Quelques outils}

\medskip

Nous souhaitons définir une classe \texttt{Marchandise} dont chaque
instance définit une marchandise possédant deux attributs entiers
\texttt{prix} et \texttt{volume}.

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Compléter le constructeur qui renvoie un objet \texttt{Marchandise}.
  Utiliser le mot-clé \texttt{assert} afin qu'une exception soit levée
  si le paramètre \texttt{v} n'est pas strictement positif.

On rappelle que si \texttt{condition} est une expression Python
booléenne s'évaluant à \texttt{True} ou \texttt{False}, l'instruction
\texttt{assert\ condition} déclenche une exception quand la condition
s'évalue à \texttt{False}.

\begin{Shaded}
\begin{Highlighting}[]
      \KeywordTok{class}\NormalTok{ Marchandise:}
          \KeywordTok{def} \FunctionTok{__init__}\NormalTok{(}\VariableTok{self}\NormalTok{, p: }\BuiltInTok{int}\NormalTok{, v: }\BuiltInTok{int}\NormalTok{) }\OperatorTok{->} \StringTok{'Marchandise'}\NormalTok{:}
\NormalTok{              ...}
\end{Highlighting}
\end{Shaded}
\end{enumerate}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{1}
\tightlist
\item
  Donner une instruction qui permet de créer une variable \texttt{m1}
  représentant une marchandise d'un volume de 7 litres coûtant 20 €.
\end{enumerate}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{2}
\tightlist
\item
  Proposer une méthode \texttt{ratio(self)\ -\textgreater{}\ float} qui
  renvoie le ratio prix/volume d'une marchandise.
\end{enumerate}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{3}
\tightlist
\item
  Proposer une fonction
  \texttt{prixListe(tab:\ list)\ -\textgreater{}\ int} qui renvoie le
  prix cumulé de l'ensemble des marchandises formant le tableau
  \texttt{tab}.
\end{enumerate}

\bigskip

\textbf{partie B -- Première approche de rangement}

\medskip

Le transporteur souhaite maximiser son profit. On considère que nous
avons les quatre marchandises définies par les couples (prix, volume)
suivants :

\begin{center}
  \(m_1 = (40, 20)\), \(m_2 = (210, 70)\), \(m_3 = (160, 40)\) et
  \(m_4 = (50, 50)\).
\end{center}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{4}
\tightlist
\item
  Préciser toutes les combinaisons de marchandises possibles si on ne
  dépasse pas un volume de 100 litres et le prix associé. En déduire la
  combinaison de marchandises qui maximise le prix.
\end{enumerate}

Une première méthode appelée \texttt{ChargementGlouton} consiste à trier
les marchandises dans l'ordre décroissant de leur prix volumique (ratio
prix/volume), puis transporter en priorité les marchandises avec le plus
grand prix volumique. Si une marchandise est trop volumineuse pour être
transportée, on essaie avec la marchandise ayant le prix volumique juste
inférieur, ce jusqu'à ce qu'aucune marchandise ne puisse rentrer. Ainsi,
en notant \texttt{v\_restant} le volume disponible et \(m_i\) la
\((i + 1)^e\) marchandise une fois les marchandises triées, l'algorithme
peut s'écrire

\bigskip

\begin{center}
  \begin{minipage}{.80\linewidth}
    \begin{Shaded}
      \begin{Highlighting}[]
        \NormalTok{ChargementGlouton}
        \NormalTok{n }\OperatorTok{=}\NormalTok{ nombre de marchandises}
        \NormalTok{POUR i ALLANT de }\DecValTok{0}\NormalTok{ à n}\DecValTok{-1}\NormalTok{ FAIRE}
        \OperatorTok{|}\NormalTok{ SI volume de m_i }\OperatorTok{<=}\NormalTok{ v_restant ALORS}
        \OperatorTok{|} \OperatorTok{|}\NormalTok{ charger m_i}
        \OperatorTok{|} \OperatorTok{|}\NormalTok{ v_restant }\OperatorTok{=}\NormalTok{ v_restant }\OperatorTok{-}\NormalTok{ volume de m_i}
        \NormalTok{TRANSPORTER le chargement prévu}
      \end{Highlighting}
    \end{Shaded}
  \end{minipage}
  
\end{center}

\bigskip


Le tri dans l'ordre décroissant des prix volumiques donne \(m_3\) ,
\(m_2\) , \(m_1\) , \(m_4\). Si le moyen de transport accepte 100 litres
de chargement, l'algorithme charge \(m_3\) et \(m_1\) pour un prix de
200 € (à comparer avec la combinaison trouvée précédemment pour
maximiser le prix).

Par la suite, on rappelle que dans l'implémentation Python, les
marchandises sont définies par des instances de la classe
\texttt{Marchandise}.

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{5}
\tightlist
\item
  On donne quelques qualificatifs : dichotomique, glouton, graphique,
  insertion, maximum, récursif, tri. Indiquer, sans justification, le
  qualificatif qui s'applique le mieux à l'algorithme précédent.
\end{enumerate}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{6}
\tightlist
\item
  Recopier et compléter la fonction
  \texttt{tri(tab:\ list)\ -\textgreater{}\ None} ci-dessous afin
  qu'elle trie en place un tableau contenant des objets de type
  Marchandise selon l'ordre décroissant des ratios. Ainsi,
  \texttt{tab{[}0{]}} doit contenir la marchandise avec le plus haut
  ratio prix/volume après l'appel \texttt{tri(tab)}.


\begin{Shaded}
\begin{Highlighting}[]
    \KeywordTok{def}\NormalTok{ tri(tab: }\BuiltInTok{list}\NormalTok{) }\OperatorTok{->} \VariableTok{None}\NormalTok{:}
\NormalTok{        n }\OperatorTok{=} \BuiltInTok{len}\NormalTok{(tab)}
        \ControlFlowTok{for}\NormalTok{ i }\KeywordTok{in} \BuiltInTok{range}\NormalTok{(}\DecValTok{1}\NormalTok{, n):}
\NormalTok{            marchandise }\OperatorTok{=}\NormalTok{ tab[i]}
\NormalTok{            j }\OperatorTok{=}\NormalTok{ i}\DecValTok{-1}
            \ControlFlowTok{while}\NormalTok{ ... }\KeywordTok{and}\NormalTok{ ... }\OperatorTok{>}\NormalTok{ ... :}
\NormalTok{                tab[j}\OperatorTok{+}\DecValTok{1}\NormalTok{] }\OperatorTok{=}\NormalTok{ ...}
\NormalTok{                j }\OperatorTok{=}\NormalTok{ ...}
\NormalTok{            tab[j}\OperatorTok{+}\DecValTok{1}\NormalTok{] }\OperatorTok{=}\NormalTok{ marchandise}
\end{Highlighting}
\end{Shaded}

\end{enumerate}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{7}
\tightlist
\item
  Sans justifier, préciser le nom de ce tri, ainsi que son coût temporel
  dans le pire des cas (constant, logarithmique, linéaire,
  quasi-linéaire (\(n \log_2 n\)), quadratique, cubique ou exponentiel).
\end{enumerate}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{8}
\tightlist
\item
  Recopier et compléter la fonction charge suivante qui applique
  l'algorithme \texttt{ChargementGlouton} décrit plus haut.

\begin{Shaded}
\begin{Highlighting}[]
    \KeywordTok{def}\NormalTok{ charge(tab: }\BuiltInTok{list}\NormalTok{, volume: }\BuiltInTok{int}\NormalTok{) }\OperatorTok{->} \BuiltInTok{list}\NormalTok{:}
\NormalTok{        tri(tab)}
\NormalTok{        chargement }\OperatorTok{=}\NormalTok{ []}
\NormalTok{        n }\OperatorTok{=} \BuiltInTok{len}\NormalTok{(tab)}
        \ControlFlowTok{for}\NormalTok{ ...}
            \ControlFlowTok{if}\NormalTok{ ...}
\NormalTok{                ...}
\NormalTok{                ...}
        \ControlFlowTok{return}\NormalTok{ ...}
\end{Highlighting}
\end{Shaded}
\end{enumerate}

\bigskip

\textbf{Partie C -- Rangement optimisé par récursivité}

\medskip

L'algorithme précédent ne renvoie pas toujours une solution optimale. On
peut donc suivre un algorithme récursif. On note \texttt{n} le nombre de
marchandises et on souhaite implémenter la fonction récursive
\texttt{chargeOptimale} d'entête :

\texttt{chargeOptimale(tab:\ list,\ v\_restant:\ int,\ i:\ int)\ -\textgreater{}\ list}

Un appel à cette fonction doit permettre de calculer la charge optimale
pour un transport de volume \texttt{v\_restant} utilisant les
marchandises à partir de l'indice \texttt{i} :

\begin{itemize}
\tightlist
\item
  si \(i \geqslant n\), toutes les marchandises ont été essayées et il
  n'en reste plus d'autres disponibles. L'appel récursif renvoie la
  liste vide ;
\item
  si \(i < n\) et la marchandise d'indice \texttt{i} est de volume
  strictement supérieur au volume restant, l'appel récursif renvoie le
  résultat de l'appel effectué avec le même volume restant mais avec la
  marchandise suivante, c'est-à-dire
  \texttt{chargeOptimale(tab,\ v\_restant,\ i+1)} ;
\item
  si \(i < n\) et la marchandise d'indice \texttt{i} est de volume
  inférieur ou égal au volume restant, il existe deux options possibles
  :

  \begin{itemize}
  \tightlist
  \item
    \emph{Option 1} : soit on utilise la marchandise \texttt{i}, auquel
    cas le chargement contiendra cette marchandise et celles du résultat
    de l'appel récursif à partir de la prochaine marchandise et d'un
    volume restant strictement inférieur,
  \item
    \emph{Option 2} : soit on n'utilise pas la marchandise \texttt{i},
    auquel cas le chargement sera le résultat de l'appel récursif avec
    le même volume restant mais à partir de la marchandise suivante.
  \end{itemize}
\end{itemize}

On garde l'option de chargement qui maximise le prix transporté.

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{9}
\tightlist
\item
  Compléter le code de la fonction chargeOptimale dont le principe a été
  décrit ci-avant.

\begin{Shaded}
\begin{Highlighting}[]
\KeywordTok{def}\NormalTok{ chargeOptimale(tab: }\BuiltInTok{list}\NormalTok{, v_restant: }\BuiltInTok{int}\NormalTok{, i: }\BuiltInTok{int}\NormalTok{) }\OperatorTok{->} \BuiltInTok{list}\NormalTok{:}
    \ControlFlowTok{if}\NormalTok{ i}\OperatorTok{>=}\NormalTok{...:}
        \ControlFlowTok{return}\NormalTok{ ...}
    \ControlFlowTok{else}\NormalTok{:}
        \ControlFlowTok{if}\NormalTok{ tab[i].volume }\OperatorTok{>}\NormalTok{ v_restant:}
            \ControlFlowTok{return}\NormalTok{ chargeOptimale(tab, v_restant, i}\OperatorTok{+}\DecValTok{1}\NormalTok{)}
        \ControlFlowTok{else}\NormalTok{:}
\NormalTok{            option1 }\OperatorTok{=}\NormalTok{ chargeOptimale(tab, ..., ...)}
\NormalTok{            option2 }\OperatorTok{=}\NormalTok{ [tab[i]] }\OperatorTok{+}\NormalTok{ chargeOptimale(tab, ..., ...)}
            \ControlFlowTok{if}\NormalTok{ prixListe(option1) }\OperatorTok{>}\NormalTok{ prixListe(option2):}
                \ControlFlowTok{return}\NormalTok{ ...}
            \ControlFlowTok{else}\NormalTok{:}
                \ControlFlowTok{return}\NormalTok{ ...}
\end{Highlighting}
\end{Shaded}
\end{enumerate}
