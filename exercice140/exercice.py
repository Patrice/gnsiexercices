class Marchandise:
    def __init__(self, p, v):
        assert v > 0
        self.prix = p
        self.volume = v

    def ratio(self):
        return self.prix / self.volume

        
m1 = Marchandise(20, 7)


def prixListe(tab):
    total = 0
    for m in tab:
        total = total + m.prix
    return total


# Q_5 : La combinaison qui maximise le gain est :
# "a" : m1
# "b" : m1 + m2
# "c" : m1 + m3
# "d" : m4 + m3
# Choississez la bonne réponse.

reponse_Q_5 = "b"

# Q_6 : On donne quelques qualificatifs : dichotomique, glouton, graphique, insertion, maximum, récursif, tri. Indiquer, sans justification, le qualificatif qui s’applique le mieux à l’algorithme précédent.

reponse_Q_6 = "glouton"

def tri(tab: list) -> None:
    n = len(tab)
    for i in range(1, n):
        marchandise = tab[i]
        j = i-1
        while j >= 0 and marchandise.ratio() > tab[j].ratio() :
            tab[j+1] = tab[j]
            j = j - 1
        tab[j+1] = marchandise


# Q_8_a : Le nom du tri est ??
# Choisir parmi les propositions suivantes
# 'a' : sélection
# 'b' : insertion
# 'c' : dichotomique
# 'd' : glouton
# 'e' : fusion

reponse_Q_8_a = 'b'

# Q_8_b :le côut du tri est ???
# Choisir parmi les propositions suivantes :
# 'a' : constant
# 'b' : logarithmique
# 'c' : linéaire
# 'd' : quasi-linéaire
# 'e' : quadratique
# 'f' : cubique
# 'g' : exponentiel

reponse_Q_8_b = 'e'

def charge(tab: list, volume: int) -> list:
    tri(tab)
    chargement = []
    n = len(tab)
    for i in range(n):
        if volume > 0 and tab[i].volume <= volume:
            chargement.append(tab[i])
            volume = volume - tab[i].volume
    return chargement


def chargeOptimale(tab: list, v_restant: int, i: int) -> list:
    if i>=len(tab):
        return []
    else:
        if tab[i].volume > v_restant:
            return chargeOptimale(tab, v_restant, i+1)
        else:
            option1 = chargeOptimale(tab, v_restant, i+1)
            option2 = [tab[i]] + chargeOptimale(tab, v_restant - tab[i].volume, i+1)
            if prixListe(option1) > prixListe(option2):
                return option1
            else:
                return option2
