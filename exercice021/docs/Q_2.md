    >>> from exercice import *

    >>> recherche("AATC", "GTA")
    False
    >>> recherche("GTACAAATCTTGCC", "GTACAAATCTTGCC")
    True
    >>> recherche("GTACAAATCTTGCCT", "GTACAAATCTTGCC")
    False

    >>> recherche("AATC", "GTA")
    False
    >>> recherche("GTACAAATCTTGCC", "GTACAAATCTTGCC")
    True
    >>> recherche("GTACAAATCTTGCCT", "GTACAAATCTTGCC")
    False

 
