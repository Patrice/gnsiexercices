    >>> from exercice import *

    >>> grille = [['f', 'e', 'e', 'o'], ['m', 'u', 'r', 'l'], ['n', 'e', 'i', 'c'], ['i', 'o', 'c', 'e']]
    >>> ruzzle = Ruzzle(grille)
    >>> masque = ruzzle.generer_masque()
    >>> possibilites = ruzzle.liste_possibilites_avec_position_depart(0, 0, masque, dico)
    >>> 'fee' in possibilites
    True
    >>> 'feerie' in possibilites
    True
    >>> 'ee' in possibilites
    False

 
