~~~ {.hidden .meta}
classe : terminale
type : écrit
chapitre : réseaux
thème : réseaux
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========

Cet exercice porte sur les réseaux et le routage.
Les deux parties de cet exercice sont indépendantes.

**Partie A**

À son domicile, une élève remarque que l’adresse IP de l’interface réseau (carte wifi)
de son ordinateur personnel est 192.168.1.14 avec le masque 255.255.255.0.

Pour chacune des questions ci-dessous, recopier la seule bonne réponse.
1. Sous Unix, quelle instruction en ligne de commande a pu délivrer
   cette information ?
   - `ifconfig`
   - `ping`
   - `ps`
   - `ls`

~~~ {.python .hidden .test .amc file="Q_A_1.md" bareme="1"}
>>> reponse_A_1
'a'
~~~


2. Parmi les protocoles ci-dessous, quel est celui qui a permis
   d’attribuer automatiquement cette adresse IP ?  
   - DNS 
   - DHCP 
   - TCP 
   - HTTP

~~~ {.python .hidden .test .amc file="Q_A_2.md" bareme="1"}
>>> reponse_A_2
'b'
~~~

3. Parmi les adresses IP ci-dessous, quelle est la seule possible pour
   un autre appareil connecté au même réseau ?
   - 192.168.0.14
   - 192.168.0.1
   - 192.168.1.1
   - 192.168.1.255

~~~ {.python .hidden .test .amc file="Q_A_3.md" bareme="1"}
>>> reponse_A_3
'c'
~~~

4. Toujours à son domicile, l’élève consulte une page web qui prétend
   que l’adresse IP de son ordinateur est 88.168.10.210.
   - C’est une fausse information car son adresse IP est 192.168.1.14 .
   - C’est sûrement faux car seul le fournisseur d’accès peut avoir
     connaissance de cette information.
   - C’est possible et cette adresse serait celle de la box vers Internet.
   - C’est possible, mais cela signifierait que l’ordinateur est
     infecté par un malware.

~~~ {.python .hidden .test .amc file="Q_A_4.md" bareme="1"}
>>> reponse_A_4
'c'
~~~


5. Est-il possible qu’un ordinateur connecté au réseau du lycée
   possède la même adresse IP que l'élève à son domicile ?
   - Oui, à condition que les connexions n’aient pas lieu au même moment.
   - Oui, car les adresses 192.168.x.x ne sont pas routées sur Internet.
   - Oui, à condition d’utiliser un VPN.
   - Non, car deux machines sont identifiées de manière unique par
     leur adresse IP.

~~~ {.python .hidden .test .amc file="Q_A_5.md" bareme="1"}
>>> reponse_A_5
'b'
~~~


**Partie B**

On représente ci-dessous un réseau dans lequel `R1`, `R2`, `R3`, `R4`,
`R5`, `R6`, `R7` et `R8` sont des routeurs. Le réseau local `L1` est
relié au routeur `R1` et le réseau local `L2` au routeur `R8`.

L1 --- R1 --- R2
       | \    | 
       |  \   | 
       |   \  | 
       |    \ | 
       R3 --- R4 --- R5
       | \    | \    |
       |  \   |  \   |
       |   \  |   \  |
       |    \ |    \ |
       R6 --- R7 --- R8 --- L2


Les liaisons sont de trois types :
• Eth : Ethernet, dont la bande passante est de 10 Mb/s ;
• V1 : VDSL, dont la bande passante est de 50 Mb/s ;
• V2 : VDSL2, dont la bande passante est de 100 Mb/s.
On rappelle que la bande passante d’une liaison est la quantité d’information qui peut être
transmise en bits/s.

Le tableau ci-dessous précise les types des liaisons entre les routeurs.

|---------+-------+-------+-------+-------+-------+-------|
| Liaison | R1-R2 | R1-R3 | R1-R4 | R2-R4 | R3-R4 | R3-R6 |
|---------+-------+-------+-------+-------+-------+-------|
| Type    | Eth   | V2    | Eth   | V2    | Eth   | V2    |
|---------+-------+-------+-------+-------+-------+-------|


|---------+-------+-------+-------+-------+-------+-------+-------|
| Liaison | R3-R7 | R4-R5 | R4-R7 | R4-R8 | R5-R8 | R6-R7 | R7-R8 |
|---------+-------+-------+-------+-------+-------+-------+-------|
| Type    | Eth   | V1    | V2    | Eth   | V1    | V2    | Eth   |
|---------+-------+-------+-------+-------+-------+-------+-------|

Pour tenir compte du débit des liaisons, on décide d’utiliser le
protocole OSPF (distance liée au coût minimal des liaisons) pour
effectuer le routage.  Le coût $C$ d’une liaison est donné par la
formule : $C = \frac{10^9}{BP} où est la bande passante de la liaison en bits/s. 

1. Vérifier que le coût d’une liaison VDSL est égal à 20.

~~~ {.python .hidden .amc file="Q_B_1.md" bareme="1"}
>>> pass
~~~


2. a. Recopier la table du routeur `R1` sur votre copie en inscrivant
   les coûts des liaisons.

|---------+------|
| Routeur | Coût |
|---------+------|
| R2      |      |
|---------+------|
| R3      |      |
|---------+------|
| R4      |      |
|---------+------|
| R5      |      |
|---------+------|
| R6      |      |
|---------+------|
| R7      |      |
|---------+------|
| R8      |      |
|---------+------|


~~~ {.python .hidden .test .amc file="Q_B_2_a.md" bareme="1"}
>>> R1
[('R2', 50), ('R3', 10), ('R4', 40), ('R5', 60), ('R6', 20), ('R7', 30), ('R8', 80)]
~~~

   b. Déterminer le chemin parcouru par un paquet partant du réseau L1
   et arrivant au réseau L2, en utilisant le protocole OSPF.

~~~ {.python .hidden .test .amc file="Q_B_2_b.md" bareme="1"}
>>> chemin
'L1-R1-R3-R6-R7-R4-R5-R8-L2'
~~~

   c. La liaison R1-R4 est remplacée par une liaison de type ADSL avec
   une bande passante intermédiaire entre celles de type Ethernet et
   VDSL.

   Quel devrait être le coût maximal de cette liaison pour que des
   paquets issus du réseau L1 à destination du réseau L2 transitent
   par celle-ci ? En déduire la bande passante minimale de cette liaison.


~~~ {.python .hidden .test .amc file="Q_B_2_c.md" bareme="1"}
>>> cout_max
40
~~~

