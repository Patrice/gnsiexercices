~~~ {.hidden .meta}
classe : terminale
type : oral1
chapitre : graphes
thème : parcours
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========

On considère dans cet exercice un graphe orienté représenté sous forme de listes d’adjacence.

On suppose que les sommets sont numérotés de `0` à `n-1`.

Par exemple, le graphe suivant :

~~~ {.dot}
digraph G {
0 -> 1, 2;
1 -> 2;
2 -> 0;
3 -> 0;
}
~~~
![image](graphe_exo_130.png)

est représenté par la liste d’adjacence suivante :

~~~ {.python}
adj = [[1, 2], [2], [0], [0]]
~~~

Écrire une fonction `voisins_entrants(adj, x)` qui prend en paramètre le graphe
donné sous forme de liste d’adjacence et qui renvoie une liste contenant les voisins entrants
du sommet `x`, c’est-à-dire les sommets `y` tels qu’il existe une arête de `y` vers `x`.

Exemples :

~~~ {.python .amc file="Q_1.md" bareme="2"}
>>> voisins_entrants([[1, 2], [2], [0], [0]], 0)
[2, 3]
>>> voisins_entrants([[1, 2], [2], [0], [0]], 1)
[0]
~~~

~~~ {.python .hidden .test file="Q_1.md" bareme="1"}
>>> voisins_entrants([[1, 2], [2], [0], [0]], 0)
[2, 3]
>>> voisins_entrants([[1, 2], [2], [0], [0]], 1)
[0]
~~~

~~~ {.python .hidden .test file="Q_2.md" bareme="1"}
>>> voisins_entrants([[1, 2], [2], [0], [0]], 2)
[0, 1]
>>> voisins_entrants([[1, 2], [2], [0], [0]], 3)
[]
~~~
