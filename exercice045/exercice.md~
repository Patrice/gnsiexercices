~~~ {.hidden .meta}
classe : terminale
type : écrit
chapitre : poo
thème : codage de César

~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
==========

---
title: Chiffrement de César
author: Vincent-Xavier Jumel
tags:
    - Prog-POO
---

Dans cet exercice, on étudie une méthode de chiffrement de chaines de caractères
alphabétiques. Pour des raisons historiques, cette méthode de chiffrement
est appelée « code de César ». On considère que les messages ne contiennent
que les lettres capitales de l'alphabet `"ABCDEFGHIJKLMNOPQRSTUVWXYZ"` et la
méthode de chiffrement utilise un nombre entier fixé appelé la clé de
chiffrement.

**1.** Soit la classe `CodeCesar` définie ci-dessous :

~~~ {.python}
class CodeCesar:
    def __init__(self, cle):
        self.cle = cle

    def decale(self, lettre):
        indice_1 = indice_capitale(lettre)
        indice_2 = indice_1 + self.cle
        if indice_2 >= 26:
            indice_2 = indice_2 - 26
        if indice_2 < 0:
            indice_2 = indice_2 + 26
        nouvelle_lettre = lettre_capitale(indice_2)
        return nouvelle_lettre
~~~

On dispose aussi des fonctions `indice_capitale(indice)` et
`lettre_capitale(lettre)` qui renvoient la lettre majuscule correspondant à
un indice donné et l'indice (la position d'une lettre) dans l'alphabet latin
usuel.


**Représenter** le résultat d'exécution du code Python suivant :

~~~ {.python .amc file="Q_1.md" bareme="2"}
>>> code = CodeCesar(3)
>>> code.decale('A')
...
>>> code.decale('X')
...
~~~

**2.** La méthode de chiffrement du « code de César » consiste à décaler les
lettres du message dans l'alphabet d'un nombre de rangs fixé par la clé.
Par exemple, avec la clé 3, toutes les lettres sont décalées de 3 rangs
vers la droite : le A devient le D, le B devient le E, etc.

**Ajouter** une méthode `chiffre(self, texte)` dans la classe `CodeCesar`
définie à la question précédente, qui reçoit en paramètre une chaîne de
caractères (le message à chiffrer) et qui renvoie une chaîne de
caractères (le message chiffré).

Cette méthode `chiffre(self, texte)` doit chiffrer la chaîne `texte` avec la
clé de l'objet de la classe `CodeCesar` qui a été instanciée.

Exemple :

~~~ {.python .test .amc file="Q_2.md" bareme="2"}
>>> code = CodeCesar(3)
>>> code.chiffre("NSI")
'QVL'
~~~

~~~ {.python .hidden .test file="Q_2_c.md" bareme="1"}
>>> code = CodeCesar(18)
>>> code.chiffre("INTEMPERIE")
'INTEMPERIE'

~~~



**3.** **Écrire** une fonction qui :
    * prend en argument la clef de chiffrement et le message à chiffrer ;
    * instancie un objet de la classe `CodeCesar` ;
    * renvoie le texte chiffré.




**4.** On ajoute la méthode `transforme(texte)` à la classe `CodeCesar` :

```python
def transforme(self, texte):
    self.cle = -self.cle
    message = self.cryptage(texte)
    self.cle = -self.cle
    return message
```
On exécute la ligne suivante dans une console `CodeCesar(10).transforme("PSX")`

**Que va-t-il s'afficher** ? **Expliquer** votre réponse.


??? success "Réponse"
    Le message `'FIN'` va s'afficher sur la console.
    
    La méthode proposée permet de déchiffrer le message en appliquant la
    transformation opposée.


