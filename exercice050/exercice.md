~~~ {.hidden .meta}
classe : première
type : écrit
chapitre : structures linéaires
thème : listes de listes
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
>>> g2 = [[0, 2, 0, 0, 0, 0, 0],
...       [0, 2, 0, 0, 0, 0, 0],
...       [0, 2, 0, 0, 0, 0, 0],
...       [0, 1, 0, 1, 0, 0, 0],
...       [0, 1, 1, 1, 0, 0, 0],
...       [2, 1, 1, 2, 2, 2, 0]]
~~~


Exercice
==========

On souhaite développer un jeu de puissance 4. Le but du jeu est d'aligner une suite de 4 pions de même couleur sur une grille comptant 6 rangées et 7 colonnes. 

![puissance4](puissance4.png)

La grille correspondant à la zone de jeu sera implémentée par une liste de listes. Les espaces vides seront représentés par des 0. Les jetons du joueur seront représentés par de 1 et ceux de l'ordinateur par des 2. Ainsi, la situation de l'image précédente est représentée par la liste suivante :

~~~ {.python .all}
>>> g = [[0, 0, 0, 0, 0, 0, 0],
...      [0, 0, 0, 0, 0, 0, 0],
...      [0, 1, 0, 0, 0, 0, 0],
...      [0, 1, 0, 1, 0, 0, 0],
...      [0, 1, 0, 2, 0, 0, 0],
...      [2, 1, 2, 1, 2, 2, 2]]
~~~

1. Écrire une fonction `derniere_ligne_vide(t, colonne)` qui renvoie l'indice de la première ligne vide dans la `colonne`. Si la colonne est pleine, la fonction doit renvoyer -1.

~~~ {.python .test file="Q_1.md" bareme="1"}
>>> derniere_ligne_vide(g, 0)
4
>>> derniere_ligne_vide(g, 1)
1
~~~

~~~ {.python .hidden .test file="Q_1_bis.md" bareme="1"}
>>> derniere_ligne_vide(g2, 1)
-1
>>> derniere_ligne_vide(g2, 6)
5
~~~

~~~ {.python .hidden .amc file="Q_1.md" bareme="2"}
>>> pass
~~~


2. Écrire une fonction `ajouter_pion_colonne(t, colonne, joueur)` qui ajoute, si possible, le pion du `joueur` à la `colonne` (un entier compris entre 0 et 6) de la grille `t`. La fonction renverra la ligne à laquelle a été ajouté le pion. Si la colonne est pleine, la fonction renvoie -1.

~~~ {.python .test file="Q_2.md" bareme="1"}
>>> g[4][0]
0
>>> ajouter_pion_colonne(g, 0, 2)
4
>>> g[4][0]
2
~~~
~~~ {.python .test file="Q_2_bis.md" bareme="1"}
>>> ajouter_pion_colonne(g, 1, 2)
1
>>> g[1][1]
2
>>> ajouter_pion_colonne(g, 1, 1)
0
>>> g[0][1]
1
>>> ajouter_pion_colonne(g, 1, 2)
-1
~~~

~~~ {.python .hidden .amc file="Q_2.md" bareme="2"}
>>> pass
~~~

3. Une fois que le joueur a joué, il faut vérifier si ce jeton crée un alignement de 4 jetons.

    Écrire une fonction `alignement_vertical(t, ligne, colonne, joueur)` qui teste si le `joueur` a réussi un alignement vertical avec le jeton qu'il vient de placer dans la grille `t` à l'emplacement `(colonne,ligne)`. La fonction doit renvoyer un booléen.

![Alignement vertical](puissance4AlignementVertical.png)

Dans ce cas précis, on doit avoir :

~~~ {.python .test file="Q_3.md" bareme="1"}
>>> alignement_vertical(g, 2, 1, 1)
True
~~~

~~~ {.python .hidden .test file="Q_3_bis.md" bareme="1"}
>>> alignement_vertical(g, 2, 1, 2)
False
>>> alignement_vertical(g2, 0, 1, 2)
False
~~~

~~~ {.python .hidden .test file="Q_3_ter.md" bareme="1"}
>>> alignement_vertical(g2, 3, 3, 1)
False
>>> alignement_vertical(g2, 3, 1, 1)
False
~~~

~~~ {.python .hidden .amc file="Q_3.md" bareme="3"}
>>> pass
~~~



4. Écrire une fonction `alignement_horizontal(t, ligne, colonne, joueur)` qui teste si le `joueur` a réussi un alignement horizontal avec le jeton qu'il vient de placer dans la grille `t` à l'emplacement `(colonne,ligne)`. La fonction doit renvoyer un booléen.

Si le dernier jeton ajouté est celui de la colonne 4, voila les situations qu'il faut tester.


![puissance_4_alignement_horizontal_1](puissance4AlignementHorizontal1.png)
![puissance_4_alignement_horizontal_2](puissance4AlignementHorizontal2.png)
![puissance_4_alignement_horizontal_3](puissance4AlignementHorizontal3.png)
![puissance_4_alignement_horizontal_4](puissance4AlignementHorizontal4.png)


Dans ce cas précis,

~~~ {.python .test file="Q_4.md" bareme="1"}
>>> alignement_horizontal(g, 5, 4, 2)
False
~~~

~~~ {.python .hidden .test file="Q_4_bis.md" bareme="1"}
>>> alignement_horizontal(g2, 5, 3, 2)
False
>>> ajouter_pion_colonne(g2, 6, 2)
5
>>> alignement_horizontal(g2, 5, 3, 2)
True
~~~

~~~ {.python .hidden .test file="Q_4_ter.md" bareme="1"}
>>> alignement_horizontal(g2, 5, 4, 2)
False
>>> ajouter_pion_colonne(g2, 6, 2)
5
>>> alignement_horizontal(g2, 5, 6, 2)
True
>>> alignement_horizontal(g2, 5, 6, 1)
False
~~~

~~~ {.python .hidden .amc file="Q_4.md" bareme="3"}
>>> pass
~~~


