def creer_grille_vide(nl=6, nc=7):
    """
    Crée une grille vide
    """
    return [ [0 for i in range(nc)] for j in range(nl)]

def afficher_grille(t):
    """
    Affiche la grille du puissance 4 avec des croix et des ronds.
    """
    for i in range(len(t)):
        ligne = "|"
        for j in range(len(t[0])):
            if  t[i][j] == 0:
                ligne += " |"
            elif t[i][j] == 1:
                ligne += "X|"
            else:
                ligne += "O|"
        print(ligne)
    print("-" * (len(t[0])*2 + 1))

def derniere_ligne_vide(t, colonne):
    """
    Renvoie la ligne où on peut placer la
    """
    assert 0 <= colonne < len(t[0])
    # Si la colonne est pleine, on retourne faux
    if t[0][colonne] != 0:
        return -1
    # On descend dans la colonne. Si on rencontre une valeur
    # non nulle, on renvoie la ligne précedente
    for i in range(len(t)):
        if t[i][colonne] != 0:
            return i-1
    # Si on arrive ici, c'est que l'on n'a rencontré que des 0.
    # Autrement dit la colonne est vide
    return len(t)-1

def ajouter_pion_colonne(t, colonne, joueur):
    """
    Ajoute le pion de joueur dans la colonne.
    Renvoie la ligne où le pion a été ajouté.
    Si la colonne était pleine, renvoie -1.
    """
    assert 0 <= colonne < len(t[0])    
    # On cherche la ligne où le pion va tomber
    profondeur = derniere_ligne_vide(t, colonne)
    # Si la ligne n'est pas pleine, on l'ajoute.
    if profondeur > -1:
        t[profondeur][colonne] = joueur
    return profondeur

def alignement_vertical(t, ligne, colonne, joueur):
    """
    Teste l'alignement vertical pour joueur 
    à partir de la case(ligne, colonne).
    """
    assert 0 <= ligne < len(t)
    assert 0 <= colonne < len(t[0])
    # S'il y a suffisament de pions dans la colonne
    if len(t) - 1 - ligne >= 3:
        # on descend dans la colonne
        for i in range(4):
            # on renvoie faux si on trouve un pion 
            # de la mauvaise couleur
            if t[ligne + i][colonne] != joueur:
                return False
        # si on arrive ici, c'est qu'on a trouvé 
        # 4 pions identiques
        return True
    # Il n'y a pas assez de pions.    
    return False


def alignement_horizontal(t, ligne, colonne, joueur):
    """
    Teste l'alignement vertical pour joueur 
    à partir de la case(ligne, colonne).
    """
    assert 0 <= ligne < len(t)
    assert 0 <= colonne < len(t[0])
    # On suppose qu'il n'y a pas d'alignement
    resultat_final = False
    resultat_partiel = False
    # On a 4 positions à tester
    for i in range(-3, 1):
        # On fait le test uniquement si la
        # première case est dans la grille
        # et s'il y a au moins 4 pions
        if 0<= colonne+i < len(t[0])-3:
            # On suppose que la série testée est complète
            resultat_partiel = True
            for j in range(4):
                # Si on trouve un pion de la mauvaise couleur
                # c'est qu'il n'y a pas d'alignement
                if t[ligne][colonne+i+j] != joueur:
                    resultat_partiel = False
            # Si dans les tests effectués, on a une série de 4
            # la variable finale deviendra True également
            # grâce à la ligne suivante.
        resultat_final = resultat_final or resultat_partiel
    return resultat_final
