\~\~\~

Exercice
========

On souhaite développer un jeu de puissance 4. Le but du jeu est
d'aligner une suite de 4 pions de même couleur sur une grille comptant 6
rangées et 7 colonnes.

![puissance4](puissance4.png)

La grille correspondant à la zone de jeu sera implémentée par une liste
de listes. Les espaces vides seront représentés par des 0. Les jetons du
joueur seront représentés par de 1 et ceux de l'ordinateur par des 2.
Ainsi, la situation de l'image précédente est représentée par la liste
suivante :

``` {.python}
>>> g = [[0, 0, 0, 0, 0, 0, 0],
...      [0, 0, 0, 0, 0, 0, 0],
...      [0, 1, 0, 0, 0, 0, 0],
...      [0, 1, 0, 1, 0, 0, 0],
...      [0, 1, 0, 2, 0, 0, 0],
...      [2, 1, 2, 1, 2, 2, 2]]
```

1.  Écrire une fonction `derniere_ligne_vide(t, colonne)` qui renvoie
    l'indice de la première ligne vide dans la `colonne`. Si la colonne
    est pleine, la fonction doit renvoyer -1.

``` {.python}
>>> derniere_ligne_vide(g, 0)
4
>>> derniere_ligne_vide(g, 1)
1
```

``` {.python}
>>> derniere_ligne_vide(g2, 1)
-1
>>> derniere_ligne_vide(g2, 6)
5
```

2.  Écrire une fonction `ajouter_pion_colonne(t, colonne, joueur)` qui
    ajoute, si possible, le pion du `joueur` à la `colonne` (un entier
    compris entre 0 et 6) de la grille `t`. La fonction renverra la
    ligne à laquelle a été ajouté le pion. Si la colonne est pleine, la
    fonction renvoie -1.

``` {.python}
>>> g[4][0]
0
>>> ajouter_pion_colonne(g, 0, 2)
4
>>> g[4][0]
2
```

``` {.python}
>>> ajouter_pion_colonne(g, 1, 2)
1
>>> g[1][1]
2
>>> ajouter_pion_colonne(g, 1, 1)
0
>>> g[0][1]
1
>>> ajouter_pion_colonne(g, 1, 2)
-1
```

3.  Une fois que le joueur a joué, il faut vérifier si ce jeton crée un
    alignement de 4 jetons.

    Écrire une fonction `alignement_vertical(t, ligne, colonne, joueur)`
    qui teste si le `joueur` a réussi un alignement vertical avec le
    jeton qu'il vient de placer dans la grille `t` à l'emplacement
    `(colonne,ligne)`. La fonction doit renvoyer un booléen.

![Alignement vertical](puissance4AlignementVertical.png)

Dans ce cas précis, on doit avoir :

``` {.python}
>>> alignement_vertical(g, 2, 1, 1)
True
```

4.  Écrire une fonction
    `alignement_horizontal(t, ligne, colonne, joueur)` qui teste si le
    `joueur` a réussi un alignement horizontal avec le jeton qu'il vient
    de placer dans la grille `t` à l'emplacement `(colonne,ligne)`. La
    fonction doit renvoyer un booléen.

Si le dernier jeton ajouté est celui de la colonne 4, voila les
situations qu'il faut tester.

![puissance\_4\_alignement\_horizontal\_1](puissance4AlignementHorizontal1.png)
![puissance\_4\_alignement\_horizontal\_2](puissance4AlignementHorizontal2.png)
![puissance\_4\_alignement\_horizontal\_3](puissance4AlignementHorizontal3.png)
![puissance\_4\_alignement\_horizontal\_4](puissance4AlignementHorizontal4.png)

Dans ce cas précis,

``` {.python}
>>> alignement_horizontal(g, 5, 4, 2)
False
```
