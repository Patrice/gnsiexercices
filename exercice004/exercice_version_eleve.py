########### Ne pas modifier ############

class Arbre:
    def __init__(self, valeur = None,  arbre_gauche = None, arbre_droit = None):
        "Classe  arbre qui correspond plutôt à un noeud"
        self.valeur = valeur
        self.arbre_gauche = arbre_gauche
        self.arbre_droit = arbre_droit

def racine(arb):
    return arb.valeur

def est_vide(arb):
    return arb == None or arb.valeur == None

def gauche(arb):
    return arb.arbre_gauche

def droit(arb):
    return arb.arbre_droit

lea3 = Arbre("Lea", Arbre("Marc"), Arbre("Lea"))
theo3 = Arbre("Theo", Arbre("Claire"), Arbre("Theo"))
louis3 = Arbre("Louis", Arbre("Marie"), Arbre("Louis"))
anne3 = Arbre("Anne", Arbre("Anne"), Arbre("Kevin"))
lea2 = Arbre("Lea", lea3, theo3)
louis2 = Arbre("Louis", louis3, anne3)
B = Arbre("Lea", lea2, louis2)

##########################################
## Implémenter les fonctions ci-dessous ##
##########################################


def vainqueur(arb):
    pass

def finale(arb):
    pass 

def occurrences(arb, nom):
    pass

def a_gagne(arb, nom):
    pass

def nombre_matchs(arb, nom):
    pass

def liste_joueurs ( arb ):
    """ arbre_competition -> tableau """
    if est_vide ( arb ):
        return ...
    elif ... and ... :
        return [ racine ( arb )]
    else :
        return ...+ liste_joueurs ( droit ( arb ))
       
        
