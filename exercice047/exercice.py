T = [[4, 1, 1, 3], [2, 0, 2, 1], [3, 1, 5, 1]]

def somme_max(l, i, j):
    if (i,j)==(0,0):
        return l[0][0]
    elif i == 0:
        return l[0][j] + somme_max(l, 0, j-1)
    elif j == 0:
        return l[i][0] + somme_max(l, i-1, 0)
    else:
        return l[i][j] + max(somme_max(l, i-1, j), somme_max(l, i, j-1))
        
print(somme_max(T, 2, 3))