    >>> from exercice import *
    >>> j4 = Joueur("Albert")
    >>> j4.main = [Carte(5), Carte(7), Carte(75), Carte(77),  Carte(88),  Carte(101)]
    >>> j4.score = 12
    >>> j5 = Joueur("Claire")

    >>> j5.prendre_cartes([Carte(8), Carte(2), Carte(47), Carte(45)])
    >>> j5.main
    [Carte n°2 - 1 TdB, Carte n°8 - 1 TdB, Carte n°45 - 2 TdB, Carte n°47 - 1 TdB]

 
