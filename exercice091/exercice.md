~~~ {.hidden .meta}
classe : terminale
type : écrit
chapitre : poo
thème : listes
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
>>> j4 = Joueur("Albert")
>>> j4.main = [Carte(5), Carte(7), Carte(75), Carte(77),  Carte(88),  Carte(101)]
>>> j4.score = 12
>>> j5 = Joueur("Claire")
~~~

Exercice
========

La classe `Joueur` gère un joueur qui sera caractérisé par son nom, sa main et son score.

,--------------------------------------------------------------------------.
| Joueur                                                                   |
|--------------------------------------------------------------------------|
| + nom : string                                                           |
| + main : une liste de cartes.                                            |
| + score : le nombre de têtes de bœufs récupérees lors de la partie.      |
|                                                                          |
| +__init__(self, nom="Vous"): initialise le joueur avec un nom par défaut |
|     une main vide ([]) et un score nul.                                  |
| + prendre_cartes(self, l) : ajoute la liste de cartes à la main. La main |
|     doit être triée. On vérifie qu'il s'agit bien d'une liste de cartes. |
| + get_score(self) : renvoie le score.                                    |
| + a_des_cartes(self) : renvoie True si le joueur a encore des cartes.    |
| + est_carte_valable(self, n): Pour la version texte. Vérifie si n est le |
|      numéro d'une carte.                                                 |
| + poser_carte(n) : renvoie la carte de valeur n et l'enlève de la main.  |
|     Si la carte de valeur n n'est pas dans la main, une AssertionError   |
|     est levée.                                                           |
| + poser_carte_au_hasard(self) : renvoie une carte au hasard. Utile pour  |
|     pour faire jouer les autres joueurs.                                 |
| + ajouter_penalites(self, l) : ajoute les nombres de têtes de bœufs      |
|     de la liste l au score.                                              |
| + get_main(self) : renvoie la liste des cartes séparées par un tiret     |
| + __str__(self) : renvoie le nom du joueur et son score                  |
`--------------------------------------------------------------------------'

1. On crée un joueur. On vérifie qu'il n'a pas de carte et que son score est vierge.

~~~ {.python .test .amc file="Q_1.md" bareme="1"}
	>>> j = Joueur('Bob')
    >>> j.nom
    'Bob'
    >>> j.main
    []
    >>> j.score
    0
~~~

~~~ {.python .hidden .test file="Q_1_bis.md" bareme="1"}
>>> j5.nom
'Claire'
>>> j5.main
[]
>>> j5.score
0
~~~

2. La méthode `prendre_cartes()` sera utilisée lors de la distribution
   des cartes. Il faut vérifier qu'il n'y a que des cartes dans la
   liste.

~~~ {.python .test .amc file="Q_2.md" bareme="1"}
	>>> j = Joueur('Bob')
	>>> j.prendre_cartes([3, 10, 51])
    Traceback (most recent call last):
    ...
    AssertionError
    >>> j.prendre_cartes([Carte(3), Carte(10), 51])
    Traceback (most recent call last):
    ...
    AssertionError
    >>> j.prendre_cartes([Carte(3), Carte(10), Carte(51)])
    >>> j.main
    [Carte n°3 - 1 TdB, Carte n°10 - 3 TdB, Carte n°51 - 1 TdB]
~~~

~~~ {.python .hidden .test file="Q_2_bis.md" bareme="1"}
>>> j5.prendre_cartes([Carte(8), Carte(2), Carte(47), Carte(45)])
>>> j5.main
[Carte n°2 - 1 TdB, Carte n°8 - 1 TdB, Carte n°45 - 2 TdB, Carte n°47 - 1 TdB]
~~~



3. La méthode `get_score(self)` renvoie le score du joueur.

~~~ {.python .test .amc file="Q_3.md" bareme="1"}
	>>> j = Joueur('Bob')
	>>> j.prendre_cartes([Carte(3), Carte(10), Carte(51)])
	>>> j.get_score()
	0
	>>> j2 = Joueur("Roger")
	>>> j2.prendre_cartes([Carte(1), Carte(20), Carte(30), Carte(55)])
	>>> j2.get_score()
	0
~~~

~~~ {.python .hidden .test file="Q_3_bis.md" bareme="1"}
>>> j4.get_score()
12
>>> j5.get_score()
0
~~~



4. On teste si les joueurs ont des cartes.

~~~ {.python .test .amc file="Q_4.md" bareme="1"}
	>>> j = Joueur('Bob')
	>>> j.prendre_cartes([Carte(3), Carte(10), Carte(51)])
    >>> j.a_des_cartes()
	True
	>>> j2 = Joueur("Roger")
	>>> j2.prendre_cartes([Carte(1), Carte(20), Carte(30), Carte(55)])
	>>> j2.a_des_cartes()
	True
	>>> j3 = Joueur("Marcel")
	>>> j3.a_des_cartes()
	False
~~~

~~~ {.python .hidden .test file="Q_4_bis.md" bareme="1"}
>>> j4.a_des_cartes()
True
>>> j5.a_des_cartes()
False
~~~



5. Quand le joueur demande à poser une carte, il faut que la carte
   soit dans sa main sinon cela provoque une `AssertionError`.

~~~ {.python .test .amc file="Q_5.md" bareme="1"}
	>>> j = Joueur('Bob')
	>>> j.prendre_cartes([Carte(3), Carte(10), Carte(51)])
    >>> j.est_carte_valable(5)
    False
    >>> j.poser_carte(5)
    Traceback (most recent call last):
    ...
    AssertionError
    >>> j.est_carte_valable(3)
    True
~~~

~~~ {.python .hidden .test file="Q_5_bis.md" bareme="1"}
>>> j4.poser_carte(6)
Traceback (most recent call last):
...
AssertionError
>>> j5.poser_carte(6)
Traceback (most recent call last):
...
AssertionError
~~~



6. Si le joueur possède la carte demandée alors elle est renvoyée et le
   joueur possède une carte en moins.

~~~ {.python .test .amc file="Q_6.md" bareme="1"}
	>>> j = Joueur('Bob')
	>>> j.prendre_cartes([Carte(3), Carte(10), Carte(51)])
    >>> len(j.main)
	3
	>>> j.poser_carte(3)
	Carte n°3 - 1 TdB
    >>> len(j.main)
	2
	>>> j2 = Joueur("Roger")
	>>> j2.prendre_cartes([Carte(1), Carte(20), Carte(30), Carte(55)])
    >>> len(j2.main)
	4
	>>> j2.poser_carte(55)
	Carte n°55 - 7 TdB
    >>> len(j2.main)
	3
~~~

~~~ {.python .hidden .test file="Q_6_bis.md" bareme="1"}
>>> j4.poser_carte(7)
Carte n°7 - 1 TdB
>>> len(j4.main)
5
>>> j4.main
[Carte n°5 - 2 TdB, Carte n°75 - 2 TdB, Carte n°77 - 5 TdB, Carte n°88 - 5 TdB, Carte n°101 - 1 TdB]
~~~



7.  On teste l'ajout de pénalités.

~~~ {.python .test .amc file="Q_7.md" bareme="1"}
	>>> j = Joueur('Bob')
    >>> j.get_score()
	0
	>>> j.ajouter_penalites([Carte(9), Carte(11)])
    >>> j.get_score()
	6
	>>> j2 = Joueur("Roger")
	>>> j2.ajouter_penalites([Carte(90), Carte(33), Carte(104)])
    >>> j2.get_score()
	9
~~~

~~~ {.python .hidden .test file="Q_7_bis.md" bareme="1"}
>>> j4.ajouter_penalites([Carte(90), Carte(33), Carte(104)])
>>> j4.get_score()
21
~~~



8. On vérifie la main des joueurs.

~~~ {.python .test .amc file="Q_8.md" bareme="1"}
	>>> j = Joueur('Bob')
	>>> j.prendre_cartes([Carte(3), Carte(10), Carte(51)])
	>>> j.poser_carte(3)
	Carte n°3 - 1 TdB
    >>> j.get_main()
	'10-51'
	>>> j2 = Joueur("Roger")
	>>> j2.prendre_cartes([Carte(1), Carte(20), Carte(30), Carte(55)])
	>>> j2.poser_carte(55)
	Carte n°55 - 7 TdB
	>>> j2.get_main()
	'1-20-30'
    >>> j2.poser_carte(20)
	Carte n°20 - 3 TdB
    >>> j2.get_main()
	'1-30'
~~~

~~~ {.python .hidden .test file="Q_9_bis.md" bareme="1"}
>>> j4.get_main()
'5-7-75-77-88-101'
>>> j5.get_main()
''
~~~



9. On teste la fonction `__str()__`.

~~~ {.python .test .amc file="Q_9.md" bareme="1"}
	>>> j = Joueur('Bob')
	>>> j.ajouter_penalites([Carte(9), Carte(11)])
	>>> str(j)
	'Bob : 6'
	>>> j2 = Joueur("Roger")
	>>> j2.ajouter_penalites([Carte(90), Carte(33), Carte(104)])
	>>> str(j2)
	'Roger : 9'
    >>> print(j)
	Bob : 6
	>>> print(j2)
	Roger : 9
~~~

~~~ {.python .hidden .test file="Q_8_bis.md" bareme="1"}
>>> str(j4)
'Albert : 12'
>>> str(j5)
'Claire : 0'
~~~

