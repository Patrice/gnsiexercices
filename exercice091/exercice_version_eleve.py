"""
Module joueur.
"""

from carte import Carte
from random import randint

class Joueur:
    """
    La classe joueur.
    Les attributs :
      - nom : string
      - main : une liste de cartes.
      - score : le nombre de têtes de bœufs récupérees lors de la partie.
    Les méthodes :
      - __init__(self, nom="Vous"): initialise le joueur avec un nom par défaut 
        une main vide ([]) et un score nul.                                 
      - prendre_cartes(self, l) : ajoute la liste de cartes à la main. La main
        doit être triée. On vérifie qu'il s'agit bien d'une liste de cartes.
      - get_score(self) : renvoie le score.                                   
      - a_des_cartes(self) : renvoie True si le joueur a encore des cartes.   
      - est_carte_valable(self, n): Pour la version texte. Vérifie si n est le
        numéro d'une carte.                                                
      - poser_carte(n) : renvoie la carte de valeur n et l'enlève de la main. 
        Si la carte de valeur n n'est pas dans la main, une AssertionError  
        est levée.                                                          
      - poser_carte_au_hasard(self) : renvoie une carte au hasard. Utile pour 
        pour faire jouer les autres joueurs.                                
      - ajouter_penalites(self, l) : ajoute les nombres de têtes de bœufs     
        de la liste l au score.                                             
      - get_main(self) : renvoie la liste des cartes séparées par un tiret    
      -  __str__(self) : renvoie le nom du joueur et son score                 
    """

    def __init__(self, nom="Vous"):
        """
        Le constructeur.
        """
        pass
    
    def prendre_cartes(self, l):
        """
        Ajoute la carte à la main et classe la main.
        """
        pass

    def get_score(self):
        """
        Renvoie le score du joueur.
        """
        pass

    def a_des_cartes(self):
        """
        Renvoie True si le joueur a encore des cartes.
        """
        pass

    def est_carte_valable(self, n):
        """
        Pour la version texte. Vérifie si n est le numéro d'une carte.
        """
        pass

    def poser_carte(self, n):
        """
        renvoie la carte de valeur n et l'enlève de la main.
           Si la carte de valeur n n'est pas dans la main, une AssertionError
           est levée.
        """
        pass

        return c

    def poser_carte_au_hasard(self):
        """
        renvoie la carte de valeur n et l'enlève de la main.
           Si la carte de valeur n n'est pas dans la main, une AssertionError
           est levée.
        """
        pass

    def ajouter_penalites(self, l):
        """
        ajoute les nombres de têtes de bœufs
        de la liste l au score.
        """
        pass

    def get_main(self):
        """
        renvoie la liste des cartes séparées par un tiret
        """
        pass
        
            
    def __str__(self):
        """
        renvoie le nom du joueur et son score sous la forme
        nom : score
        """
        pass

