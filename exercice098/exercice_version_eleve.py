from random import randrange

### Partie A ###

# Q.A.1.a.
# Compléter la liste suivante.
A2 = []

# Q.A.1.b
# Compléter la liste suivante.
A4 = []

# Q.A.2
def amis_de_quartier(i, j):
    pass

# Q.A.3
def star(s, n):
    pass


# Q.A.4.a
# Compléter la chaine suivante avec un seul mot. Attention à l'orthographe.
type_retourne_par_est_ami_avec = ""

# Q.A.4.b
def est_ami_avec(A, p) : 
    pass

# Q.A.5
# Compléter la liste suivante. Les listes intérieures correspondent aux étapes.
# Ne pas en rajouter ni en enlever.
etapes = [[], [], [], [], [], [], []]


# Q.A.6
def insertion_sort(t):
    pass

## Partie B

# Q.B.1
# Compléter la liste suivante.
Rb = [[], [], [], [], []]

#Q.B.2
def creer_reseau_vide(n):
    pass

# Q.B.3
def inserer_ami(A, ami):
    pass

# Q.B.4
def rech_dicho(A, p):
    pass

# Q.B.6
def sont_amis(R, i, j):
    pass

# Q.B.6
def declarer_amis(R, i, j):
    pass

# Q.B.7
def generer_reseau_aleatoire(n, na_max):
    pass

