def rendu(somme_a_rendre, liste_pieces):
    """
    Renvoie une liste avec les quantités de pièces à rendre correspondant à la liste des pièces.
    Arguments:
    somme_a_rendre -- un entier.
    liste_pieces   -- liste de pièces rangées dans l'ordre croissant.
    """
    nombres_de_pieces = [0]*len(liste_pieces)
    for i in range(len(liste_pieces)):
        piece = liste_pieces[-1-i]
        n = somme_a_rendre // piece
        nombres_de_pieces[-1-i] = n
        somme_a_rendre = somme_a_rendre - n * piece
    return nombres_de_pieces


    
