~~~ {.hidden .meta}
classe : terminale
type : oral1
chapitre : algorithme glouton
thème : algorithme glouton
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========

On cherche à rendre une certaine somme entière en utilisant le moins
de pièces possibles parmi un système monétaire donné, dont les valeurs
sont stockées dans une liste (triée par ordre croissant de
valeur). Pour notre système monétaire usuel, les valeurs sont celles
de la liste [1, 2, 5, 10, 20, 50, 100]. On peut utiliser plusieurs
fois la même valeur.

Le but est d’écrire une fonction nommée `rendu` dont le premier paramètre est un entier positif non nul `somme_a_rendre`, le second une liste des pièces du système monétaire rangée dans l'ordre croissant et qui retourne une liste correspondant aux nombres de pièces ou billets de chaque valeur à rendre. 

On utilisera un algorithme glouton : on commencera par rendre le nombre maximal de billets de plus grande valeur, puis le nombre maximal de billets de valeur juste inférieure au précédent et ainsi de suite jusqu'à la pièce de plus petite valeur.

Exemples :

~~~ {.python .test file="test1.md" bareme="1"}
    >>> rendu(13, [1, 2, 5])
    [1, 1, 2]
    >>> rendu(64, [1, 2, 5])
    [0, 2, 12]
	>>> rendu(89, [1, 2, 5])
	[0, 2, 17]
~~~

Le dernier résultat signifie que pour rendre 89€ avec des pièces de 1€ et 2€ et des billets de 5 à disposition, il faut 17 billets de 5€ et 2 pièces de 2€.

~~~ {.python .hidden .amc file="Q_1.md" bareme="2"}
	>>> pass
~~~

~~~ {.python .hidden .test file="test2.md" bareme="2"}
>>> rendu(89, [1, 2, 5, 10, 20])
[0, 2, 1, 0, 4]
>>> rendu(89, [1, 2, 5, 10, 20, 50])
[0, 2, 1, 1, 1, 1]
>>> rendu(89, [1, 2, 5, 10, 20, 50, 100])
[0, 2, 1, 1, 1, 1, 0]
~~~

~~~ {.python .hidden .test file="test3.md" bareme="1"}
>>> rendu(0, [1, 2, 5, 10, 20])
[0, 0, 0, 0, 0]
>>> rendu(89, [1])
[89]
~~~






