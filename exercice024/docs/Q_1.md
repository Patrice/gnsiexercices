    >>> from exercice import *

        >>> nb_repetitions(5, [2, 5, 3, 5, 6, 9, 5])
        3
        >>> nb_repetitions('A', ['B', 'A', 'B', 'A', 'R'])
        2
        >>> nb_repetitions(12, [1, '!', 7, 21, 36, 44])
        0

 
