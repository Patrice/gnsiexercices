"""
Le jeu de cartes complet. Les cartes sont mélangées.
"""

from random import shuffle
from carte import Carte


class Jeu:
    """
    La classe contient un attribut :
    - cartes : une liste de d'objets Carte mélangées.
    La classe contient une méthode.
    - piocher() : qui renvoie la carte sur le haut de la pile. Si la pile est
    vide, une AssertionError est levée.
    - __len(self)__: renvoie le nombre de cartes restant dans le jeu,
    c'est-à-dire le nombre de cartes de la pioche.
`-
    """
    def __init__(self):
        """
        Le constructeur
        """
        pass

    def __len__(self):
        """
        Renvoie la longueur de la liste de cartes
        """
        pass

    def piocher(self):
        """
        Renvoie la carte sur le haut de la pioche.
        """
        pass
