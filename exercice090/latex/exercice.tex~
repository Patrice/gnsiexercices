\hypertarget{exercice}{%
\section{Exercice}\label{exercice}}

La classe Jeu contient un jeu de cartes complet. Les cartes sont
mélangées. On pourra utiliser la fonction \texttt{shuffle()} du module
\texttt{random}.

,------------------------------------------------------------------.
\textbar{}Jeu \textbar{}
\textbar{}------------------------------------------------------------------\textbar{}
\textbar{}+ cartes : la liste mélangée d'objets de type Carte.
\textbar{} \textbar{} \textbar{} \textbar{}+ \textbf{init}(self) : le
constructeur de la classe. \textbar{} \textbar{}+ piocher(self) : qui
renvoie la carte sur le haut de la pile. \textbar{} \textbar{} Si la
pile est vide, une AssertionError est levée.\textbar{}
\textbar{}+\textbf{len(self)}: renvoie le nombre de cartes restant dans
le jeu, \textbar{} \textbar{} c'est-à-dire le nombre de cartes de la
pioche. \textbar{}
`------------------------------------------------------------------'

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  On vérifie que le jeu contient le bon nombre de cartes.
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>}\NormalTok{ j }\OperatorTok{=}\NormalTok{ Jeu()}
    \OperatorTok{>>>} \BuiltInTok{len}\NormalTok{(j.cartes)}
    \DecValTok{104}
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{1}
\tightlist
\item
  On vérifie que le jeu ne contient que des cartes.
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>} \KeywordTok{def}\NormalTok{ contient_cartes(l):}
\NormalTok{    ...     }\ControlFlowTok{for}\NormalTok{ c }\KeywordTok{in}\NormalTok{ l.cartes:}
\NormalTok{    ...         }\ControlFlowTok{if} \KeywordTok{not} \BuiltInTok{isinstance}\NormalTok{(c, Carte):}
\NormalTok{    ...             }\ControlFlowTok{return} \VariableTok{False}
\NormalTok{    ...     }\ControlFlowTok{return} \VariableTok{True}
    \OperatorTok{>>>}\NormalTok{ contient_cartes(j)}
    \VariableTok{True}
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{2}
\tightlist
\item
  On vérifie que le jeu est mélangé.
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>} \KeywordTok{def}\NormalTok{ est_trie(l):}
\NormalTok{    ...     }\ControlFlowTok{for}\NormalTok{ i }\KeywordTok{in} \BuiltInTok{range}\NormalTok{(}\BuiltInTok{len}\NormalTok{(l.cartes)}\OperatorTok{-}\DecValTok{1}\NormalTok{):}
\NormalTok{    ...         }\ControlFlowTok{if}\NormalTok{ l.cartes[i] }\OperatorTok{>}\NormalTok{ l.cartes[i}\DecValTok{-1}\NormalTok{]:}
\NormalTok{    ...             }\ControlFlowTok{return} \VariableTok{False}
\NormalTok{    ...     }\ControlFlowTok{return} \VariableTok{True}
    \OperatorTok{>>>}\NormalTok{ est_trie(j)}
    \VariableTok{False}
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{3}
\tightlist
\item
  En implémentant la méthode \texttt{\_\_len(self)\_\_}, on peut écrire
  simplement \texttt{len(j)} au lieu de \texttt{len(j.cartes)}.
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>} \BuiltInTok{len}\NormalTok{(j)}
    \DecValTok{104}
\end{Highlighting}
\end{Shaded}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{4}
\tightlist
\item
  On vérifie que, si on pioche une carte, le nombre de cartes diminue de
  1 et que l'on obtient bien une carte.
\end{enumerate}

\begin{Shaded}
\begin{Highlighting}[]
    \OperatorTok{>>>}\NormalTok{ c1 }\OperatorTok{=}\NormalTok{ j.piocher()}
    \OperatorTok{>>>} \BuiltInTok{len}\NormalTok{(j)}
    \DecValTok{103}
    \OperatorTok{>>>} \BuiltInTok{isinstance}\NormalTok{(c1, Carte)}
    \VariableTok{True}
    \OperatorTok{>>>}\NormalTok{ c2 }\OperatorTok{=}\NormalTok{ j.piocher()}
    \OperatorTok{>>>}\NormalTok{ c3 }\OperatorTok{=}\NormalTok{ j.piocher()}
    \OperatorTok{>>>} \BuiltInTok{len}\NormalTok{(j)}
    \DecValTok{101}
    \OperatorTok{>>>} \BuiltInTok{isinstance}\NormalTok{(c3, Carte)}
    \VariableTok{True}
\end{Highlighting}
\end{Shaded}

