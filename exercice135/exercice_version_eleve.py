NB_CASES = 10
TAILLE_CASE = 40
NB_BOMBES = 10
BOMBE = 10

def nouvelle_grille(n):
    """
    Renvoie une grille de dimension NB_CASESxNB_CASES remplie 
    avec la valeur n.
    """
    pass

def trait():
    """
    Trace un trait horizontal.
    Fonction utilitaire pour faire une affichage texte de la grille.
    """
    txt = "+"
    for i in range(NB_CASES):
        txt += "-+"
    txt += "\n"
    return txt 

def grille_to_string(g):
    """
    Fonction qui affiche la grille en mode texte.
    Utile pour les tests.
    """
    trt = trait()
    txt = trt
    for i in range(NB_CASES):
        ligne = "|"
        for j in range(NB_CASES):
            if g[i][j] == 0 or g[i][j] == False:
                ligne += " |"
            elif type(g[i][j]) == int and g[i][j]<10:
                ligne += str(g[i][j])+"|"
            else:
                ligne += "X|"
        txt += ligne+"\n"
        txt += trt
    return txt
