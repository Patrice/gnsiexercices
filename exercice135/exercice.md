~~~ {.hidden .meta}
classe : terminale
type : oral1
chapitre : structures linéaires
thème : listes de listes
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========

Les fonctions suivantes seront à recopier dans un module `constantes.py` qui contient des fonctions utiles pour gérer une grille. 

## Constantes

- `NB_CASES`: Le nombre de cases dans une dimension (10).
- `TAILLE_CASE`: La taille d'une case (40). Cette donnée est nécessaire pour créer l'interface graphique avec Processing.
- `NB_BOMBES`: Le nombre de bombes (10).
- `BOMBE`: Valeur représentative d'une bombe (10).

1. La fonction `nouvelle_grille(n)` renvoie une grille de dimension `NB_CASES x NB_CASES` remplie avec la valeur `n`.
~~~ {.python .test .amc file="Q_1.md" bareme="1"}
>>> nouvelle_grille(0)
[[0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
>>> nouvelle_grille(1)
[[1, 1, 1, 1, 1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]]
>>> nouvelle_grille(True)
[[True, True, True, True, True, True, True, True, True, True], [True, True, True, True, True, True, True, True, True, True], [True, True, True, True, True, True, True, True, True, True], [True, True, True, True, True, True, True, True, True, True], [True, True, True, True, True, True, True, True, True, True], [True, True, True, True, True, True, True, True, True, True], [True, True, True, True, True, True, True, True, True, True], [True, True, True, True, True, True, True, True, True, True], [True, True, True, True, True, True, True, True, True, True], [True, True, True, True, True, True, True, True, True, True]]
~~~

2. Les fonctions `trait()` et `grille_to_string(g)` vous sont fournies pour faire des tests. Elles permettent d'afficher la grille en mode console.

~~~ {.python .test file="Q_2.md" bareme="0"}
>>> grille = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
...           [0, 0, 10, 0, 0, 0, 0, 0, 0, 0],
...           [10, 0, 0, 0, 0, 0, 10, 0, 0, 0],
...           [0, 0, 0, 0, 0, 0, 0, 0, 10, 0],
...           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
...           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
...           [0, 10, 0, 0, 0, 0, 0, 0, 0, 0],
...           [0, 0, 0, 0, 0, 0, 0, 10, 0, 0],
...           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
...           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
>>> print(grille_to_string(grille))
+-+-+-+-+-+-+-+-+-+-+
| | | | | | | | | | |
+-+-+-+-+-+-+-+-+-+-+
| | |X| | | | | | | |
+-+-+-+-+-+-+-+-+-+-+
|X| | | | | |X| | | |
+-+-+-+-+-+-+-+-+-+-+
| | | | | | | | |X| |
+-+-+-+-+-+-+-+-+-+-+
| | | | | | | | | | |
+-+-+-+-+-+-+-+-+-+-+
| | | | | | | | | | |
+-+-+-+-+-+-+-+-+-+-+
| |X| | | | | | | | |
+-+-+-+-+-+-+-+-+-+-+
| | | | | | | |X| | |
+-+-+-+-+-+-+-+-+-+-+
| | | | | | | | | | |
+-+-+-+-+-+-+-+-+-+-+
| | | | | | | | | | |
+-+-+-+-+-+-+-+-+-+-+
~~~
