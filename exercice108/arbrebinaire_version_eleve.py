#################################################
#              Module arbre_binaire             # 
#                                               #
# Définit les classes Nœud et ArbreBinaire      #
#                                               #
#            NSI La Pérouse Kerichen            # 
#                                               #
#################################################

class Noeud:
    """
    La classe Noeud stocke :
    - une valeur : ici, une lettre;
    - fin : un booléen qui précise si la valeur correspond à la dernière lettre d'un mot.
    - un fils gauche et un fils droit qui sont des références à d'autres Noeuds.
    Cette classe est utile pour définir l'arbre vide.
    """
    def __init__(self, valeur, gauche, droit, fin=False):
        self.valeur = valeur
        self.fin = fin
        self.fils_gauche = gauche
        self.fils_droit = droit
        
class ArbreBinaire:
    
    def __init__(self, *args):
        """
        Le constructeur de la classe ArbreBinaire qui offre 2 possibilités.
        Si l'objet est construit sans argument, on construit un arbre vide : un arbre qui ne constient pas de nœud.
            arbre_vide = ArbreBinaire()
        Si l'objet est construit avec 3 arguments, on crée un nœud avec :
            - une valeur ;
            - un pointeur vers l'arbre fils gauche (éventuellement vide) ;
            - un pointeur vers l'arbre fils droit (éventuellement vide).
        Pour créer une feuille :
            feuille = ArbreBinaire(3, None, None)
        Pour ajouter cette feuille à un arbre, 
            a1 = ArbreBinaire(5, feuille, None)
        """
        assert len(args) == 3 or len (args) == 0, "Le nombre d'arguments est incorrect."
        if len (args) == 0 :
            self.noeud = None
        else :
            valeur, gauche, droit = args
            assert valeur is not None, "La valeur n'est pas définie."
            if gauche is None :
                gauche = ArbreBinaire()
            if droit is None :
                droit = ArbreBinaire()
            self.noeud = Noeud(valeur, gauche, droit)

    def est_vide(self):
        """
        Un arbre vide est un arbre qui ne contient pas de nœud.
        """
        return self.noeud is None
    
    def racine(self):
        """
        On renvoie la valeur contenue dans le nœud à la racine de l'arbre, 
        et non le nœud lui-même.
        """
        assert not self.est_vide(), "L'arbre est vide."
        return self.noeud.valeur
    
    def fils_gauche(self):
        """
        Renvoie l'arbre fils gauche.
        """
        assert not self.est_vide(), "L'arbre est vide."
        return self.noeud.fils_gauche

    def fils_droit(self):
        """
        Renvoie l'arbre fils droit.
        """
        assert not self.est_vide(), "L'arbre est vide."
        return self.noeud.fils_droit
    
    def cherche_a_droite(self,x):
        """
        Renvoie True si x est dans la branche droite de l'arbre.
        """
        if self.est_vide():
            return None
        elif self.racine() == x :
            return self
        else:
            return self.fils_droit().cherche_a_droite(x)
            
    def cherche_mot(self, mot):
        """
        Renvoie True si mot est dans l'arbre.
        """
        if len(mot) == 1:
            arbre = self.cherche_a_droite(mot)
            if arbre.est_vide():
                return False
            else:
                return arbre.noeud.fin
        else:
            lettre = mot[0]
            mot = mot[1:]
            arbre_suivant = self.cherche_a_droite(lettre)
            if arbre_suivant == None:
                return False
            else:
                return arbre_suivant.fils_gauche().cherche_mot(mot)

    def est_racine(self, mot):
        """
        Renvoie True si mot est la racine d'un mot.
        """
        pass
    
    def ajoute_a_droite(self,x, fin = False):
        """
        Ajoute x dans la branche droite de l'arbre.
        """
        if self.est_vide():
            self.noeud = Noeud(x, ArbreBinaire(), ArbreBinaire(), fin)
            return self
        elif self.racine() == x :
            return self
        else:
            return self.fils_droit().ajoute_a_droite(x, fin)

    def ajoute(self, mot):
        """
        Ajoute x dans l'arbre.
        """

        if mot != "":
            if len(mot)==1:
                arbre_suivant = self.ajoute_a_droite(mot, True)
            else:
                lettre = mot[0]
                arbre_suivant = self.ajoute_a_droite(lettre)
                mot = mot[1:]          
                arbre_suivant.fils_gauche().ajoute(mot)
            
    def longueur_mot_le_plus_long(self):
        """
        Renvoie la longueur du mot le plus long.
        """
        if self.est_vide():
            return 0
        else:
            return max (1+ self.fils_gauche().longueur_mot_le_plus_long(), self.fils_droit().longueur_mot_le_plus_long())

            
    def afficher_arbre(self, indentation = ""):
        """
        Parcours en profondeur de l'arbre de la droite vers la gauche,
        afin de pouvoir afficher nos arbres.
        """
        if not self.est_vide():

            self.fils_droit().afficher_arbre(indentation + "   ")
            print(indentation + str(self.racine())) 
            self.fils_gauche().afficher_arbre(indentation + "   ")
    
  
