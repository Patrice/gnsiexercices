from numpy import sqrt

STATS = [[1, 5.61, 0.5, "F"],
        [2, 2.99, 5.09, "A"],
        [3, 5.28, 0.52, "F"],
        [4, 2.57, 5.87, "A"],
        [5, 6.91, 0.59, "F"],
        [6, 2.68, 5.56, "A"],
        [7, 5.06, 0.63, "F"],
        [8, 2.91, 5.28, "A"],
        [9, 3.09, 4.9, "A"],
        [10, 5.75, 0.6, "F"]]


def distance(x_1, y_1, x_2, y_2):
    "Calcule la distance euclidienne entre 2 points"
    return sqrt((x_1-x_2)**2+(y_1-y_2)**2)


def frequence_lettre(lettre, texte):
    """Calcule la fréquence d'apparition d'une lettre dans un texte.
    La fréquence est en pourcentage."""
    somme = 0
    for caractere in texte:
        if caractere.lower() == lettre.lower():
            somme += 1
    return 100*somme/len(texte)


def liste_distances(x, y, liste_donnees):
    """Renvoie la liste des distances séparant la cible (x;y)
    de tous les points connus (présents dans la liste_donnees.
La liste renvoyée doit être au format [[numero du texte, distance, etiquette]]
    """
    liste = []
    num = 1
    for ligne in liste_donnees:
        dist = distance(x, y, ligne[1], ligne[2])
        liste.append([num, dist, ligne[3]])
        num += 1
    return liste


def prediction(liste, k):
    """Fournit la valeur de l'étiquette la plus présente parmi les k premières valeurs.
    Ici l'étiquette vaut A, F ou ? si on ne peut savoir.
    """
    somme = 0
    for element in liste[:k]:
        if element[-1] == "F":
            somme += -1
        else:
            somme += 1
    if somme < 0:
        return "F"
    if somme > 0:
        return "A"
    return "?"


def predire_langue(texte, k):
    """ Programme qui renvoie la langue dans laquelle est écrite le texte.
    Les résultats possibles sont :
    - français
    - anglais
    - on ne sait pas
    """
    X = frequence_lettre("u", texte)
    Y = frequence_lettre("h", texte)
    ld = liste_distances(X, Y, STATS)
    ld = sorted(ld, key=lambda valeur: valeur[1])
    resultat = prediction(ld, k)
    if resultat == "F":
        return "Je pense que le texte est écrit en français."
    elif resultat == "A":
        return "Je pense que le texte est écrit en anglais."
    else:
        return "Je ne sais pas dans quelle langue est écrit le texte."

"""
T = ("Earlier releases have been removed because"
     "we can only support the current versions of the software. "
     "To update old code, read the changes page. "
     "Changes for each release can be found in revisions.txt. "
     "If you have problems with the current release, please file a bug "
     "so that we can fix it. Older releases can also be built "
     "from the source. Read More about the releases and their numbering. "
     "To use Android Mode, Processing 3 or later is required.")
"""

#T = "I'm very happy to be here !"
#for i in range(1,11) :
#    predire_langue(T, i)
"""
print(liste_distances(3, 3, STATS))
t = "Whatever you may think about NSI, it's wonderful topic !"
print(frequence_lettre('w', t))
print(frequence_lettre('y', t))
print(frequence_lettre('i', t))
"""
