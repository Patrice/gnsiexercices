    >>> from exercice import *

    >>> cartes = [0]
    >>> for i in range(1, 105):
    ...    m = Carte(i)
    ...    cartes.append(m)
    ...    assert m.numero == i

    >>> cartes_pas_simples = []

    >>> m10 = Carte(16)
    >>> m20 = Carte(13)
    >>> m30 = Carte(100)

    >>> m10 < m20
    False
    >>> m30 < m20
    False
    >>> m10 < m30
    True

 
