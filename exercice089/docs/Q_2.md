    >>> from exercice import *

    >>> cartes = [0]
    >>> for i in range(1, 105):
    ...    m = Carte(i)
    ...    cartes.append(m)
    ...    assert m.numero == i

    >>> cartes_pas_simples = []

    >>> m10 = Carte(16)
    >>> m20 = Carte(13)
    >>> m30 = Carte(100)

    >>> m0 = Carte(0)
    Traceback (most recent call last):
    ...
    AssertionError

    >>> m1 = Carte(105)
    Traceback (most recent call last):
    ...
    AssertionError

    >>> m3 = Carte(100.0)
    Traceback (most recent call last):
    ...
    AssertionError

 
