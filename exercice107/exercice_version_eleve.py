# Questions préliminaires

"""
1. Expliquer pourquoi le couple `(1, 3)` est une inversion dans le tableau `[4, 8, 3, 7]`.


"""
score_QP1 = 0


"""
2. Justifier que le couple `(2, 3)` n’en est pas une.


"""
score_QP2 = 0


## Partie A

# Q.A.1

def fonction1(tab, i):
    pass

# Q.A.2
def nombre_inversions(tab):
    pass

# Q.A.3
# Choisir la bonne réponse :
# a. O(n)    b.O(n.log(n))    c. O(n^2)

reponse_A_3 = ""


## Partie B

# Q.B.1
# Choisir la bonne réponse :
# a. tri par sélection    b.tri à bulles    c. tri fusion

reponse_B_1 = ""

## Q.B.2
def moitie_gauche(tab):
    pass


## Q.B.3
def moitie_droite(tab):
    pass


## Q.B.4
def nb_inv_tab(l1, l2):
    compteur = 0
    for e1 in l1:
        for e2 in l2:
            if e1 > e2:
                compteur += 1
    return compteur

def tri(l):
    l.sort()

def nombre_inversions_rec(tab):
    pass
