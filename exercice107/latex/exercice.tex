\hypertarget{exercice}{%
\section{Exercice}\label{exercice}}

\textit{Cet\ exercice\ traite\ de\ manipulation\ de\ tableaux,\ de\ récursivité\ et\ du\ paradigme\ «~diviser\ pour\ régner~».}

Dans un tableau Python d'entiers \texttt{tab}, on dit que le couple
d'indices \texttt{(i,j)} forme une inversion lorsque
\texttt{i\ \textless{}\ j} et
\texttt{tab{[}i{]}\ \textgreater{}\ tab{[}j{]}}. On donne ci-dessous
quelques exemples.

\begin{itemize}
\tightlist
\item
  Dans le tableau \texttt{{[}1,\ 5,\ 3,\ 7{]}}, le couple d'indices
  \texttt{(1,2)} forme une inversion car \texttt{5\ \textgreater{}\ 3}.
  Par contre, le couple \texttt{(1,3)} ne forme pas d'inversion car
  \texttt{5\ \textless{}\ 7}. Il n'y a qu'une inversion dans ce tableau.
\item
  Il y a trois inversions dans le tableau
  \texttt{{[}1,\ 6,\ 2,\ 7,\ 3{]}}, à savoir les couples d'indices
  \texttt{(1,\ 2)}, \texttt{(1,\ 4)} et \texttt{(3,\ 4)}.
\item
  On peut compter six inversions dans le tableau
  \texttt{{[}7,\ 6,\ 5,\ 3{]}} : les couples d'indices
  \texttt{(0,\ 1),\ (0,\ 2),\ (0,\ 3),\ (1,\ 2),\ (1,\ 3)} et
  \texttt{(2,\ 3)}.
\end{itemize}

On se propose dans cet exercice de déterminer le nombre d'inversions
dans un tableau quelconque.

\noindent \textbf{Questions préliminaires}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Expliquer pourquoi le couple \texttt{(1,\ 3)} est une inversion dans
  le tableau \texttt{{[}4,\ 8,\ 3,\ 7{]}}.
\item
  Justifier que le couple \texttt{(2,\ 3)} n'en est pas une.
\end{enumerate}

\noindent \textbf{Partie A : Méthode itérative}

Le but de cette partie est d'écrire une fonction itérative
\texttt{nombre\_inversion} qui renvoie le nombre d'inversions dans un
tableau. Pour cela, on commence par écrire une fonction
\texttt{fonction1} qui sera ensuite utilisée pour écrire la fonction
\texttt{nombre\_inversion}.

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Écrire la fonction \texttt{fonction1(tab,\ i)} qui calcule le nombre
  d'éléments du tableau \texttt{tab} inférieurs à \texttt{tab{[}i{]}}et
  situés après \texttt{i}.

\begin{Shaded}
\begin{Highlighting}[]
\OperatorTok{>>>}\NormalTok{ fonction1([}\DecValTok{1}\NormalTok{, }\DecValTok{5}\NormalTok{, }\DecValTok{3}\NormalTok{, }\DecValTok{7}\NormalTok{], }\DecValTok{0}\NormalTok{)}
\DecValTok{0}
\OperatorTok{>>>}\NormalTok{ fonction1([}\DecValTok{1}\NormalTok{, }\DecValTok{5}\NormalTok{, }\DecValTok{3}\NormalTok{, }\DecValTok{7}\NormalTok{], }\DecValTok{1}\NormalTok{)}
\DecValTok{1}
\OperatorTok{>>>}\NormalTok{ fonction1([}\DecValTok{1}\NormalTok{, }\DecValTok{5}\NormalTok{, }\DecValTok{2}\NormalTok{, }\DecValTok{6}\NormalTok{, }\DecValTok{4}\NormalTok{], }\DecValTok{1}\NormalTok{)}
\DecValTok{2}
\end{Highlighting}
\end{Shaded}

En utilisant la fonction précédente, écrire une fonction
\texttt{nombre\_inversion(tab)} qui prend en argument un tableau et
renvoie le nombre d'inversions dans ce tableau.

On donne ci-dessous les résultats attendus pour certains appels.

\begin{Shaded}
\begin{Highlighting}[]
\OperatorTok{>>>}\NormalTok{ nombre_inversions([}\DecValTok{1}\NormalTok{, }\DecValTok{5}\NormalTok{, }\DecValTok{7}\NormalTok{])}
\DecValTok{0}
\OperatorTok{>>>}\NormalTok{ nombre_inversions([}\DecValTok{1}\NormalTok{, }\DecValTok{6}\NormalTok{, }\DecValTok{2}\NormalTok{, }\DecValTok{7}\NormalTok{, }\DecValTok{3}\NormalTok{])}
\DecValTok{3}
\OperatorTok{>>>}\NormalTok{ nombre_inversions([}\DecValTok{7}\NormalTok{, }\DecValTok{6}\NormalTok{, }\DecValTok{5}\NormalTok{, }\DecValTok{3}\NormalTok{])}
\DecValTok{6}
\end{Highlighting}
\end{Shaded}

\item Quelle est l'ordre de grandeur de la complexité en temps de
  l'algorithme obtenu ? Aucune justification n'est attendue.
\end{enumerate}

\noindent \textbf{Partie B : Méthode récursive}

Le but de cette partie est de concevoir une version récursive de la
fonction \texttt{nombre\_inversion}.

On définit pour cela des fonctions auxiliaires.

\begin{enumerate}
\item Donner le nom d'un algorithme de tri ayant une complexité
  meilleure que quadratique. Dans la suite de cet exercice, on suppose
  qu'on dispose d'une fonction \texttt{tri(tab)} qui prend en argument
  un tableau et renvoie un tableau contenant les mêmes éléments rangés
  dans l'ordre croissant.
\item Écrire une fonction \texttt{moitie\_gauche(tab)} qui prend en
  argument un tableau \texttt{tab} et renvoie un nouveau tableau
  contenant la moitié gauche de \texttt{tab}. Si le nombre d'éléments
  de \texttt{tab} est impair, l'élément du centre se trouve dans cette
  partie gauche. On donne ci-dessous les résultats attendus pour
  certains appels.

\begin{Shaded}
\begin{Highlighting}[]
\OperatorTok{>>>}\NormalTok{ moitie_gauche([])}
\NormalTok{[]}
\OperatorTok{>>>}\NormalTok{ moitie_gauche([}\DecValTok{4}\NormalTok{, }\DecValTok{8}\NormalTok{, }\DecValTok{3}\NormalTok{])}
\NormalTok{[}\DecValTok{4}\NormalTok{, }\DecValTok{8}\NormalTok{]}
\OperatorTok{>>>}\NormalTok{ moitie_gauche ([}\DecValTok{4}\NormalTok{, }\DecValTok{8}\NormalTok{, }\DecValTok{3}\NormalTok{, }\DecValTok{7}\NormalTok{])}
\NormalTok{[}\DecValTok{4}\NormalTok{, }\DecValTok{8}\NormalTok{]}
\end{Highlighting}
\end{Shaded}

\item Écrire la fonction \texttt{moitie\_droite(tab)} qui renvoie la
  moitié droite sans l'élément du milieu.

\begin{Shaded}
\begin{Highlighting}[]
\OperatorTok{>>>}\NormalTok{ moitie_droite([])}
\NormalTok{[]}
\OperatorTok{>>>}\NormalTok{ moitie_droite([}\DecValTok{4}\NormalTok{, }\DecValTok{8}\NormalTok{, }\DecValTok{3}\NormalTok{])}
\NormalTok{[}\DecValTok{3}\NormalTok{]}
\OperatorTok{>>>}\NormalTok{ moitie_droite ([}\DecValTok{4}\NormalTok{, }\DecValTok{8}\NormalTok{, }\DecValTok{3}\NormalTok{, }\DecValTok{7}\NormalTok{])}
\NormalTok{[}\DecValTok{3}\NormalTok{, }\DecValTok{7}\NormalTok{]}
\end{Highlighting}
\end{Shaded}

\item On suppose qu'une fonction \texttt{nb\_inv\_tab(tab1,\ tab2)} a
  été écrite. Cette fonction renvoie le nombre d'inversions du tableau
  obtenu en mettant bout à bout les tableaux \texttt{tab1} et
  \texttt{tab2}, à condition que \texttt{tab1} et \texttt{tab2} soient
  triés dans l'ordre croissant.

  On donne ci-dessous deux exemples d'appel de cette fonction :

\begin{Shaded}
\begin{Highlighting}[]
\OperatorTok{>>>}\NormalTok{ nb_inv_tab([}\DecValTok{3}\NormalTok{, }\DecValTok{7}\NormalTok{, }\DecValTok{9}\NormalTok{], [}\DecValTok{2}\NormalTok{, }\DecValTok{10}\NormalTok{])}
\DecValTok{3}
\OperatorTok{>>>}\NormalTok{ nb_inv_tab([}\DecValTok{7}\NormalTok{, }\DecValTok{9}\NormalTok{, }\DecValTok{13}\NormalTok{], [}\DecValTok{7}\NormalTok{, }\DecValTok{10}\NormalTok{, }\DecValTok{14}\NormalTok{])}
\DecValTok{3}
\end{Highlighting}
\end{Shaded}

En utilisant la fonction \texttt{nb\_inv\_tab} et les questions
précédentes, écrire une fonction récursive
\texttt{nb\_inversions\_rec(tab)} qui permet de calculer le nombre
d'inversions dans un tableau. Cette fonction renverra le même nombre que
\texttt{nombre\_inversions(tab)} de la \textbf{partie A}. On procédera
de la façon suivante :

\begin{itemize}
\tightlist
\item
  Séparer le tableau en deux tableaux de tailles égales (à une unité
  près).
\item
  Appeler récursivement la fonction \texttt{nb\_inversions\_rec} pour
  compter le nombre d'inversions dans chacun des deux tableaux.
\item
  Trier les deux tableaux.
\item
  Ajouter au nombre d'inversions précédemment comptées le nombre renvoyé
  par la fonction \texttt{nb\_inv\_tab} avec pour arguments les deux
  tableaux triés
\end{itemize}
\end{enumerate}
