~~~ {.hidden .meta}
classe : terminale
type : écrit
chapitre : arbres
thème : parcours
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========

On souhaite développer un jeu de Scrabble. Pour pouvoir vérifier
rapidement si un mot existe, nous pouvons stocker le dictionnaire sous
forme d'arbre binaire. Dans le cadre de cet exercice, nous allons nous
limiter aux mots qui commencent par la lettre `a`.

**Partie A**

Sur les figures suivantes, les arbres sont représentés vers le bas,
les fils droits vers la droite et les fils gauches vers le bas. Ainsi
les mots `abaca` (bananier des Philippines), `abaque` et `abats`
seront stockés sous la forme suivante.

   a
   |
   b
   |
   a
   |
   c--q--t
   |  |  |
   a  u  s
      |
      e
	  
	  
1.  Quel est la taille de l'arbre ci-dessus ?

~~~ {.python .hidden .test .amc file="A_1.md" bareme="1"}
>>> taille
10
~~~

2. Pourquoi est-il pertinent de mémoriser le dictionnaire de la langue
   française sous forme d'arbre binaire ?

~~~ {.python .hidden .amc file="A_2.md" bareme="1"}
>>> pass
~~~

3. Dans la figure ci-dessous, deux mots ont été ajoutés. Quels
   sont-ils ?

   a
   |
   b
   |
   a--------b
   |        |
   c--q--t  é--e
   |  |  |     |
   a  u  s     s
      |        |
      e        s
               |
               e
    

~~~ {.python .hidden .test .amc file="A_3.md" bareme="1"}
>>> mot_1 in ["abbé", "abbesse"]
True
>>> mot_2 in ["abbé", "abbesse"]
True
~~~

4. Compléter la figure en représentant le mot `abbaye`.

~~~ {.python .hidden .test .amc file="A_4.md" bareme="1"}
>>> reponse_A_4
'b'
~~~

**Partie B**.

Vous trouverez en annexe le code source de la classe
`ArbreBinaire`. L'objet de cette partie est d'implémenter une méthode
de recherche d'un mot dans l'arbre.

1. Écrire une méthode `ajoute_a_droite(self,x)` qui ajoute `x`, s'il
   n'est pas présent, à la branche droite de l'arbre en partant de la
   racine. La méthode doit renvoyer l'arbre dont la racine contient la
   lettre `x`.

   Supposons que `a` corresponde à l'arbre binaire représenté
   ci-dessous, dont la racine est `"c"`.

   c--q--t
   |  |  |
   a  u  s
      |
      e
      

   `a.ajoute_a_droite("q")` doit renvoyer l'arbre qui contient la lettre `"q"`
   sans modifier l'arbre.

   En revanche `a.ajoute_a_droite("b")` doit ajouter la lettre `"b"` à
   l'extrémité de la branche et renvoyer l'arbre qui la
   contient. On obtiendra alors alors le suivant :

   c--q--t--b
   |  |  |
   a  u  s
      |
      e

~~~ {.python .hidden .amc file="B_1.md" bareme="2"}
>>> pass
~~~

~~~ {.python .hidden .test file="B_1_a.md" bareme="1"}
>>> arbre_1 = ArbreBinaire('c', ArbreBinaire('a', None, None), None)
>>> arbre_1.racine()
'c'
>>> q = arbre_1.ajoute_a_droite('q')
>>> q.racine()
'q'
>>> arbre_1.fils_droit() == q
True
>>> t = arbre_1.ajoute_a_droite('t')
>>> t.racine()
't'
>>> arbre_1.fils_droit().fils_droit() == t
True
~~~

~~~ {.python .hidden .test file="B_1_b.md" bareme="1"}
>>> arbre_1 = ArbreBinaire('c', ArbreBinaire('a', None, None), None)
>>> arbre_1.racine()
'c'
>>> q = arbre_1.ajoute_a_droite('q')
>>> q.racine()
'q'
>>> arbre_1.fils_droit() == q
True
>>> t = arbre_1.ajoute_a_droite('t')
>>> t.racine()
't'
>>> arbre_1.fils_droit().fils_droit() == t
True
>>> t_bis = arbre_1.ajoute_a_droite('t')
>>> t_bis == t
True
~~~
  
2.  Écrire une méthode `ajoute(self, mot)` qui ajoute le `mot` à l'arbre.

~~~ {.python .hidden .amc file="B_2.md" bareme="2"}
>>> pass
~~~

~~~ {.python .hidden .test file="B_2_a.md" bareme="1"}
>>> a = ArbreBinaire('a', None, None)
>>> a.racine()
'a'
>>> a.ajoute('abaca')
>>> a.fils_droit().est_vide()
True
>>> a.fils_gauche().racine()
'b'
>>> a.fils_gauche().fils_gauche().fils_gauche().fils_gauche().racine()
'a'
~~~

~~~ {.python .hidden .test file="B_2_b.md" bareme="1"}
>>> a = ArbreBinaire('a', None, None)
>>> a.racine()
'a'
>>> a.ajoute('abaca')
>>> a.fils_droit().est_vide()
True
>>> a.fils_gauche().racine()
'b'
>>> a.fils_gauche().fils_gauche().fils_gauche().noeud.fin
False
>>> a.fils_gauche().fils_gauche().fils_gauche().fils_gauche().noeud.fin
True
~~~


**Partie C**

1.  Écrire une méthode `cherche_a_droite(self, x)` qui cherche la
    présence de `x` dans la branche droite de l'arbre en partant de la
    racine. Cette méthode renverra `None` si `x` n'est pas présent
    sinon elle renverra l'arbre qui contient la valeur `x`.

    Supposons que `a` corresponde à l'arbre binaire représenté
    ci-dessous, dont la racine est `"c"`.
    
    c--q--t
    |  |  |
    a  u  s
       |
       e
 

	`a.cherche_a_droite("q")` doit renvoyer l'arbre qui contient la lettre
    `"q"`.
    
    `a.cherche_a_droite("d")` doit renvoyer `None`.
    

~~~ {.python .hidden .amc file="C_1.md" bareme="2"}
>>> pass
~~~

~~~ {.python .hidden .test file="C_1_a.md" bareme="1"}
>>> t = ArbreBinaire('t', ArbreBinaire('s', None, None), None)
>>> t.fils_gauche().noeud.fin = True
>>> q = ArbreBinaire('q', ArbreBinaire('u', ArbreBinaire('e', None, None), None), t)
>>> q.fils_gauche().fils_gauche().noeud.fin = True
>>> c = ArbreBinaire('c',ArbreBinaire('a', None, None), q)
>>> c.fils_droit().racine()
'q'
>>> c.fils_droit().fils_droit().racine()
't'
>>> c.fils_droit().fils_droit().fils_droit().est_vide()
True
>>> c.cherche_a_droite('q').racine()
'q'
>>> c.cherche_a_droite('t').racine()
't'
~~~

~~~ {.python .hidden .test file="C_1_b.md" bareme="1"}
>>> t = ArbreBinaire('t', ArbreBinaire('s', None, None), None)
>>> t.fils_gauche().noeud.fin = True
>>> q = ArbreBinaire('q', ArbreBinaire('u', ArbreBinaire('e', None, None), None), t)
>>> q.fils_gauche().fils_gauche().noeud.fin = True
>>> c = ArbreBinaire('c',ArbreBinaire('a', None, None), q)
>>> c.fils_droit().racine()
'q'
>>> c.fils_droit().fils_droit().racine()
't'
>>> c.fils_droit().fils_droit().fils_droit().est_vide()
True
>>> c.cherche_a_droite('q').racine()
'q'
>>> c.cherche_a_droite('t').racine()
't'
>>> c.cherche_a_droite('r')
~~~

2.  Écrire une méthode `cherche_mot(self, mot)` qui renvoie `True` si
    le mot est présent dans l'arbre, `False` sinon.

~~~ {.python .hidden .test .amc file="C_2.md" bareme="2"}
>>> t = ArbreBinaire('t', ArbreBinaire('s', None, None), None)
>>> t.fils_gauche().noeud.fin = True
>>> q = ArbreBinaire('q', ArbreBinaire('u', ArbreBinaire('e', None, None), None), t)
>>> q.fils_gauche().fils_gauche().noeud.fin = True
>>> c = ArbreBinaire('c',ArbreBinaire('a', None, None), q)
>>> c.fils_gauche().noeud.fin = True
>>> a = ArbreBinaire('a',ArbreBinaire('b', ArbreBinaire('a', c, None), None), None)
>>> a.cherche_mot('abaque')
True
>>> a.cherche_mot('abaque')
True
>>> a.cherche_mot('abats')
True
>>> a.cherche_mot('abat')
False
~~~

3.  Écrire une méthode `longueur_mot_le_plus_long(self)` qui renvoie
    la longueur du mot le plus long.

~~~ {.python .hidden .amc file="C_3.md" bareme="2"}
>>> pass
~~~

~~~ {.python .hidden .test file="C_3_a.md" bareme="1"}
>>> t = ArbreBinaire('t', ArbreBinaire('s', None, None), None)
>>> t.fils_gauche().noeud.fin = True
>>> q = ArbreBinaire('q', ArbreBinaire('u', ArbreBinaire('e', None, None), None), t)
>>> q.fils_gauche().fils_gauche().noeud.fin = True
>>> c = ArbreBinaire('c',ArbreBinaire('a', None, None), q)
>>> c.fils_gauche().noeud.fin = True
>>> a = ArbreBinaire('a',ArbreBinaire('b', ArbreBinaire('a', c, None), None), None)
>>> a.longueur_mot_le_plus_long()
6
~~~

~~~ {.python .hidden .test file="C_3_b.md" bareme="1"}
>>> t = ArbreBinaire('t', ArbreBinaire('s', None, None), None)
>>> t.fils_gauche().noeud.fin = True
>>> c = ArbreBinaire('c',ArbreBinaire('a', None, None), t)
>>> c.fils_gauche().noeud.fin = True
>>> a = ArbreBinaire('a',ArbreBinaire('b', ArbreBinaire('a', c, None), None), None)
>>> a.longueur_mot_le_plus_long()
5
~~~

