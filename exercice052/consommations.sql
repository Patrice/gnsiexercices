BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "Mesures" (
	"id_mesure"	INTEGER,
	"id_centre"	INTEGER,
	"date_mesure"	TEXT,
	"temperature"	INTEGER,
	"pression"	INTEGER,
	"pluviometrie"	INTEGER
);
CREATE TABLE IF NOT EXISTS "Centres" (
	"id_centre"	INTEGER,
	"nom_ville"	TEXT,
	"latitude"	REAL,
	"longitude"	REAL,
	"altitude"	INTEGER
);
INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (1566,138,'2021-10-29',8,1015,3);
INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (1568,213,'2021-10-29',15.1,1011,0);
INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (2174,126,'2021-10-30',18.2,1023,0);
INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (2200,185,'2021-10-30',5.6,989,20);
INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (2232,459,'2021-10-31',25,1035,0);
INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (2514,213,'2021-10-31',17.4,1020,0);
INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (2563,126,'2021-11-01',10.1,1005,15);
INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (2592,459,'2021-11-01',23.3,1028,2);
INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (3425,317,'2021-11-02',9,1012,13);
INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (3430,138,'2021-11-02',7.5,996,16);
INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (3611,263,'2021-11-03',13.9,1005,8);
INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (3625,126,'2021-11-03',10.8,1008,8);
INSERT INTO "Centres" ("id_centre","nom_ville","latitude","longitude","altitude") VALUES (213,'Amiens',49.894,2.293,60);
INSERT INTO "Centres" ("id_centre","nom_ville","latitude","longitude","altitude") VALUES (138,'Grenoble',45.185,5.723,550);
INSERT INTO "Centres" ("id_centre","nom_ville","latitude","longitude","altitude") VALUES (263,'Brest',48.388,-4.49,52);
INSERT INTO "Centres" ("id_centre","nom_ville","latitude","longitude","altitude") VALUES (185,'Tignes',45.469,6.909,2594);
INSERT INTO "Centres" ("id_centre","nom_ville","latitude","longitude","altitude") VALUES (459,'Nice',43.706,7.262,260);
INSERT INTO "Centres" ("id_centre","nom_ville","latitude","longitude","altitude") VALUES (126,'LePuy-en-Velay',45.042,3.888,744);
INSERT INTO "Centres" ("id_centre","nom_ville","latitude","longitude","altitude") VALUES (317,'Gérardmer',48.073,6.879,855);
COMMIT;
