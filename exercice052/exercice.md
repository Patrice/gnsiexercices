~~~ {.hidden .meta}
classe : terminale
type : écrit
chapitre : bases de données
thème : parcours
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
>>> import sqlite3 
>>> conn = sqlite3.connect('consommations.sqlite')
>>> def initialisation():
...     """
...     Fonction qui vide la table des coureurs si elle existe puis en crée une nouvelle.
...     """
... 
...     curseur = conn.cursor()
... 
...     curseur.executescript("""
...     DROP TABLE IF EXISTS Mesures;
...     DROP TABLE IF EXISTS Centres;
...     CREATE TABLE IF NOT EXISTS "Mesures" (
...     	"id_mesure"	INTEGER,
...     	"id_centre"	INTEGER,
...     	"date_mesure"	TEXT,
...     	"temperature"	INTEGER,
...     	"pression"	INTEGER,
...     	"pluviometrie"	INTEGER
...     );
...     CREATE TABLE IF NOT EXISTS "Centres" (
...     	"id_centre"	INTEGER,
...     	"nom_ville"	TEXT,
...     	"latitude"	REAL,
...     	"longitude"	REAL,
...     	"altitude"	INTEGER
...     );
...     INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (1566,138,'2021-10-29',8,1015,3);
...     INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (1568,213,'2021-10-29',15.1,1011,0);
...     INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (2174,126,'2021-10-30',18.2,1023,0);
...     INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (2200,185,'2021-10-30',5.6,989,20);
...     INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (2232,459,'2021-10-31',25,1035,0);
...     INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (2514,213,'2021-10-31',17.4,1020,0);
...     INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (2563,126,'2021-11-01',10.1,1005,15);
...     INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (2592,459,'2021-11-01',23.3,1028,2);
...     INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (3425,317,'2021-11-02',9,1012,13);
...     INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (3430,138,'2021-11-02',7.5,996,16);
...     INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (3611,263,'2021-11-03',13.9,1005,8);
...     INSERT INTO "Mesures" ("id_mesure","id_centre","date_mesure","temperature","pression","pluviometrie") VALUES (3625,126,'2021-11-03',10.8,1008,8);
...     INSERT INTO "Centres" ("id_centre","nom_ville","latitude","longitude","altitude") VALUES (213,'Amiens',49.894,2.293,60);
...     INSERT INTO "Centres" ("id_centre","nom_ville","latitude","longitude","altitude") VALUES (138,'Grenoble',45.185,5.723,550);
...     INSERT INTO "Centres" ("id_centre","nom_ville","latitude","longitude","altitude") VALUES (263,'Brest',48.388,-4.49,52);
...     INSERT INTO "Centres" ("id_centre","nom_ville","latitude","longitude","altitude") VALUES (185,'Tignes',45.469,6.909,2594);
...     INSERT INTO "Centres" ("id_centre","nom_ville","latitude","longitude","altitude") VALUES (459,'Nice',43.706,7.262,260);
...     INSERT INTO "Centres" ("id_centre","nom_ville","latitude","longitude","altitude") VALUES (126,'LePuy-en-Velay',45.042,3.888,744);
...     INSERT INTO "Centres" ("id_centre","nom_ville","latitude","longitude","altitude") VALUES (317,'Gérardmer',48.073,6.879,855);
...     """)
...     conn.commit()
>>> def requete_correcte(solution, proposition):
...     # Créer un cursor 
...     cur = conn.cursor() 
...     comparaison = "WITH TA AS ({}), TB AS ({}) SELECT * FROM TA EXCEPT SELECT * FROM TB UNION ALL SELECT * FROM TB EXCEPT SELECT * FROM TA;".format(solution, proposition)
...     # Exécution de la requete
...     try:
...         resultat = cur.execute(comparaison) 
...         row = cur.fetchone()
...         if row is None:
...             rc = True
...         else:
...             rc = False
...     except Exception:
...         print("Le nombre de colonnes est incorrect !")
...         rc = False
...     # Envoyer la requete 
...     conn.commit()
...     # Fermer la connexion 
...     conn.close
...     return rc
>>> def execute(proposition):
...     # Créer un cursor 
...     cur = conn.cursor() 
...     # Exécution de la requete
...     try:
...         resultat = cur.execute(proposition)
...         rc =True
...     except Exception:
...         print("Erreur lors de l'exécution de la requête !")
...         rc = False
...     # Envoyer la requete 
...     conn.commit()
...     # Fermer la connexion 
...     conn.close
...     return rc
>>> def verification_simple(proposition, sortie):
...     # Créer un cursor 
...     cur = conn.cursor() 
...     # Exécution de la requete
...     try:
...         resultat = cur.execute(proposition) 
...         row = cur.fetchone()
...         rc = row == sortie
...     except Exception:
...         print("Erreur lors de l'exécution de la requête !")
...         rc = False
...     # Envoyer la requete 
...     conn.commit()
...     # Fermer la connexion 
...     conn.close
...     return rc
>>> solution_Q_A_2_b = "SELECT nom_ville FROM Centres WHERE altitude>=700 AND altitude<=1200"
>>> solution_Q_A_2_c = "SELECT longitude, nom_ville FROM Centres WHERE longitude>5 ORDER BY nom_ville"
>>> solution_Q_A_6 = "SELECT DISTINCT c.nom_ville FROM Centres AS c JOIN Mesures AS m ON c.id_centre=m.id_centre WHERE m.pluviometrie >=10"
>>> conso = [{'Année': 23, 'Consommation': 6.8, 'Date': '03/01', 'Jour': 'Mardi'},
...  {'Année': 23, 'Consommation': 8.5, 'Date': '02/01', 'Jour': 'Lundi'},
...  {'Année': 23, 'Consommation': 7.5, 'Date': '01/01', 'Jour': 'Dimanche'},
...  {'Année': 22, 'Consommation': 10, 'Date': '31/12', 'Jour': 'Samedi'},
...  {'Année': 22, 'Consommation': 9.6, 'Date': '30/12', 'Jour': 'Vendredi'},
...  {'Année': 22, 'Consommation': 8, 'Date': '29/12', 'Jour': 'Jeudi'},
...  {'Année': 22, 'Consommation': 8.1, 'Date': '28/12', 'Jour': 'Mercredi'},
...  {'Année': 22, 'Consommation': 11.1, 'Date': '27/12', 'Jour': 'Mardi'},
...  {'Année': 22, 'Consommation': 12.5, 'Date': '26/12', 'Jour': 'Lundi'},
...  {'Année': 22, 'Consommation': 9.4, 'Date': '25/12', 'Jour': 'Dimanche'}]
~~~

Exercice
========

*Cet exercice porte sur les bases de données, les fichiers csv et tableaux de dictionnaires.*

Martin veut faire des économies dans sa vie de tous les jours en réduisant notamment ses dépenses en
énergie. En particulier, il cherche à réduire ses factures d’électricité. Pour cela, il va utiliser ses
connaissances en informatique pour obtenir des informations lui permettant de faire les économies
souhaitées. Il est également par ailleurs très impliqué dans la lutte contre le réchauffement climatique et a
décidé de créer une base de données pour étudier plus profondément ce phénomène. On étudie dans cet
exercice séparément dans deux parties distinctes les deux intérêts de Martin. Les parties A et B sont donc
indépendantes.

Partie A
--------

Martin a récupéré sur internet des données d’un centre météorologique et a créé deux relations (tables). La
relation Centres contient l’identifiant des centres météorologiques, la ville, la latitude, la longitude et l'altitude
du centre. La relation Mesures contient l'identifiant de la mesure, l'identifiant du centre, la date de la mesure,
la température, la pression et la pluviométrie mesurées.

Le schéma relationnel de la relation `centres` est le suivant :

`Centres(id_centre: INT, nom_ville: VARCHAR, latitude: FLOAT, longitude: FLOAT, altitude: INT)`

Le schéma relationnel de la relation `mesures` est le suivant :

`Mesures(id_mesure: INT, id_centre: INT, date_mesure: DATE, temperature: FLOAT, pression: INT, pluviometrie: INT)`

On fournit ci-dessous le contenu des deux relations.

*Relation centres*

| id_centre | nom_ville         | latitude | longitude | altitude |
|:---------:|:-----------------:|:--------:|:---------:|:--------:|
| 213       | 'Amiens'          | 49.894   | 2.293     | 60       |
| 138       | 'Grenoble'        | 45.185   | 5.723     | 550      |
| 263       | 'Brest'           | 48.388   | -4.49     | 52       |
| 185       | 'Tignes'          | 45.469   | 6.909     | 2594     |
| 459       | 'Nice'            | 43.706   | 7.262     | 260      |
| 126       | 'Le Puy-en-Velay' | 45.042   | 3.888     | 744      |
| 317       | 'Gérardmer'       | 48.073   | 6.879     | 855      |

*Relation mesures*

| id_mesure | id_centre | date_mesure  | temperature | pression | pluviometrie |
|:---------:|:---------:|:------------:|:-----------:|:--------:|:------------:|
| 1566      | 138       | '2021-10-29' | 8.0         | 1015     | 3            |
| 1568      | 213       | '2021-10-29' | 15.1        | 1011     | 0            |
| 2174      | 126       | '2021-10-30' | 18.2        | 1023     | 0            |
| 2200      | 185       | '2021-10-30' | 5.6         | 989      | 20           |
| 2232      | 459       | '2021-10-31' | 25.0        | 1035     | 0            |
| 2514      | 213       | '2021-10-31' | 17.4        | 1020     | 0            |
| 2563      | 126       | '2021-11-01' | 10.1        | 1005     | 15           |
| 2592      | 459       | '2021-11-01' | 23.3        | 1028     | 2            |
| 3425      | 317       | '2021-11-02' | 9.0         | 1012     | 13           |
| 3430      | 138       | '2021-11-02' | 7.5         | 996      | 16           |
| 3611      | 263       | '2021-11-03' | 13.9        | 1005     | 8            |
| 3625      | 126       | '2021-11-03' | 10.8        | 1008     | 8            |

1.a. Proposer une clé primaire pour la relation `Mesures`. Justifier votre choix.

~~~ {.python .hidden .amc file="Q_A_1_a.md" bareme="1"}
>>> pass
~~~

~~~ {.python .hidden .test file="A_1_1_a.md" bareme="1"}
>>> cle_primaire_mesure
'id_mesure'
~~~

  b. Avec quel attribut peut-on faire une jointure entre la relation `Centres` et la relation `Mesures` ?

~~~ {.python .hidden .amc file="Q_A_1_b.md" bareme="1"}
>>> pass
~~~

~~~ {.python .hidden .test file="Q_A_1_b.md" bareme="1"}
>>> attribut_de_jointure
'id_centre'
~~~

2. a. Qu’affiche la requête suivante `SELECT nom_ville FROM Centres WHERE altitude > 500;`

~~~ {.python .hidden .amc file="Q_A_2_a.md" bareme="1"}
>>> pass
~~~

b. On souhaite récupérer le nom de la ville des centres météorologiques situés à une altitude comprise entre 700 m et 1200 m, inclus. Écrire la requête SQL correspondante.

~~~ {.python .hidden .test .amc file="Q_A_2_b.md" bareme="1"}
>>> initialisation()
>>> assert requete_correcte(solution_Q_A_2_b, prop_Q_A_2_b)
~~~


c. On souhaite récupérer la liste des longitudes et des noms des villes des centres météorologiques dont la longitude est supérieure à 5.0 La liste devra être triée par ordre alphabétique des noms
de ville. Écrire la requête SQL correspondante.

~~~ {.python .hidden .test .amc file="Q_A_2_c.md" bareme="1"}
>>> assert requete_correcte(solution_Q_A_2_c, prop_Q_A_2_c)
~~~


3. 
    1. Qu'affiche la requête suivante ?
 
`SELECT * FROM Mesures WHERE date_mesure = '2021-10-30';`

~~~ {.python .hidden .amc file="Q_A_3_a.md" bareme="1"}
>>> pass
~~~

    2. Écrire une requête SQL permettant d'ajouter une mesure prise le
       8 novembre 2021 dans le centre numéro 138, où la température était de
       11°C, la pression de 1013 hPa et la pluviométrie de 0 mm.  La donnée
       dont l'attribut est id_mesure aura pour valeur 3650.

~~~ {.python .hidden .test .amc file="Q_A_3_b.md" bareme="1"}
>>> execute(prop_Q_A_3_b)
True
>>> verification_simple("SELECT * FROM Mesures WHERE id_mesure=3650", (3650, 138, '2021-11-08', 11, 1013, 0))
True
~~~

4. Écrire une requête SQL donnant la température moyenne de toutes les
   mesures effectuées dans la relation `Mesures`.

~~~ {.python .hidden .test .amc file="Q_A_4.md" bareme="1"}
>>> initialisation()
>>> verification_simple(prop_Q_A_4, (13.658333333333333,))
True
~~~



5. Expliquer ce que renvoie la requête SQL suivante :

`SELECT * FROM Centres WHERE latitude = (SELECT MIN(latitude) FROM Centres);`

~~~ {.python .hidden .amc file="Q_A_5.md" bareme="1"}
>>> pass
~~~

6. Écrire une requête SQL donnant la liste des villes (leur nom) dans
   lesquelles on a enregistré une pluviométrie supérieure ou égal à 10mm.
   On utilisera le mot clé DISTINCT afin d'éviter
   d'avoir des doublons. On rappelle que l'on peut utiliser les
   opérateurs de comparaison avec les dates.

~~~ {.python .hidden .test .amc file="Q_A_6.md" bareme="2"}
>>> assert requete_correcte(solution_Q_A_6, prop_Q_A_6)
~~~

Partie B
--------

Dans cette partie, on s’intéresse à la consommation en électricité de
Martin. La consommation électrique d’un appareil se mesure en kwH
(kilowattheure). Par exemple, le réfrigérateur américain de Martin
consomme en moyenne 1,8 kwH chaque jour. En l’espace d’un an, son
réfrigérateur aura donc consommé $1,8 \times 365$ soit 657
kwH. Actuellement, au 1er janvier 2023, le prix du kwH pour un
logement comme celui de Martin est de 0,1740 € le kwH ( TRV : Tarif
Réglementé de l’électricité en Vigueur ). Si ce prix se maintient
pendant 1 an, le réfrigérateur américain de Martin lui coûtera donc
$657 \times 0,1740$ soit environ 114€.

Grâce à son compteur électrique communiquant, Martin dispose d’un
fichier `consommations_martin.csv` donnant sa consommation en kwH
chaque jour de l’année du mercredi 01/01/2020 au mardi 03/01/2023
(voir image ci-contre). Par exemple, on voit que le mardi 03/01/2023,
il a consommé au total 6,8 kwH avec tous ses appareils électriques.

Extrait du fichier `consommations_martin.csv`

Le but de cette partie est d’étudier les données de ce fichier afin de
trouver une offre intéressante pour Martin. On décide d’importer les
données en Python dans une variable consommations_martin de type liste
de dictionnaires représentant toutes les consommations quotidiennes de
Martin du 03/01/2023 au 01/01/2020.

~~~ {.python}
>>> print(consommations_martin)
[{'Année': 23, 'Consommation': 6.8, 'Date': '03/01', 'Jour': 'Mardi'},
 {'Année': 23, 'Consommation': 8.5, 'Date': '02/01', 'Jour': 'Lundi'},
 {'Année': 23, 'Consommation': 7.5, 'Date': '01/01', 'Jour': 'Dimanche'},
 {'Année': 22, 'Consommation': 10, 'Date': '31/12', 'Jour': 'Samedi'},
 {'Année': 22, 'Consommation': 9.6, 'Date': '30/12', 'Jour': 'Vendredi'},
 {'Année': 22, 'Consommation': 8, 'Date': '29/12', 'Jour': 'Jeudi'},
 {'Année': 22, 'Consommation': 8.1, 'Date': '28/12', 'Jour': 'Mercredi'},
 {'Année': 22, 'Consommation': 11.1, 'Date': '27/12', 'Jour': 'Mardi'},
...
~~~

1. Que vaut `consommations_martin[0]` ? Quel est son type ?

~~~ {.python .hidden .amc file="Q_B_1.md" bareme="1"}
>>> pass
~~~

2. Écrire une fonction `week_end(donnees_jour)` qui prend en paramètre un dictionnaire représentant une consommation un jour donné et qui renvoie `True` si ce jour est un samedi ou un dimanche et `False`sinon. 

On doit avoir par exemple : 

~~~ {.python}
>>> week_end({'Année': 23, 'Consommation': '6.8', 'Date': '03/01', 'Jour': 'Mardi'})
False
~~~

~~~ {.python .hidden .amc .test file="Q_B_2.md" bareme="1"}
>>> week_end({'Année': 22, 'Consommation': 8, 'Date': '29/12', 'Jour': 'Jeudi'})
False
>>> week_end({'Année': 23, 'Consommation': 7.5, 'Date': '01/01', 'Jour': 'Dimanche'})
True
~~~


3. On voudrait connaître la date où Martin a le plus consommé. Compléter la fonction suivante
`conso_maximale(consommations)` qui prend en paramètre une liste de dictionnaires
consommations représentant les consommations d’un particulier et donnant
sous forme d’un tuple la date et l’année où la consommation a été la plus forte.

On doit avoir par exemple, en considérant l'échantillon précédent de la liste `consommations_martin` :

~~~ {.python}
>>> conso_maximale(consommations_martin)
("19/12", 22)
~~~

~~~ {.python}
def conso_maximale(consommations):
    date = None
    année = None
    res_maxi = 0
    for x in ………………… :
        if ……………………… :
            res_maxi = ………………
            date = …………………
            année = …………………
    return (date, année)
~~~

~~~ {.python .hidden .test .amc file="Q_B_3.md" bareme="1"}
>>> conso_maximale(conso)
('26/12', 22)
~~~

4. Martin aimerait connaître sa consommation totale en 2020 puis en
   2021 puis en 2022 pour voir si d’une année à l’autre, il diminue ou
   augmente sa consommation. Cela lui permettra également de prédire
   sa consommation en 2023. Écrire une fonction
   `somme_conso(consommations, année)` qui prend en paramètre un liste
   de dictionnaires consommations représentant les consommations d’un
   particulier et un entier année et qui renvoie la somme totale des
   consommations en kwH lors de cette année.

À titre d’informations, Martin a obtenu les résultats suivants :

~~~ {.python}
>>> somme_conso(consommations_martin, 20)
2669.3
>>> somme_conso(consommations_martin, 21)
2920.7
>>> somme_conso(consommations_martin, 22)
3174.7
~~~

~~~ {.python .hidden .test .amc file="Q_B_4.md" bareme="1"}
>>> somme_conso(conso, 23)
22.8
>>> somme_conso(conso, 22)
68.7
~~~



5. Grâce à la question précédente, Martin se rend compte que sa
   consommation augmente d’une année sur l’autre. Il se rappelle qu’il a
   acheté plus d’appareils électriques notamment un lave-linge. En
   regardant les offres des fournisseurs d’énergie, il voit une offre
   nommée « ZEN WEEK-END » :
   - 0,1366€ le kwH pendant le week-end ;
   - 0,1912€ le kwH les 5 autres jours de la semaine.  
   Il se demande si cette offre vaut plus le coup que l’offre à 0,1740€ le kwH tout
   le temps sachant qu’il est en effet plus présent à la maison le
   week-end et qu’il pourrait faire tourner ses machines le
   week-end. Pour cela, il aimerait connaître pour chacun des 7 jours
   de la semaine sa consommation totale accumulée en 2022 afin de
   savoir déjà les jours où il consomme le plus.
   
   Écrire une fonction `consommations_jours(consommations)` qui
   renvoie un dictionnaire dont les clés sont les jours de la semaine
   et les valeurs les consommations en kwH accumulées le jour
   correspondant. 
   
Par exemple, Martin a obtenu les résultats suivants :

~~~ {.python}
>>> consommations_jours(consommations_martin)
{'Dimanche': 524.7,
'Jeudi': 406.9,
'Lundi': 446.59999999999997,
'Mardi': 416.7999999999999,
'Mercredi': 433.2,
'Samedi': 475.5999999999999,
'Vendredi': 470.9000000000002}
~~~

~~~ {.python .hidden .test .amc file="Q_B_5.md" bareme="1"}
>>> d = consommations_jours(conso)
>>> d['Lundi']
21.0
>>> d['Vendredi']
9.6
~~~

Cela signifie qu’il a consommé 524.7 kwH les dimanches de 2022, 406.9 kwH les jeudis de 2022 etc

...
Cela confirme le fait que Martin consomme naturellement plus le week-end. S’il se force à faire ses
machines le week-end, il y a des chances que l’offre « ZEN WEEK-END » soit plus intéressante pour
lui ...
