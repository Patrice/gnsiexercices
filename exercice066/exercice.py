def fusion(tab1, tab2):
    """
    Fusionne deux listes rangées dans l'ordre croissant.
    """
    tab_fusion = []
    i1 = 0
    i2 = 0
    # tant qu'il reste des valeurs dans les 2 listes
    while i1 < len(tab1) and i2 < len(tab2):
        if tab1[i1] < tab2[i2]:
            tab_fusion.append(tab1[i1])
            i1 += 1
        else:
            tab_fusion.append(tab2[i2])
            i2 += 1
    # si on a fini de parcourir tab1
    if i1 == len(tab1):
        # on ajoute les valeurs qu'il reste dans tab2
        while i2 < len(tab2):
            tab_fusion.append(tab2[i2])
            i2 += 1
    # si on a fini de parcourir tab1
    else:
        # on ajoute les valeurs qu'il reste dans tab2
        while i1 < len(tab1):
            tab_fusion.append(tab1[i1])
            i1 += 1        

    return tab_fusion
