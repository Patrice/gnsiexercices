# Q1 : quels sont l'isbn, l'éditeur et le titre des collections du niveau Terminale ?
prop_Q1 = ""

# Q2 : quels sont l'isbn, l'éditeur et le titre des collections obsolètes du niveau Terminale ?
prop_Q2 = ""

# Q3 : quels sont les éditeurs ayant fourni des manuels au lycée ?
prop_Q3 = ""

# Q4 : quels sont les isbn et les titres des collections d'espagnol ?
prop_Q4 = ""

# Q5 : quels sont les éditeurs ayant fourni les collections d'histoire ?
prop_Q5 = ""

# Q6 : dans quelles matières utilise t-on les livres de l'éditeur Nathan ?
prop_Q6 = ""

# Q7 : combien chaque éditeur fournit-il de collections ?
prop_Q7 = ""

#  Q8 : combien l'éditeur Hatier fournit-il de collections ?
prop_Q8 = ""

# Q9 : Quels sont les isbn, les éditeurs et les titres des collections achetées en 2005 ?
prop_Q9 = ""

# Q10 : quelles sont les matières correspondant à un enseignement de spécialité ?
prop_Q10 = ""
