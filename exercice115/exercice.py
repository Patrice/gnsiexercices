from math import sqrt

class Sudoku:
    """La classe Sudoku qui stocke la grille sous forme de graphe.
    - l'attribut graphe est un dictionnaire dont la clé est un tuple
    qui correspond à une case et la valeur est également un dictionnaire.
    - l'attribut niveau correspond au dimension des sous-cases. Le sudoku classique est de niveau 3.

    """
    def __init__(self, grille = None):
        """La grille est une liste de listes.  Si la grille n'est pas fournie, on déclare que la grille est de niveau 2 et self.grille est un graphe qui correspond à une grille vide.
        
        """
        if grille is None:
            self.niveau = 2
        else:
            self.niveau = int(sqrt(len(grille)))
        self.graphe = self._graphe_vide(self.niveau)
        if grille is not None:
            for i in range(len(grille)):
                for j in range(len(grille)):
                    self.graphe[(i,j)]['valeur'] = grille[i][j]
                    
    def _graphe_vide(self, n):
        """
        renvoie un graphe vide correspondant aux dimensions
        self.niveau
        """
        g = {}
        # on ajoute les sommets
        for i in range(n**2):
            for j in range(n**2):
                g[(i,j)] = {"valeur":0, "voisins": set()}
        # On ajoute les voisins
        for i in range(n**2):
            for j in range(n**2):
                carre_ligne = (i // n ) * n
                carre_colonne =(j // n) * n
                for k in range(n**2):
                    # on ajoute la colonne
                    if (i,j) != (i,k):
                        g[(i,j)]["voisins"].add((i,k))
                    # on ajoute la ligne
                    if (i,j) != (k,j):
                        g[(i,j)]["voisins"].add((k,j))
                    # on ajoute le carre
                    for k in range(n):
                        for l in range(n):
                            if (i,j) != (carre_ligne + k, carre_colonne + l):
                                g[(i,j)]["voisins"].add((carre_ligne + k,carre_colonne + l))
        return g

    def valeurs_possibles(self,case):
        """
        Renvoie la liste des valeurs possibles pour la case (i,j)
        """
        n = self.niveau
        l = []
        valeurs_impossibles = []
        for voisin in self.graphe[case]['voisins']:
            if self.graphe[voisin]['valeur'] != 0:
                valeurs_impossibles.append(self.graphe[voisin]['valeur'])
        for k in range(1,n**2+1):
            if k not in valeurs_impossibles:
                l.append(k)
        return l

    def case_a_remplir(self):
        """Renvoie la case à remplir de préférence : c'est celle qui
        a le moins de possibilités.
        """
        l = []
        for case in self.graphe.keys():
            if self.graphe[case]['valeur'] == 0:
                l.append([case, self.valeurs_possibles(case)])
        l.sort(key = lambda x: len(x[1]))
        return l[0]

    def est_plein(self):
        """
        Renvoie True si le graphe est plein. False sinon.
        """
        for case in self.graphe:
            if self.graphe[case]['valeur'] == 0:
                return False
        return True
    
    def resolution(self):
        """
        Résous la grille de sudoku.
        """
        pile = []
        # initialisation de la pile
        # on cherche la case à remplir
        # on l'ajoute à la pile
        case_a_remplir = self.case_a_remplir()
        if case_a_remplir[1] !=[]:
            pile.append(case_a_remplir)
        while pile != []:
            # on traite la case en haut de la pile : la case_courante
            case_courante = pile[-1][0]
            # si on n'a plus de valeur possible, c'est qu'on est dans une impasse
            if pile[-1][1] == []:
                # on remet la valeur 0 et on supprime cette case de la pile
                self.graphe[case_courante]['valeur'] = 0
                pile.pop()
            # sinon    
            else:
                # on choisit une valeur à tester
                valeur = pile[-1][1].pop()
                # on la place dans la grille
                self.graphe[case_courante]['valeur'] = valeur
                # si la grille est pleine, on a fini
                if self.est_plein():
                    pile = []
                # sinon on continue l'exploration
                else:
                    case_a_remplir = self.case_a_remplir()
                    if case_a_remplir[1] !=[]:
                        pile.append(case_a_remplir)
  
        
    
    def __repr__(self):
        n = self.niveau
        texte = ""
        for i in range(n**2):
            for j in range(n**2):
                texte += f"({i},{j}) : {self.graphe[(i,j)]['valeur']} -> "
                texte += str(self.graphe[(i,j)]['voisins'])
                texte += "\n"
        return texte

    def __str__(self):
        n = self.niveau
        ligne_horizontale = "+"
        for i in range(n):
            ligne_horizontale += "-"*(n) + "+"
        ligne_horizontale += "\n"
        texte = ligne_horizontale
        for i in range(n**2):
            ligne = ""
            for j in range(n**2):
                if self.graphe[i,j]['valeur'] == 0:
                    v = "."
                else:
                    v = str(self.graphe[i,j]['valeur'])
                if j % n == 0:
                    ligne += "|"
                ligne += v
            ligne += "|\n"
            texte += ligne
            if (i+1) % n == 0:
                texte += ligne_horizontale
        return texte
    
