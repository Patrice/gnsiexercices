def preparer(texte,cle):
    """
    Fonction que renvoie le texte sous forme de liste de phrases composées de
    'cle' caractères.
    """
    texte_a_coder = []
    while len(texte) > 0:
        if len(texte) >= cle:
            texte_a_coder.append(texte[:cle])
            texte = texte[cle:]
        else:
            texte_a_coder.append(texte + " "*(cle-len(texte)))
            texte = ""
    return texte_a_coder


def coder(texte,cle):
    """
    Renvoie le texte crypté.
    * Entrée :
    texte : string, le texte à coder
    cle : int, le nombre de colonnes du texte préparé au format rectangulaire
    * Sortie :
    String
    """
    tac = preparer(texte, cle)
    nl = len(tac)
    tc = ""
    for colonne in range(len(tac[0])):
        for ligne in range(len(tac)):
            tc = tc + tac[ligne][colonne]
    return tc

def cle_de_decryptage(texte,cle):
    return len(preparer(texte,cle))

def liste_paragraphes(fichier):
        lp = []
        f = open(fichier,"r")
        for ligne in f:
            if len(ligne) != 1:
                lp.append(ligne[:-1])
        f.close()
        return lp

def paragraphe_a_dechiffrer(nom):
    n = 0
    for i in nom:
        n += ord(i)
    return n % 65

PRENOM = "Patrice" # rentrez votre prenom
np = paragraphe_a_dechiffrer(PRENOM)
lp = liste_paragraphes("texte_code.txt")
paragraphe_a_dechiffrer = lp[np]
cle_de_dechiffrage = 50    
