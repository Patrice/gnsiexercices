~~~ {.hidden .meta}
classe : première
type : écrit
chapitre : string
thème : cryptographie
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
>>> lc = [23, 17, 12, 22, 29, 30, 25, 48, 17, 19, 15, 9, 8, 12, 44, 34, 56, 50, 27, 20, 24, 27, 17, 55, 28, 26, 13, 45, 22, 34, 68, 140, 43, 98, 17, 13, 129, 20, 18, 33, 29, 32, 127, 14, 22, 67, 70, 48, 166, 48, 42, 41, 28, 57, 202, 45, 54, 31, 144, 84, 58, 18, 50, 43, 28]
~~~

Exercice
========

Voici le texte que nous voulons coder. 


~~~ {.python .all}
>>> texte_a_coder = "À dix-huit ans (1641), Pascal commence le développement de la pascaline, machine à calculer capable d’effectuer des additions et des soustractions20, afin d’aider son père dans son travail. Il en écrit le mode d’emploi : Avis nécessaire à ceux qui auront la curiosité de voir ladite machine et s’en servir. Plusieurs exemplaires sont conservés, en France, au Musée des arts et métiers à Paris et au musée de Clermont-Ferrand."
~~~


## Préparer le texte à coder

Écrire une fonction `preparer(texte,cle)` qui découpe le `texte` en ligne de `cle` caractères. Le résultat sera renvoyé sous forme de **liste** de chaînes de caractères. Veillez bien à ce que la dernière ligne contienne le bon nombre de caractères.


~~~ {.python .test .amc file="Q_1.md" bareme="1"}
>>> solution = ['À dix-huit ans (1', 
...             '641), Pascal comm', 
...             'ence le développe', 
...             'ment de la pascal', 
...             'ine, machine à ca', 
...             'lculer capable d’', 
...             'effectuer des add', 
...             'itions et des sou', 
...             'stractions20, afi', 
...             'n d’aider son pèr', 
...             'e dans son travai', 
...             'l. Il en écrit le', 
...             ' mode d’emploi : ', 
...             'Avis nécessaire à', 
...             ' ceux qui auront ', 
...             'la curiosité de v', 
...             'oir ladite machin', 
...             'e et s’en servir.', 
...             ' Plusieurs exempl', 
...             'aires sont conser', 
...             'vés, en France, a', 
...             'u Musée des arts ', 
...             'et métiers à Pari', 
...             's et au musée de ', 
...             'Clermont-Ferrand.']

>>> assert preparer(texte_a_coder,17) == solution
~~~

~~~ {.python .hidden .test file="Q_1_bis.md" bareme="1"}
>>> solution_1_bis = ['À dix-huit ans (1641),', 
...                   ' Pascal commence le dé', 
...                   'veloppement de la pasc', 
...                   'aline, machine à calcu', 
...                   'ler capable d’effectue', 
...                   'r des additions et des', 
...                   ' soustractions20, afin', 
...                   ' d’aider son père dans', 
...                   ' son travail. Il en éc', 
...                   'rit le mode d’emploi :', 
...                   ' Avis nécessaire à ceu', 
...                   'x qui auront la curios', 
...                   'ité de voir ladite mac', 
...                   'hine et s’en servir. P', 
...                   'lusieurs exemplaires s', 
...                   'ont conservés, en Fran', 
...                   'ce, au Musée des arts ', 
...                   'et métiers à Paris et ', 
...                   'au musée de Clermont-F', 
...                   'errand.               ']
>>> assert preparer(texte_a_coder,22) == solution_1_bis
>>> solution_1_ter = ['À dix-huit', 
...                   ' ans (1641', 
...                   '), Pascal ', 
...                   'commence l', 
...                   'e développ', 
...                   'ement de l', 
...                   'a pascalin', 
...                   'e, machine', 
...                   ' à calcule', 
...                   'r capable ', 
...                   'd’effectue', 
...                   'r des addi', 
...                   'tions et d', 
...                   'es soustra', 
...                   'ctions20, ', 
...                   'afin d’aid', 
...                   'er son pèr', 
...                   'e dans son', 
...                   ' travail. ', 
...                   'Il en écri', 
...                   't le mode ', 
...                   'd’emploi :', 
...                   ' Avis néce', 
...                   'ssaire à c', 
...                   'eux qui au', 
...                   'ront la cu', 
...                   'riosité de', 
...                   ' voir ladi', 
...                   'te machine', 
...                   ' et s’en s', 
...                   'ervir. Plu', 
...                   'sieurs exe', 
...                   'mplaires s', 
...                   'ont conser', 
...                   'vés, en Fr', 
...                   'ance, au M', 
...                   'usée des a', 
...                   'rts et mét', 
...                   'iers à Par', 
...                   'is et au m', 
...                   'usée de Cl', 
...                   'ermont-Fer', 
...                   'rand.     ']
>>> assert preparer(texte_a_coder,10) == solution_1_ter
~~~

## Codage du texte
Écrire une fonction `coder(texte,cle)` qui renvoie le texte crypté. Cette fonction fera appel à la fonction `preparer(texte,cle)`.


~~~ {.python .test .amc file="Q_2.md" bareme="1"}
>>> texte_code_1 = "À6emileisnel A loe avuesC 4nencftt  .mvcai Pié t ld1cneufirdd oie relrsM eei)et,leoa’aIdsuc tue,umtrx,   ecncanle xul ss sé m- ldmrtstis  n rasi eétaohPeea u id edéqid’esneiunua  cceeoesn’cuoieuo  e tisdlhartnro eeistnrnFdrm-tcéaip  s néms ie stresuF av nadd2s cpsat s  as sealepebee0otrlauémeecn àérn la lss,nrioir arxoca erscosàe    atirodcvenerP a opc  asapv  enehims,tadn(mpacddofèal: t irpe sred1mela’duirie à vn.lra i ."
>>> assert coder(texte_a_coder,17) == texte_code_1
~~~

~~~ {.python .hidden .test file="Q_2_bis.md" bareme="1"}
>>> solution_2_bis ="À valr   r xihloceae Pele sdsiA tiuneturdalirdo’otvqénst,  rison euan iu ei  mmaxcpecssi lsid ecaéun-ap,a tdte  eeuoutsdhle parer na trn ié.u mmadaraméuv ssMee iceabdc vocros eur  tonclitsadeoi’erssd  mthetioiesnrexvé e am i ionl st neéeà  nedndon .da l ms  C snee’nsp ’ilasp,dPl  c  es2èIeradel eae (elàf 0rlme iraesrr 1 a fe,e p ctvin im 6l cet  elàueir aso 4epac adno r reFr n 1 altdfa icim.srtet )dscueiné eoa  ast- ,écuesnsc:uscPsn  F "
>>> assert coder(texte_a_coder,22) == solution_2_bis
>>> solution_2_ter = "À )ceeae rdrtecaee Itd serr t esmovauriiuer a,o m ,à ’ istfr tl ’Asuoiveeripnénstessradn mdep  cedo ii dr levaxnoo tveltscésr émnisPménamcafensonsaaeemii tsim iua ,ee seeodx aevtsaapfsson onvn psrq irasrric , e t n.-(sne cclae  usdnsa ml eult c’.sroe dtà dt h1ccldahcbcaes2’  iéoon iaélhe  ennae  ae- u6aeoeliultdtt0apslcdiéà   ainPess usmPu F i4l p inleud r,ièo.re c acddn lx eF  éa Ce t1 lplnee eida drn i :ecuueiesuesrrMatrmlr "
>>> assert coder(texte_a_coder,10) == solution_2_ter
~~~

## La clé de décodage

Alice devra également faire parvenir à Bob la clé de décodage. On rappelle qu'il s'agit du nombre de ligne du texte préparé.

Écrire la fonction `cle_de_decryptage(texte,cle)` qui renvoie la clé de décryptage nécessaire pour décoder le texte.


~~~ {.python .test .amc file="Q_3.md" bareme="1"}
>>> assert cle_de_decryptage(texte_a_coder,17) == 25
>>> tc = "À6emileisnel A loe avuesC 4nencftt  .mvcai Pié t ld1cneufirdd oie relrsM eei)et,leoa’aIdsuc tue,umtrx,   ecncanle xul ss sé m- ldmrtstis  n rasi eétaohPeea u id edéqid’esneiunua  cceeoesn’cuoieuo  e tisdlhartnro eeistnrnFdrm-tcéaip  s néms ie stresuF av nadd2s cpsat s  as sealepebee0otrlauémeecn àérn la lss,nrioir arxoca erscosàe    atirodcvenerP a opc  asapv  enehims,tadn(mpacddofèal: t irpe sred1mela’duirie à vn.lra i ."
>>> texte_decode = coder(tc,25)
>>> assert texte_decode == texte_a_coder
~~~

~~~ {.python .hidden .test file="Q_3_bis.md" bareme="1"}
>>> cle_de_decryptage(texte_a_coder,22)
20
>>> cle_de_decryptage(texte_a_coder,10)
43
~~~

## Votre mission

Le fichier `texte_code.txt` contient un texte crypté. Votre mission
est de décrypter le paragraphe dont l'indice est précisé
ci-dessous. Le premier paragraphe est celui d'indice 0. Le deuxième
paragraphe a pour indice 1.

~~~ {.python}
>>> def paragraphe_a_dechiffrer(nom):
...     n = 0
...     for i in nom:
...         n += ord(i)
...     return n % 65
>>> PRENOM = "" # rentrez votre prenom
>>> np = paragraphe_a_dechiffrer(PRENOM)
~~~

La fonction `liste_paragraphe(fichier)` renvoie la liste de tous les
paragraphes de `fichier`. Si vous parcourez le fichier
`texte_code.txt` vous constaterez qu'un paragraphe se termine par un
`\n` et est séparé d'un autre paragraphe par une ligne vide, qui en
réalité n'est pas vide mais contient uniquement un `\n`.

~~~ {.python}
>>> def liste_paragraphes(fichier):
...         lp = []
...         f = open(fichier,"r")
...         for ligne in f:
...             if len(ligne) != 1:
...                 lp.append(ligne[:-1])
...         f.close()
...         return lp
~~~

Ce codage n'est pas fiable surtout quand on a un casseur de vous en face. Avec un ordinateur il suffit de lui faire afficher le texte décodé en parcourant un ensemble de clé possible. En faisant l'hypothèse que la clé de décodage est comprise entre 2 et 250, déchiffrer le paragraphe que vous devez déchiffrer.


~~~ {.python}
>>> lp = liste_paragraphes("texte_code.txt")
>>> paragraphe_a_dechiffrer = lp[np]
~~~

Préciser, dans votre fichier, la clé de décryptage que vous avez utilisée pour déchiffrer le paragraphe.


~~~ {.python .hidden .test .amc file="Q_4.md" bareme="2"}
>>> assert lc[np] == cle_de_dechiffrage
~~~



