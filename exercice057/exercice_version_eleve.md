Exercice
========

Voici le texte que nous voulons coder.

``` {.python}
>>> texte_a_coder = "À dix-huit ans (1641), Pascal commence le développement de la pascaline, machine à calculer capable d’effectuer des additions et des soustractions20, afin d’aider son père dans son travail. Il en écrit le mode d’emploi : Avis nécessaire à ceux qui auront la curiosité de voir ladite machine et s’en servir. Plusieurs exemplaires sont conservés, en France, au Musée des arts et métiers à Paris et au musée de Clermont-Ferrand."
```

Préparer le texte à coder
-------------------------

Écrire une fonction `preparer(texte,cle)` qui découpe le `texte` en
ligne de `cle` caractères. Le résultat sera renvoyé sous forme de
**liste** de chaînes de caractères. Veillez bien à ce que la dernière
ligne contienne le bon nombre de caractères.

``` {.python}
>>> solution = ['À dix-huit ans (1', 
...             '641), Pascal comm', 
...             'ence le développe', 
...             'ment de la pascal', 
...             'ine, machine à ca', 
...             'lculer capable d’', 
...             'effectuer des add', 
...             'itions et des sou', 
...             'stractions20, afi', 
...             'n d’aider son pèr', 
...             'e dans son travai', 
...             'l. Il en écrit le', 
...             ' mode d’emploi : ', 
...             'Avis nécessaire à', 
...             ' ceux qui auront ', 
...             'la curiosité de v', 
...             'oir ladite machin', 
...             'e et s’en servir.', 
...             ' Plusieurs exempl', 
...             'aires sont conser', 
...             'vés, en France, a', 
...             'u Musée des arts ', 
...             'et métiers à Pari', 
...             's et au musée de ', 
...             'Clermont-Ferrand.']

>>> assert preparer(texte_a_coder,17) == solution
```

Codage du texte
---------------

Écrire une fonction `coder(texte,cle)` qui renvoie le texte crypté.
Cette fonction fera appel à la fonction `preparer(texte,cle)`.

``` {.python}
>>> texte_code_1 = "À6emileisnel A loe avuesC 4nencftt  .mvcai Pié t ld1cneufirdd oie relrsM eei)et,leoa’aIdsuc tue,umtrx,   ecncanle xul ss sé m- ldmrtstis  n rasi eétaohPeea u id edéqid’esneiunua  cceeoesn’cuoieuo  e tisdlhartnro eeistnrnFdrm-tcéaip  s néms ie stresuF av nadd2s cpsat s  as sealepebee0otrlauémeecn àérn la lss,nrioir arxoca erscosàe    atirodcvenerP a opc  asapv  enehims,tadn(mpacddofèal: t irpe sred1mela’duirie à vn.lra i ."
>>> assert coder(texte_a_coder,17) == texte_code_1
```

La clé de décodage
------------------

Alice devra également faire parvenir à Bob la clé de décodage. On
rappelle qu'il s'agit du nombre de ligne du texte préparé.

Écrire la fonction `cle_de_decryptage(texte,cle)` qui renvoie la clé de
décryptage nécessaire pour décoder le texte.

``` {.python}
>>> assert cle_de_decryptage(texte_a_coder,17) == 25
>>> tc = "À6emileisnel A loe avuesC 4nencftt  .mvcai Pié t ld1cneufirdd oie relrsM eei)et,leoa’aIdsuc tue,umtrx,   ecncanle xul ss sé m- ldmrtstis  n rasi eétaohPeea u id edéqid’esneiunua  cceeoesn’cuoieuo  e tisdlhartnro eeistnrnFdrm-tcéaip  s néms ie stresuF av nadd2s cpsat s  as sealepebee0otrlauémeecn àérn la lss,nrioir arxoca erscosàe    atirodcvenerP a opc  asapv  enehims,tadn(mpacddofèal: t irpe sred1mela’duirie à vn.lra i ."
>>> texte_decode = coder(tc,25)
>>> assert texte_decode == texte_a_coder
```

Votre mission
-------------

Le fichier `texte_code.txt` contient un texte crypté. Votre mission est
de décrypter le paragraphe dont l'indice est précisé ci-dessous. Le
premier paragraphe est celui d'indice 0. Le deuxième paragraphe a pour
indice 1.

``` {.python}
>>> def paragraphe_a_dechiffrer(nom):
...     n = 0
...     for i in nom:
...         n += ord(i)
...     return n % 65
>>> PRENOM = "" # rentrez votre prenom
>>> np = paragraphe_a_dechiffrer(PRENOM)
```

La fonction `liste_paragraphe(fichier)` renvoie la liste de tous les
paragraphes de `fichier`. Si vous parcourez le fichier `texte_code.txt`
vous constaterez qu'un paragraphe se termine par un `\n` et est séparé
d'un autre paragraphe par une ligne vide, qui en réalité n'est pas vide
mais contient uniquement un `\n`.

``` {.python}
>>> def liste_paragraphes(fichier):
...         lp = []
...         f = open(fichier,"r")
...         for ligne in f:
...             if len(ligne) != 1:
...                 lp.append(ligne[:-1])
...         f.close()
...         return lp
```

Ce codage n'est pas fiable surtout quand on a un casseur de vous en
face. Avec un ordinateur il suffit de lui faire afficher le texte décodé
en parcourant un ensemble de clé possible. En faisant l'hypothèse que la
clé de décodage est comprise entre 2 et 250, déchiffrer le paragraphe
que vous devez déchiffrer.

``` {.python}
>>> lp = liste_paragraphes("texte_code.txt")
>>> paragraphe_a_dechiffrer = lp[np]
```

Préciser, dans votre fichier, la clé de décryptage que vous avez
utilisée pour déchiffrer le paragraphe.
