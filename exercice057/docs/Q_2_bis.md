    >>> from exercice import *
    >>> lc = [23, 17, 12, 22, 29, 30, 25, 48, 17, 19, 15, 9, 8, 12, 44, 34, 56, 50, 27, 20, 24, 27, 17, 55, 28, 26, 13, 45, 22, 34, 68, 140, 43, 98, 17, 13, 129, 20, 18, 33, 29, 32, 127, 14, 22, 67, 70, 48, 166, 48, 42, 41, 28, 57, 202, 45, 54, 31, 144, 84, 58, 18, 50, 43, 28]

    >>> texte_a_coder = "À dix-huit ans (1641), Pascal commence le développement de la pascaline, machine à calculer capable d’effectuer des additions et des soustractions20, afin d’aider son père dans son travail. Il en écrit le mode d’emploi : Avis nécessaire à ceux qui auront la curiosité de voir ladite machine et s’en servir. Plusieurs exemplaires sont conservés, en France, au Musée des arts et métiers à Paris et au musée de Clermont-Ferrand."

    >>> solution_2_bis ="À valr   r xihloceae Pele sdsiA tiuneturdalirdo’otvqénst,  rison euan iu ei  mmaxcpecssi lsid ecaéun-ap,a tdte  eeuoutsdhle parer na trn ié.u mmadaraméuv ssMee iceabdc vocros eur  tonclitsadeoi’erssd  mthetioiesnrexvé e am i ionl st neéeà  nedndon .da l ms  C snee’nsp ’ilasp,dPl  c  es2èIeradel eae (elàf 0rlme iraesrr 1 a fe,e p ctvin im 6l cet  elàueir aso 4epac adno r reFr n 1 altdfa icim.srtet )dscueiné eoa  ast- ,écuesnsc:uscPsn  F "
    >>> assert coder(texte_a_coder,22) == solution_2_bis
    >>> solution_2_ter = "À )ceeae rdrtecaee Itd serr t esmovauriiuer a,o m ,à ’ istfr tl ’Asuoiveeripnénstessradn mdep  cedo ii dr levaxnoo tveltscésr émnisPménamcafensonsaaeemii tsim iua ,ee seeodx aevtsaapfsson onvn psrq irasrric , e t n.-(sne cclae  usdnsa ml eult c’.sroe dtà dt h1ccldahcbcaes2’  iéoon iaélhe  ennae  ae- u6aeoeliultdtt0apslcdiéà   ainPess usmPu F i4l p inleud r,ièo.re c acddn lx eF  éa Ce t1 lplnee eida drn i :ecuueiesuesrrMatrmlr "
    >>> assert coder(texte_a_coder,10) == solution_2_ter

 
