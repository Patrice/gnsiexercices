~~~ {.hidden .meta}
classe : terminale
type : écrit
chapitre : graphes
thème : parcours
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========

On considère le graphe ci-dessous :

  b---e---f
 / \ / \ /
a   d   g 
 \ /   /
  c   h
  
1. Partant du sommet **b**, proposer deux parcours en largeur de ce graphe.
2. Partant du sommet **b**, proposer deux parcours en profondeur de ce graphe.

~~~ {.python .hidden .amc file="Q_1.md" bareme="2"}
>>> pass
~~~

~~~ {.python .hidden .amc file="Q_2.md" bareme="2"}
>>> pass
~~~


