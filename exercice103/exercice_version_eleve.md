Exercice
========

Compléter le code de la fonction `f(x)` qui correspond à la fonction
$f(x) = 3x + 2$.

``` {.python}
def f(x):
    return .........
```

**Exemple d'appels de la fonction**

``` {.python}
>>> f(0)
2
>>> f(3)
11
>>> f(-1)
-1
```
