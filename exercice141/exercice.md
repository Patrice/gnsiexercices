~~~ {.hidden .meta}
classe : terminale
type : écrit
chapitre : bases de données
thème : parcours
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========

*Cet exercice porte sur l’architecture réseau et des protocoles de communication.*

On souhaite tester un jeu vidéo en mettant en place un réseau d’ordinateurs répartis dans
trois salles.




La figure 1 représente le schéma du réseau d’ordinateurs étudié. Il comprend trois réseaux locaux T1, T2 et T3 dans lesquels vont s’installer les joueurs. Un quatrième réseau local sera utilisé pour un serveur de jeux.

Ces réseaux locaux sont interconnectés grâce à des routeurs R1, R2, R3 et R4. Les réseaux
locaux T1, T2 et T3 sont constitués de plusieurs ordinateurs portables nommés « Portable i », $1 \leqslant i \leqslant 9, et de commutateurs (switchs) nommés S1, S2 et S3.

Le serveur est connecté au routeur (passerelle) R4 par l’intermédiaire du commutateur (switch) S4.

Rappels et notations :

Une adresse IPv4, codée sur 4 octets, doit être associée à un masque de réseau pour être interprétable.

Le masque de réseau :

- permet de distinguer la partie de l'adresse qui identifie un réseau
  de celle qui identifie une machine ;
- est codé sur 4 octets soit 32 bits sous la forme d’une suite de 1
  puis une suite de 0 ;
- peut être indiqué sous forme décimale ou en notation CIDR (Classless Inter-Domain Routing) « $/n$ » où $n$ correspond aux nombres de bits égaux à 1.

Il peut être codé sous la forme 1111 1111.0000 0000.0000 0000.0000 0000 ou en ajoutant « /8 » à l’adresse IP (exemple : 172.16.1.1/8).

Le tableau 1 donne des informations sur les adresses IP de la plupart des éléments constituant le réseau : le nom de l’élément, son type, l’adresse IPv4 de son ou ses interfaces par lesquelles sortent les paquets, l’élément directement relié à l’interface.



| Nom        | Type                | Adresse IP                   | Côté |
|:----------:|:-------------------:|:----------------------------:|:----:|
| R1         | routeur             | Interface 1 : 195.168.1.1/24 | S1   |
|            |                     | Interface 2 : 196.163.2.1/24 | R2   |
|            |                     | Interface 3 : 194.162.1.1/24 | R4   |
| R2         | routeur             | Interface 1 : 197.162.1.1/24 | S2   |
|            |                     | Interface 2 : 196.163.2.2/24 | R1   |
|            |                     | Interface 3 : 198.164.3.1/24 | R3   |
|            |                     | Interface 4 : 193.154.5.1/24 | R4   |
| R3         | routeur             | Interface 1 : .........../24 | S3   |
|            |                     | Interface 2 : .........../24 | R2   |
|            |                     | Interface 3 : .........../24 | R4   |
| R4         | routeur             | Interface 1 : 220.10.1.1/24  | S4   |
|            |                     | Interface 2 : 194.162.1.2/24 | R1   |
|            |                     | Interface 3 : 193.154.5.2/24 | R2   |
|            |                     | Interface 4 : 200.158.4.2/24 | R3   |
| Portable 1 | ordinateur portable | 195.168.1.40/24              | S1   |
| Portable 5 | ordinateur portable | 197.162.1.50/24              | S2   |
| Portable 6 | ordinateur portable | 199.160.1.60/24              | S3   |
| Portable 7 | ordinateur portable | 199.160.1.61/24              | S3   |
| Portable 8 | ordinateur portable | 199.160.1.62/24              | S3   |
| Portable 9 | ordinateur portable | 199.160.1.63/24              | S3   |
| Serveur    | serveur             | 220.10.1.12/24               | S4   |

Tableau 1

1. 

   a. Indiquer l’adresse du réseau local dont fait partie l’ordinateur Portable 3.
   b. Indiquer une adresse possible pour l’ordinateur Portable 3.
   c. Indiquer, en justifiant, le nombre d’adresses encore disponibles pour l’ordinateur Portable 4 du réseau local T2.
  
~~~ {.python .hidden .amc file="Q_1_a.md" bareme="1"}
>>> pass
~~~

~~~ {.python .hidden .amc file="Q_1_b.md" bareme="1"}
>>> pass
~~~

~~~ {.python .hidden .amc file="Q_1_c.md" bareme="1"}
>>> pass
~~~

  
2. Indiquer les adresses IP des interfaces du routeur R3. Chaque adresse IP est la première disponible sur la plage d’adressage de chaque réseau connecté.

~~~ {.python .hidden .amc file="Q_2.md" bareme="1"}
>>> pass
~~~


3. Lors du jeu, l’ordinateur Portable 1 veut communiquer avec l’ordinateur Portable 5.

   a. Indiquer trois parcours possibles en listant tous les éléments utilisés du réseau.
   b. Indiquer le plus court chemin au sens du protocole RIP, minimisant le nombre de
routeurs traversés (sauts), en précisant son nombre de sauts.
   c. La liaison R1-R2 vient de se rompre. Indiquer le plus court chemin au sens du protocole RIP, minimisant le nombre de routeurs traversés.
   
~~~ {.python .hidden .amc file="Q_3_a.md" bareme="1"}
>>> pass
~~~

~~~ {.python .hidden .amc file="Q_3_b.md" bareme="1"}
>>> pass
~~~

~~~ {.python .hidden .amc file="Q_3_c.md" bareme="1"}
>>> pass
~~~

   
4. La liaison R1-R2 entre les deux réseaux locaux est rétablie en
   changeant le câble de raccordement. Parmi les quatre propositions
   suivantes, indiquer le type de ce câble.

	a. Internet
    b. VGA
	c. Ethernet
	d. HDMI

~~~ {.python .hidden .amc file="Q_4.md" bareme="1"}
>>> pass
~~~


5. On souhaite déterminer le parcours minimisant le coût total des liaisons traversées. Pour cela, le protocole OSPF est utilisé.
On s’intéresse donc au parcours à moindre coût entre le routeur R1 et le routeur R2.  Étant donné une bande passante de référence de $10^9$ bps (bits par seconde), le coût d’une liaison entre routeurs est donné par la formule suivante où $d$ est la bande passante en bps entre les deux routeurs.

$$\text{coût} = \dfrac{10^9}{d}$$

On admet que si le résultat du quotient précédent est inférieur ou égal à 1, le coût est égal à 1.

On donne dans le tableau ci-dessous les débits des liaisons entre routeurs.
Note : Mbps signifie 106 bits par seconde. 


| Liaison       | R1-R2 | R1-R4 | R2-R3 | R2-R4 | R3-R4 |
|:-------------:|:-----:|:-----:|:-----:|:-----:|:-----:|
| Débit en Mbps | ?     | 1000  | 10    | 1000  | 20    |



a. La liaison entre le routeur R1 et R2 a un coût de 10.
Calculer le débit de cette liaison en Mbps.
b. L’ordinateur Portable 1 communique avec l’ordinateur Portable 5. Le routeur R1 doit transmettre les paquets au routeur R2.
Déterminer le chemin de meilleur coût et indiquer ce coût. Justifier les réponses.

~~~ {.python .hidden .amc file="Q_5_a.md" bareme="1"}
>>> pass
~~~

~~~ {.python .hidden .amc file="Q_5_b.md" bareme="1"}
>>> pass
~~~
