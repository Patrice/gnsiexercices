Exercice
========

La classe `Filtre` est utilisée dans le cadre du jeu du démineur. Elle
cache une grille de cases, certaines étant des bombes, et permet de
découvrir des cases, de gérer la logique du jeu et de vérifier si la
partie est gagnée ou non.

1.  Compléter le code de la méthode `decouvrir_case(self, x, y)` qui
    découvre la case située aux coordonnées `(x, y)`. Si la case
    contient une bombe, cela renvoie `True`, `False` sinon. De plus, si
    la case est vide, elle découvre toutes les cases adjacentes vides.

``` {.python}
>>> from random import seed
>>> seed(0)  # Pour la reproductibilité
>>> g = Bombe()
>>> print(grille_to_string(g.grille))
+-+-+-+-+-+-+-+-+-+-+
| | | | | | | | | | |
+-+-+-+-+-+-+-+-+-+-+
| | | |1|1|1| |1|1|1|
+-+-+-+-+-+-+-+-+-+-+
| | | |1|X|1| |1|X|2|
+-+-+-+-+-+-+-+-+-+-+
|1|1| |1|1|2|1|2|2|X|
+-+-+-+-+-+-+-+-+-+-+
|X|1| | | |1|X|2|2|1|
+-+-+-+-+-+-+-+-+-+-+
|1|1| | | |2|3|X|1| |
+-+-+-+-+-+-+-+-+-+-+
| | | | | |1|X|3|2|1|
+-+-+-+-+-+-+-+-+-+-+
| | | |1|1|2|1|2|X|1|
+-+-+-+-+-+-+-+-+-+-+
|1|1|1|1|X|1| |1|1|1|
+-+-+-+-+-+-+-+-+-+-+
|1|X|1|1|1|1| | | | |
+-+-+-+-+-+-+-+-+-+-+
>>> f = Filtre(g)
>>> print(grille_to_string(f.grille))
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
>>> f.decouvrir_case(3, 1)
False
>>> f.decouvrir_case(4, 2)
True
>>> f.decouvrir_case(0, 0)
False
```

2.  Compléter le code de la méthode `mise_a_jour(x, y)` qui découvre les
    cases vides adjacentes à la case `(x, y)`. Elle est appelée de
    manière récursive pour découvrir toute une zone de vide.

``` {.python}
>>> from random import seed
>>> seed(0)  # Pour la reproductibilité
>>> g = Bombe()
>>> f = Filtre(g)
>>> print(grille_to_string(f.grille))
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
>>> f.decouvrir_case(3, 1)
False
>>> print(grille_to_string(f.grille))
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X| |X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
>>> f.decouvrir_case(0, 0)
False
>>> print(grille_to_string(f.grille))
+-+-+-+-+-+-+-+-+-+-+
| | | | | | | | | | |
+-+-+-+-+-+-+-+-+-+-+
| | | | | | | | | | |
+-+-+-+-+-+-+-+-+-+-+
| | | | |X| | | |X|X|
+-+-+-+-+-+-+-+-+-+-+
| | | | | | | | |X|X|
+-+-+-+-+-+-+-+-+-+-+
|X| | | | | |X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
| | | | | | |X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
| | | | | | |X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
| | | | | | |X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
| | | | |X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X|X|X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
>>> f.decouvrir_case(9, 9)
False
>>> print(grille_to_string(f.grille))
+-+-+-+-+-+-+-+-+-+-+
| | | | | | | | | | |
+-+-+-+-+-+-+-+-+-+-+
| | | | | | | | | | |
+-+-+-+-+-+-+-+-+-+-+
| | | | |X| | | |X|X|
+-+-+-+-+-+-+-+-+-+-+
| | | | | | | | |X|X|
+-+-+-+-+-+-+-+-+-+-+
|X| | | | | |X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
| | | | | | |X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
| | | | | | |X|X|X|X|
+-+-+-+-+-+-+-+-+-+-+
| | | | | | | | |X|X|
+-+-+-+-+-+-+-+-+-+-+
| | | | |X| | | | | |
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X| | | | | |
+-+-+-+-+-+-+-+-+-+-+
>>> f.decouvrir_case(9, 5)
False
>>> print(grille_to_string(f.grille))
+-+-+-+-+-+-+-+-+-+-+
| | | | | | | | | | |
+-+-+-+-+-+-+-+-+-+-+
| | | | | | | | | | |
+-+-+-+-+-+-+-+-+-+-+
| | | | |X| | | |X|X|
+-+-+-+-+-+-+-+-+-+-+
| | | | | | | | |X|X|
+-+-+-+-+-+-+-+-+-+-+
|X| | | | | |X|X| | |
+-+-+-+-+-+-+-+-+-+-+
| | | | | | |X|X| | |
+-+-+-+-+-+-+-+-+-+-+
| | | | | | |X|X| | |
+-+-+-+-+-+-+-+-+-+-+
| | | | | | | | |X|X|
+-+-+-+-+-+-+-+-+-+-+
| | | | |X| | | | | |
+-+-+-+-+-+-+-+-+-+-+
|X|X|X|X|X| | | | | |
+-+-+-+-+-+-+-+-+-+-+
```

3.  Compléter le code de la méthode `partie_gagnee()` qui vérifie si
    toutes les cases de la grille ont été découvertes (c'est-à-dire que
    toutes les cases cachées sont non cachées excepté celles qui
    contiennent des bombes).

``` {.python}
>>> from random import seed
>>> seed(0)  # Pour la reproductibilité
>>> g = Bombe()
>>> f = Filtre(g)
>>> f.decouvrir_case(0, 0)
False
>>> f.decouvrir_case(9, 9)
False
>>> f.decouvrir_case(9, 5)
False
>>> f.decouvrir_case(9, 2)
False
>>> f.decouvrir_case(8, 3)
False
>>> f.decouvrir_case(7, 4)
False
>>> f.decouvrir_case(6, 5)
False
>>> f.decouvrir_case(7, 6)
False
>>> f.decouvrir_case(9, 7)
False
>>> f.decouvrir_case(0, 9)
False
>>> f.decouvrir_case(2, 9)
False
>>> f.decouvrir_case(3, 9)
False
>>> f.partie_gagnee()
False
>>> f.decouvrir_case(4, 9)
False
>>> f.partie_gagnee()
True
```
