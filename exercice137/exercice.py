from constantes import *
from bombe import *

class Filtre:
    """
    La classe Filtre cache la grille contenant les bombes.
    """
    def __init__(self, grille_bombes):
        """
        La classe Filtre est liée à une grille contenant les bombes.
        Au début de la partie la grille contient uniquement de valeur True,
        pour signifier que la case est cachée.
        """
        self.grille_bombes = grille_bombes
        g = nouvelle_grille(True)
        self.grille = g
        
    def afficher(self):
        """
        On dessine une case grise si la valeur du filtre est True.
        """
        for i in range(NB_CASES):
            for j in range(NB_CASES):
                if self.grille[i][j] :
                    fill(200,200,200)
                    rect(j *TAILLE_CASE, i * TAILLE_CASE, TAILLE_CASE, TAILLE_CASE) 
        
    def decouvrir_case(self,x,y):
        """
        Découvre la case (x,y).
        Renvoie True si on tombe sur une bombe.
        Si on tombe sur une case vide, on découvre toute la zone vide
        adjacente et on renvoie False.
        """
        self.grille[y][x] = False
        if self.grille_bombes.grille[y][x] == 10:
            return True
        if self.grille_bombes.grille[y][x] == 0:
            self.mise_a_jour(x, y)
        return False
        
    def mise_a_jour(self,x,y):
        """
        Découvre une zone de vide.
        """
        for k in range(-1, 2):                 
            for l in range(-1, 2):
                if 0<=y+k < NB_CASES and 0<=x+l < NB_CASES:
                    if self.grille[y+k][x+l] and self.grille_bombes.grille[y+k][x+l] == 0 :
                        self.grille[y+k][x+l] = False
                        self.mise_a_jour(x+l, y + k)
                    self.grille[y+k][x+l] = False    
                    
    def partie_gagnee(self):
        """
        Renvoie True si toutes les cases sont découvertes.
        """
        s = 0
        for i in range(NB_CASES):
            for j in range(NB_CASES):
                if self.grille[i][j]:
                    s += 1
        return s == NB_BOMBES
