from random import randint
from constantes import *

class Bombe:
    def __init__(self):
        self.grille= nouvelle_grille(0)
        self.placer_les_bombes()
        self.completer_les_cases()

    def placer_les_bombes(self):
        """
        Ajoute NB_BOMBES bombes à la self.grille
        """
        n = 0
        while n < NB_BOMBES:
            x = randint(0, NB_CASES - 1)
            y = randint(0, NB_CASES - 1)
            if self.grille[y][x] != 10:
                n += 1
                self.grille[y][x] = 10

    def completer_les_cases(self):
        g = self.grille
        for i in range(NB_CASES):
            for j in range(NB_CASES):
                if g[i][j] != 10:
                    s = 0
                    for k in range(-1, 2):                 
                        for l in range(-1, 2):
                            if 0<=i+k < NB_CASES and 0<=j+l < NB_CASES and (k,l) != (0,0):
                                if g[i+k][j+l] == 10:
                                    s += 1
                    g[i][j] = s
        
    def afficher(self):
        """
        Affiche la grille dans Processing.
        """
        for i in range(NB_CASES):
            for j in range(NB_CASES):
                if self.grille[i][j] == 10:
                    fill(255,0,0)
                    ellipse((j+0.5)* TAILLE_CASE, (i+0.5)* TAILLE_CASE, 0.8*TAILLE_CASE, 0.8*TAILLE_CASE) 
                else:
                    fill(0,0,255)
                    textSize(TAILLE_CASE//2)
                    if self.grille[i][j] != 0:
                        text(str(self.grille[i][j]), (j+0.5)* TAILLE_CASE, (i+0.5)* TAILLE_CASE )
            
