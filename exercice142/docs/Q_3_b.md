    >>> from exercice import *
    >>> import sqlite3 
    >>> conn = sqlite3.connect('bdd142.sqlite')
    >>> def initialisation():
    ...     """
    ...     Fonction qui vide la table des coureurs si elle existe puis en crée une nouvelle.
    ...     """
    ... 
    ...     curseur = conn.cursor()
    ... 
    ...     curseur.executescript("""
    ...     DROP TABLE IF EXISTS "interpretes";
    ...     CREATE TABLE IF NOT EXISTS "interpretes" (
    ...         "id_interprete" INTEGER,
    ...         "nom"   TEXT,
    ...         "pays"  TEXT
    ...     );
    ...     DROP TABLE IF EXISTS "morceaux";
    ...     CREATE TABLE IF NOT EXISTS "morceaux" (
    ...         "id_morceau"    INTEGER,
    ...         "titre" TEXT,
    ...         "annee" INTEGER,
    ...         "id_interprete" INTEGER
    ...     );
    ...     INSERT INTO "interpretes" ("id_interprete","nom","pays") VALUES (1,'Bob Dylan','États-Unis');
    ...     INSERT INTO "interpretes" ("id_interprete","nom","pays") VALUES (2,'Aretha Franklin','États-Unis');
    ...     INSERT INTO "interpretes" ("id_interprete","nom","pays") VALUES (3,'John Lennon','Angleterre');
    ...     INSERT INTO "interpretes" ("id_interprete","nom","pays") VALUES (4,'The Beatles','Angleterre');
    ...     INSERT INTO "interpretes" ("id_interprete","nom","pays") VALUES (5,'Nirvana','États-Unis');
    ...     INSERT INTO "morceaux" ("id_morceau","titre","annee","id_interprete") VALUES (1,'Like a Rolling Stone',1965,1);
    ...     INSERT INTO "morceaux" ("id_morceau","titre","annee","id_interprete") VALUES (2,'Respect',1967,2);
    ...     INSERT INTO "morceaux" ("id_morceau","titre","annee","id_interprete") VALUES (3,'Imagine',1970,3);
    ...     INSERT INTO "morceaux" ("id_morceau","titre","annee","id_interprete") VALUES (4,'Hey Jude',1968,4);
    ...     INSERT INTO "morceaux" ("id_morceau","titre","annee","id_interprete") VALUES (5,'Smells Like Teen Spirit',1991,5);
    ...     INSERT INTO "morceaux" ("id_morceau","titre","annee","id_interprete") VALUES (6,'I Want To hold Your Hand',1963,4);
    ...     """)
    ...     conn.commit()

    >>> def requete_correcte(solution,proposition):
    ...     # Créer un cursor 
    ...     cur = conn.cursor() 
    ...     comparaison = "WITH TA AS ({}), TB AS ({}) SELECT * FROM TA EXCEPT SELECT * FROM TB UNION ALL SELECT * FROM TB EXCEPT SELECT * FROM TA;".format(solution, proposition)
    ...     # Exécution de la requete
    ...     try:
    ...         resultat = cur.execute(comparaison) 
    ...         row = cur.fetchone()
    ...         if row is None:
    ...             rc = True
    ...         else:
    ...             rc = False
    ...     except Exception:
    ...         print("Le nombre de colonnes est incorrect !")
    ...         rc = False
    ...     # Envoyer la requete 
    ...     conn.commit()
    ...     # Fermer la connexion 
    ...     conn.close()
    ...     return rc

    >>> solution_Q1b = "SELECT nom FROM interpretes WHERE pays='Angleterre'"
    >>> solution_Q1d = "SELECT COUNT(*) FROM morceaux"
    >>> solution_Q1e = "SELECT titre FROM morceaux ORDER BY titre ASC"
    >>> solution_Q3a = "UPDATE morceaux SET annee = 1971 WHERE id_morceau = 3"
    >>> solution_Q3b = "INSERT INTO interpretes VALUES (6, 'The Who', 'Angleterre')"
    >>> solution_Q3c = "INSERT INTO morceaux VALUES (7, 'My Generation', 1965, 6)"
    >>> solution_Q4 = "SELECT titre from morceaux JOIN interpretes ON morceaux.id_interprete = interpretes.id_interprete WHERE interpretes.pays = 'Etats-Unis'"

    >>> cur = conn.cursor() 
    >>> r1 = cur.execute(prop_Q3b)
    >>> r2 = cur.execute("SELECT nom FROM interpretes WHERE id_interprete=6")
    >>> resultat = cur.fetchone()
    >>> conn.commit()
    >>> conn.close()
    >>> resultat[0]
    'The Who'

 
