"""
Écrire vos requêtes dans les variables ci-dessous.

**Attention**, 

- les attributs doivent impérativement être **dans l'ordre de la question** ; 
- n'utiliser que **des simples quotes '** et non des " ;
- surtout vous ne mettez **pas de point-virgule**.
- **Ne changez pas le nom de la variable !**
- Pour les réponses aux questions, ne pas mettre d'espaces superflus.
"""

# Question 1.a
# Choisir la bonne réponse : "a", "b" ou "c".
# a. Like a Rolling Stone
# b. Respect
# c. Hey Jude
# d. Hey Jude et I Want To hold Your Hand

reponse_Q1a = ""

# Question 1.b

prop_Q1b = ""

# Question 1.c
# Choisir la bonne réponse : "a", "b" ou "c".
#################################
# Proposition a
#   Like a Rolling Stone    , 1965
#   Respect                 , 1967
#   Imagine                 , 1970
#   Hey Jude                , 1968
#   Smells Like Teen Spirit , 1991
#   I Want To hold Your Hand, 1963
##################################
# Proposition b
#   I Want To hold Your Hand, 1963
#   Like a Rolling Stone    , 1965
#   Respect                 , 1967
#   Hey Jude                , 1968
#   Imagine                 , 1970
#   Smells Like Teen Spirit , 1991
##################################
# Proposition c
#   Smells Like Teen Spirit , 1991
#   Imagine                 , 1970
#   Hey Jude                , 1968
#   Respect                 , 1967
#   Like a Rolling Stone    , 1965
#   I Want To hold Your Hand, 1963
##################################

reponse_Q1c = ""


# Question 1.d

prop_Q1d = ""

# Question 1.e

prop_Q1e = ""


# Question 2.a
# Préciser l'attribut de la clé étrangère.

cle_etrangere=""

# Question 2.b
# Compléter les valeurs de att1, att2, type1 et type2. 
# +---------------+      +---------------+
# | Morceaux      |      | Interpretes   |
# +---------------|      +---------------+
# | att1 : int    |  +-->| att2 : type1  |
# | titre : text  |  |   | nom : text    |
# | annee : int   |  |   | att2 : type2  |
# | att2 : type1  |--+   +---------------+
# +---------------+

att1 = ""
att2 = ""
type1 = ""
type2 = ""

# Question 2.c
# Choisir la bonne réponse : "a", "b" ou "c".
# a. id_morceau est un clé primaire. Elle est donc unique. On ne peut insérer une autre valeur 1 dans la table.
# b. La valeur 1 étant déjà utilisée, il aurait fallu écrire : UPDATE INTO interpretes VALUES (1, 'Trust', 'France');
# c. Trust est un groupe anglais.

reponse_Q2c = ""

# Question 3.a
prop_Q3a = ""

# Question 3.b
prop_Q3b = ""


# Question 3.c
prop_Q3c = ""

# Question 4
prop_Q4 = ""
