~~~ {.hidden .meta}
classe : terminale
type : écrit
chapitre : bases de données
thème : requete
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
>>> import sqlite3 
>>> conn = sqlite3.connect('bdd142.sqlite')
>>> def initialisation():
...     """
...     Fonction qui vide la table des coureurs si elle existe puis en crée une nouvelle.
...     """
... 
...     curseur = conn.cursor()
... 
...     curseur.executescript("""
...     DROP TABLE IF EXISTS "interpretes";
...     CREATE TABLE IF NOT EXISTS "interpretes" (
...     	"id_interprete"	INTEGER,
...     	"nom"	TEXT,
...     	"pays"	TEXT
...     );
...     DROP TABLE IF EXISTS "morceaux";
...     CREATE TABLE IF NOT EXISTS "morceaux" (
...     	"id_morceau"	INTEGER,
...     	"titre"	TEXT,
...     	"annee"	INTEGER,
...     	"id_interprete"	INTEGER
...     );
...     INSERT INTO "interpretes" ("id_interprete","nom","pays") VALUES (1,'Bob Dylan','États-Unis');
...     INSERT INTO "interpretes" ("id_interprete","nom","pays") VALUES (2,'Aretha Franklin','États-Unis');
...     INSERT INTO "interpretes" ("id_interprete","nom","pays") VALUES (3,'John Lennon','Angleterre');
...     INSERT INTO "interpretes" ("id_interprete","nom","pays") VALUES (4,'The Beatles','Angleterre');
...     INSERT INTO "interpretes" ("id_interprete","nom","pays") VALUES (5,'Nirvana','États-Unis');
...     INSERT INTO "morceaux" ("id_morceau","titre","annee","id_interprete") VALUES (1,'Like a Rolling Stone',1965,1);
...     INSERT INTO "morceaux" ("id_morceau","titre","annee","id_interprete") VALUES (2,'Respect',1967,2);
...     INSERT INTO "morceaux" ("id_morceau","titre","annee","id_interprete") VALUES (3,'Imagine',1970,3);
...     INSERT INTO "morceaux" ("id_morceau","titre","annee","id_interprete") VALUES (4,'Hey Jude',1968,4);
...     INSERT INTO "morceaux" ("id_morceau","titre","annee","id_interprete") VALUES (5,'Smells Like Teen Spirit',1991,5);
...     INSERT INTO "morceaux" ("id_morceau","titre","annee","id_interprete") VALUES (6,'I Want To hold Your Hand',1963,4);
...     """)
...     conn.commit()

>>> def requete_correcte(solution,proposition):
...     # Créer un cursor 
...     cur = conn.cursor() 
...     comparaison = "WITH TA AS ({}), TB AS ({}) SELECT * FROM TA EXCEPT SELECT * FROM TB UNION ALL SELECT * FROM TB EXCEPT SELECT * FROM TA;".format(solution, proposition)
...     # Exécution de la requete
...     try:
...         resultat = cur.execute(comparaison) 
...         row = cur.fetchone()
...         if row is None:
...             rc = True
...         else:
...             rc = False
...     except Exception:
...         print("Le nombre de colonnes est incorrect !")
...         rc = False
...     # Envoyer la requete 
...     conn.commit()
...     # Fermer la connexion 
...     conn.close()
...     return rc

>>> solution_Q1b = "SELECT nom FROM interpretes WHERE pays='Angleterre'"
>>> solution_Q1d = "SELECT COUNT(*) FROM morceaux"
>>> solution_Q1e = "SELECT titre FROM morceaux ORDER BY titre ASC"
>>> solution_Q3a = "UPDATE morceaux SET annee = 1971 WHERE id_morceau = 3"
>>> solution_Q3b = "INSERT INTO interpretes VALUES (6, 'The Who', 'Angleterre')"
>>> solution_Q3c = "INSERT INTO morceaux VALUES (7, 'My Generation', 1965, 6)"
>>> solution_Q4 = "SELECT titre from morceaux JOIN interpretes ON morceaux.id_interprete = interpretes.id_interprete WHERE interpretes.pays = 'Etats-Unis'"


~~~

Exercice
========

*Cet exercice porte sur les bases de données relationnelles et le langage SQL.*

L’énoncé de cet exercice utilise les mots clefs du langage SQL suivants : `SELECT`, `FROM`, `WHERE`, `JOIN ON`, `UPDATE`, `SET`, `INSERT INTO VALUES`, `COUNT`, `ORDER BY`.

La clause `ORDER BY` suivie d'un attribut permet de trier les résultats par ordre
croissant de l'attribut ;

`COUNT(*)` renvoie le nombre de lignes d'une requête ;

Un musicien souhaite créer une base de données relationnelle contenant ses
morceaux et interprètes préférés. Pour cela il utilise le langage SQL. Il crée une table
`morceaux` qui contient entre autres les titres des morceaux et leur année de sortie :

| id_morceau | titre                    | annee | id_interprete |
|:----------:|--------------------------|:-----:|:-------------:|
| 1          | Like a Rolling Stone     | 1965  | 1             |
| 2          | Respect                  | 1967  | 2             |
| 3          | Imagine                  | 1970  | 3             |
| 4          | Hey Jude                 | 1968  | 4             |
| 5          | Smells Like Teen Spirit  | 1991  | 5             |
| 6          | I Want To hold Your Hand | 1963  | 4             |


Il crée la table `interpretes` qui contient les interprètes et leur pays d'origine : 

| id_interprete | nom             | pays       |
|:-------------:|-----------------|:----------:|
| 1             | Bob Dylan       | États-Unis |
| 2             | Aretha Franklin | États-Unis |
| 3             | John Lennon     | Angleterre |
| 4             | The Beatles     | Angleterre |
| 5             | Nirvana         | États-Unis |


`id_morceau` de la table `morceaux` et `id_interprete` de la table `interpretes` sont des clés primaires.

L’attribut `id_interprete` de la table `morceaux` fait directement référence à la clé primaire de la table `interpretes`.

1. a. Écrire le résultat de la requête suivante :
   `SELECT titre FROM morceaux WHERE id_interprete = 4;`
   b. Écrire une requête permettant d'afficher les noms des interprètes originaires d'Angleterre.
   c. Écrire le résultat de la requête suivante :
   `SELECT titre, annee FROM morceaux ORDER BY annee;`
   d. Écrire une requête permettant de calculer le nombre de morceaux dans la table `morceaux`.
   e. Écrire une requête affichant les titres des morceaux par ordre alphabétique.
   
~~~ {.python .hidden .test .amc file="Q_1_a.md" bareme="1"}
>>> reponse_Q1a
'd'
>>> initialisation()
~~~

~~~ {.python .hidden .test .amc file="Q_1_b.md" bareme="1"}
>>> assert requete_correcte(solution_Q1b, prop_Q1b)
~~~

~~~ {.python .hidden .test .amc file="Q_1_c.md" bareme="1"}
>>> reponse_Q1c
'b'
~~~

~~~ {.python .hidden .test .amc file="Q_1_d.md" bareme="1"}
>>> assert requete_correcte(solution_Q1d, prop_Q1d)
~~~

~~~ {.python .hidden .test .amc file="Q_1_e.md" bareme="1"}
>>> assert requete_correcte(solution_Q1e, prop_Q1e)
~~~
   
2. a. Citer, en justifiant, la clé étrangère de la table `morceaux`.
   b. Écrire un schéma relationnel des tables interpretes et morceaux.
   c. Expliquer pourquoi la requête suivante produit une erreur :
   `INSERT INTO interpretes VALUES (1, 'Trust', 'France');`

~~~ {.python .hidden .test .amc file="Q_2_a.md" bareme="1"}
>>> cle_etrangere
'id_interprete'
~~~

~~~ {.python .hidden .test .amc file="Q_2_b.md" bareme="1"}
>>> att1
'id_morceau'
>>> att2
'id_interprete'
>>> type1
'int'
>>> type2
'text'
~~~

~~~ {.python .hidden .test .amc file="Q_2_c.md" bareme="1"}
>>> reponse_Q2c
'a'
~~~

3. a. Une erreur de saisie a été faite. Écrire une requête SQL
   permettant de changer l’année du titre « Imagine » en 1971.
   b. Écrire une requête SQL permettant d'ajouter l'interprète « The
   Who » venant d'Angleterre à la table interpretes. On lui donnera un
   `id_interprete` égal à 6.
   c. Écrire une requête SQL permettant d'ajouter le titre « My
   Generation » de « The Who » à la table morceaux. Ce titre est sorti
   en 1965 et on lui donnera un `id_morceau` de 7 ainsi que
   l'`id_interprete` qui conviendra.
   
~~~ {.python .hidden .test .amc file="Q_3_a.md" bareme="1"}
>>> cur = conn.cursor() 
>>> r1 = cur.execute(prop_Q3a)
>>> r2 = cur.execute("SELECT annee FROM morceaux WHERE id_morceau=3")
>>> resultat = cur.fetchone()
>>> conn.commit()
>>> conn.close()
>>> resultat[0]
1971
~~~

~~~ {.python .hidden .test .amc file="Q_3_b.md" bareme="1"}
>>> cur = conn.cursor() 
>>> r1 = cur.execute(prop_Q3b)
>>> r2 = cur.execute("SELECT nom FROM interpretes WHERE id_interprete=6")
>>> resultat = cur.fetchone()
>>> conn.commit()
>>> conn.close()
>>> resultat[0]
'The Who'
~~~

~~~ {.python .hidden .test .amc file="Q_3_c.md" bareme="1"}
>>> cur = conn.cursor() 
>>> r1 = cur.execute(prop_Q3c)
>>> r2 = cur.execute("SELECT * FROM morceaux WHERE id_interprete=6")
>>> resultat = cur.fetchone()
>>> conn.commit()
>>> conn.close()
>>> resultat[0]
7
>>> resultat[1]
'My Generation'
>>> resultat[2]
1965
>>> resultat[3]
6
~~~
   
4. Écrire une requête permettant de lister les titres des interprètes venant des États-Unis.

~~~ {.python .hidden .test .amc file="Q_4.md" bareme="2"}
>>> assert requete_correcte(solution_Q4, prop_Q4)
~~~
