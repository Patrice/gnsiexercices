    >>> from exercice import *
    >>> br = Region('Bretagne')
    >>> nr = Region('Normandie')
    >>> pl = Region('Pays de Loire')
    >>> ce = Region('Centre')
    >>> ga = Region('Grande Aquitaine')
    >>> ge = Region('Grand Est')
    >>> br.tab_voisines = [nr, pl]
    >>> pl.tab_voisines = [br, nr, ce, ga]
    >>> nr.tab_voisines = [br,pl,ce]
    >>> ga.tab_voisines = [pl,ce]
    >>> ce.tab_voisines = [pl, ga, nr]
    >>> ou = Pays('Ouest')
    >>> ou.tab_regions = [br, nr, ce, pl, ga]

    >>> ## les couleurs sont differentes de celles des voisins
    >>> ou.colorie()
    >>> for r in ou.tab_regions:
    ...    for v in r.tab_voisines:
    ...        assert r.couleur_attribuee != v.couleur_attribuee

 
