class Region:
    """Modélise une région d'un pays sur une carte."""
    def __init__(self, nom_region):
        """
	initialise une région
	: param nom_region (str) le nom de la région
	"""
        self.nom = nom_region
	# tableau des régions voisines, vide au départ
	self.tab_voisines = []
	# tableau des couleurs disponibles pour colorier la région
	self.tab_couleurs_disponibles = ['rouge', 'vert', 'bleu', 'jaune']
	# couleur attribuée à la région et non encore choisie au départ
	self.couleur_attribuee = None

    def renvoie_premiere_couleur_disponible(self):
        return ...

    def renvoie_nb_voisines(self) :
        return ...

    def est_coloriee(self):
        return ...

    def retire_couleur(self, couleur):
        return...

    def est_voisine(self, region):
        """
            Renvoie True si la region passée en paramètre est une voisine et False sinon.
        """
        pass

class Pays:
    def __init__(self, nom):
        """
        Modélise la carte d'un pays composé de régions.
        """
        self.tab_regions = []

    def renvoie_tab_regions_non_coloriees(self):
        """"
	Renvoie un tableau dont les éléments sont les régions du pays sans couleur attribuée.
	: return (list) tableau d’instances de la classe Region
	"""
        pass

    def colorie(self):
        pass

    def renvoie_max(self):
        nb_voisines_max = -1
        region_max = None
        for reg in self.renvoie_tab_regions_non_coloriees():
            if reg.renvoie_nb_voisines() > nb_voisines_max:
                nb_voisines_max = reg.renvoie_nb_voisines()
                region_max = reg
        return region_max
    
## Partie 1

# Q1
# Associer, en vous appuyant sur l’extrait de code précédent, les noms `nom`, `tab_voisines`, `tab_couleurs_disponibles` et `couleur_attribuee` au terme qui leur correspond parmi : "objet", "attribut", "méthode" ou "classe".

nom = ""
tab_voisines =  ""
tab_couleurs_disponibles = ""


# Q2
type_nom_region = "..."

# Q3
ge = ...

## Partie 2

# Q_10
# a. Choisir la bonne réponse parmi les trois propositions suivantes :
# 1. La méthode renvoie None dans le cas ou tout est colorié.
# 2. La méthode renvoie None quand la région n'a plus de couleur disponible.
# 3. La méthode renvoie None dans le cas ou la région est déjà coloriée.

reponse_Q_10_a = ""

# b. Choisir la bonne réponse parmi les trois propositions suivantes :
# 1. La région renvoyée est la région qui a le plus de couleurs disponibles.  Si le région n'a plus de couleur disponible, renvoie None.

# 2. La région renvoyée est la région la plus grande.

# 3. La région renvoyée est la région qui a le plus de voisines parmi celles qui ne sont pas coloriées. Si tout est colorié, renvoie None.

reponse_Q_10_b = ""
