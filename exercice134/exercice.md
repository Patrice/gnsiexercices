~~~ {.hidden .meta}
classe : terminale
type : écrit
chapitre : poo
thème : listes
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
>>> br = Region('Bretagne')
>>> nr = Region('Normandie')
>>> pl = Region('Pays de Loire')
>>> ce = Region('Centre')
>>> ga = Region('Grande Aquitaine')
>>> ge = Region('Grand Est')
>>> br.tab_voisines = [nr, pl]
>>> pl.tab_voisines = [br, nr, ce, ga]
>>> nr.tab_voisines = [br,pl,ce]
>>> ga.tab_voisines = [pl,ce]
>>> ce.tab_voisines = [pl, ga, nr]
>>> ou = Pays('Ouest')
>>> ou.tab_regions = [br, nr, ce, pl, ga]
~~~

Exercice
========

Un pays est composé de différentes régions. Deux régions sont voisines si elles ont au moins une frontière en commun. L'objectif est d'attribuer une couleur à chaque région sur la carte du pays sans que deux régions voisines aient la même couleur et en utilisant le moins de couleurs possibles.

La figure 1 ci-dessous donne un exemple de résultat de coloration des régions de la France métropolitaine.


Figure 1 – Carte coloriée des régions de France métropolitaine

On rappelle quelques fonctions et méthodes des tableaux (le type `list` en Python) qui pourront être utilisées dans cet exercice :

- `len(tab)` : renvoie le nombre d'éléments du tableau `tab` ;
- `tab.append(elt)` : ajoute l'élément `elt` en fin de tableau `tab` ;
-  `tab.remove(elt)` : enlève la première occurrence de `elt` de `tab` si `elt` est dans `tab`, provoque une erreur sinon.

Exemples :

- `len([1, 3, 12, 24, 3])` renvoie 5 ;
- avec `tab = [1, 3, 12, 24, 3]`, l’instruction `tab.append(7)` modifie tab en `[1, 3, 12, 24, 3, 7]` ;
- avec `tab = [1, 3, 12, 24, 3]`, l’instruction `tab.remove(3)` modifie `tab` en `[1, 12, 24, 3]`.

Les deux parties de cet exercice forment un ensemble. Cependant, il n’est pas nécessaire d’avoir répondu à une question pour aborder la suivante. En particulier, on pourra utiliser les méthodes des questions précédentes même quand elles n’ont pas été codées.

Pour chaque question, toute trace de réflexion sera prise en compte.

**Partie 1**

On considère la classe `Region` qui modélise une région sur une carte et dont le début de l'implémentation est :

~~~ {.python}
class Region:
    """Modélise une région d'un pays sur une carte."""
    def __init__(self, nom_region):
        """
		initialise une région
		: param nom_region (str) le nom de la région
		"""
		self.nom = nom_region
		# tableau des régions voisines, vide au départ
		self.tab_voisines = []
		# tableau des couleurs disponibles pour colorier la région
		self.tab_couleurs_disponibles = ['rouge', 'vert','bleu', 
		                                 'jaune', 'orange', 'marron']
		# couleur attribuée à la région et non encore choisie au départ
		self.couleur_attribuee = None

~~~


1. Associer, en vous appuyant sur l’extrait de code précédent, les noms `nom`, `tab_voisines`, `tab_couleurs_disponibles` et `couleur_attribuee` au terme qui leur correspond parmi : *objet*, *attribut*, *méthode* ou *classe*.

~~~ {.python .hidden .test .amc file="Q_1.md" bareme="1"}
>>> nom
'attribut'
>>> tab_voisines
'attribut'
>>> tab_couleurs_disponibles
'attribut'
~~~

2. Indiquer le type du paramètre `nom_region` de la méthode `__init__` de la classe `Region`.

~~~ {.python .hidden .test .amc file="Q_2.md" bareme="1"}
>>> assert str(type_de_nom_region).lower() in ['string', 'str', 'chaine', 'texte']
~~~

3. Donner une instruction permettant de créer une instance nommée `ge` de la classe `Region` correspondant à la région dont le nom est « Grand Est ».

~~~ {.python .hidden .test .amc file="Q_3.md" bareme="1"}
>>> ge.nom
'Grand Est'
~~~

4. Recopier et compléter la ligne 6 de la méthode de la classe `Region` ci-dessous :

~~~ {.python}
def renvoie_premiere_couleur_disponible(self):
    """
	Renvoie la première couleur du tableau des couleurs disponibles supposé non vide.
	: return (str)
	"""
	return ...
~~~

~~~ {.python .hidden .test .amc file="Q_4.md" bareme="1"}
>>> br.renvoie_premiere_couleur_disponible()
'rouge'
>>> br.tab_couleurs_disponibles.pop(0)
'rouge'
>>> br.renvoie_premiere_couleur_disponible()
'vert'
~~~

5. Recopier et compléter la méthode de la classe `Region` ci-dessous :

~~~ {.python}
def renvoie_nb_voisines(self) :
    """
	Renvoie le nombre de régions voisines.
	: return (int)
	"""
	return ...
~~~

~~~ {.python .hidden .test .amc file="Q_5.md" bareme="1"}
>>> br.renvoie_nb_voisines()
2
>>> pl.renvoie_nb_voisines()
4
~~~


6. Compléter la méthode de la classe `Region` ci-dessous  : 

~~~ {.python}
def est_coloriee(self):
   """
   Renvoie True si une couleur a été attribuée à cette région et False sinon.
   : return (bool)
   """
   ...

~~~

~~~ {.python .hidden .test .amc file="Q_6.md" bareme="1"}
>>> br.est_coloriee()
False
>>> br.couleur_attribuee = 'rouge'
>>> br.est_coloriee()
True
~~~



7. Compléter la méthode de la classe `Region` ci-dessous  : 

~~~ {.python}
def retire_couleur(self, couleur):
    """
	Retire couleur du tableau de couleurs disponibles de la région 
	si elle est dans ce tableau. Ne fait rien sinon.
	: param couleur (str)
	: ne renvoie rien
	: effet de bord sur le tableau des couleurs disponibles
	"""
    ... 

~~~

~~~ {.python .hidden .test .amc file="Q_7_a.md" bareme="1"}
>>> 'rouge' in br.tab_couleurs_disponibles
True
>>> br.retire_couleur('rouge')
>>> 'rouge' in br.tab_couleurs_disponibles
False
~~~

~~~ {.python .hidden .test .amc file="Q_7_b.md" bareme="1"}
>>> 'rouge' in br.tab_couleurs_disponibles
True
>>> br.retire_couleur('rouge')
>>> br.retire_couleur('rouge')
>>> 'rouge' in br.tab_couleurs_disponibles
False
~~~


8. Compléter la méthode de la classe `Region` ci-dessous, en utilisant une boucle :

~~~ {.python}
def est_voisine(self, region):
    """"
	Renvoie True si la region passée en paramètre est une voisine et False sinon.
	: param region (Region)
	: return (bool)
	"""
	...

~~~

~~~ {.python .hidden .test .amc file="Q_8.md" bareme="2"}
>>> br.est_voisine(pl)
True
>>> br.est_voisine(nr)
True
>>> br.est_voisine(ce)
False
>>> pl.est_voisine(br)
True
~~~



**Partie 2**

Dans cette partie :

- on considère qu’on dispose d’un ensemble d’instances de la classe `Region` pour lesquelles l’attribut `tab_voisines` a été renseigné ;
- on pourra utiliser les méthodes de la classe `Region` évoquées dans les questions de la partie 1 :

    - `renvoie_premiere_couleur_disponible`
	- `renvoie_nb_voisines`
	- `est_coloriee`
	- `retire_couleur`
	- `est_voisine`
	
On a créé une classe `Pays` :

- cette classe modélise la carte d’un pays composé de régions ;
- l’unique attribut `tab_regions` de cette classe est un tableau (type `list` en Python) dont les éléments sont des instances de la classe `Region`.

9. Recopier et compléter la méthode de la classe `Pays` ci-dessous :

~~~ {.python}
def renvoie_tab_regions_non_coloriees(self):
    """"
	Renvoie un tableau dont les éléments sont les régions du pays sans couleur attribuée.
	: return (list) tableau d’instances de la classe Region
	"""
	...
~~~

~~~ {.python .hidden .test .amc file="Q_9.md" bareme="2"}
>>> l = ou.renvoie_tab_regions_non_coloriees()
>>> br in l
True
>>> pl in l
True
>>> ce in l
True
>>> ge in l
False
~~~



10. On considère la méthode de la classe `Pays` ci-dessous :

~~~ {.python}
def renvoie_max(self):
    nb_voisines_max = -1
	region_max = None
	for reg in self.renvoie_tab_regions_non_coloriees():
	    if reg.renvoie_nb_voisines() > nb_voisines_max:
		    nb_voisines_max = reg.renvoie_nb_voisines()
			region_max = reg
    return region_max
~~~

a. Expliquer dans quel cas cette méthode renvoie `None`.
b. Indiquer, dans le cas où cette méthode ne renvoie pas `None`, les deux particularités de la région renvoyée.


~~~ {.python .hidden .test .amc file="Q_10_a.md" bareme="1"}
>>> int(reponse_Q_10_a)
1
~~~

~~~ {.python .hidden .test .amc file="Q_10_b.md" bareme="1"}
>>> int(reponse_Q_10_b)
3
~~~

11. Coder la méthode `colorie(self)` de la classe Pays qui choisit une couleur pour chaque région du pays de la façon suivante :

- On récupère la région non coloriée qui possède le plus de voisines.
- Tant que cette région existe :
  - La couleur attribuée à cette région est la première couleur disponible dans son tableau de couleurs disponibles.
  - Pour chaque région voisine de la région :
      - si la couleur choisie est présente dans le tableau des couleurs disponibles de la région voisine alors on la retire de ce tableau.
  - On récupère à nouveau la région non coloriée qui possède le plus de voisines.


~~~ {.python .hidden .test .amc file="Q_11_a.md" bareme="1"}
>>> ## Toutes les regions doivent etre coloriees
>>> rm = ou.renvoie_max()
>>> rm.nom
'Pays de Loire'
>>> ou.colorie()
>>> for r in ou.tab_regions:
...    assert r.couleur_attribuee is not None
~~~

~~~ {.python .hidden .test .amc file="Q_11_b.md" bareme="1"}
>>> ## les couleurs sont differentes de celles des voisins
>>> ou.colorie()
>>> for r in ou.tab_regions:
...    for v in r.tab_voisines:
...        assert r.couleur_attribuee != v.couleur_attribuee
~~~


