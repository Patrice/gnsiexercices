class Region:
    """Modélise une région d'un pays sur une carte."""
    def __init__(self, nom_region):
        """
	initialise une région
	: param nom_region (str) le nom de la région
	"""
        self.nom = nom_region
        # tableau des régions voisines, vide au départ
        self.tab_voisines = []
        # tableau des couleurs disponibles pour colorier la région
        self.tab_couleurs_disponibles = ['rouge', 'vert', 'bleu', 'jaune']
        # couleur attribuée à la région et non encore choisie au départ
        self.couleur_attribuee = None

    def renvoie_premiere_couleur_disponible(self):
        return self.tab_couleurs_disponibles[0]

    def renvoie_nb_voisines(self):
        return len(self.tab_voisines)

    def est_coloriee(self):
        return self.couleur_attribuee is not None

    def retire_couleur(self, couleur):
        if couleur in self.tab_couleurs_disponibles:
            self.tab_couleurs_disponibles.remove(couleur)

    def est_voisine(self, region):
        for r in self.tab_voisines:
            if region == r:
                return True
        return False

class Pays:
    def __init__(self, nom):
        """
        Modélise la carte d'un pays composé de régions.
        """
        self.tab_regions = []

    def renvoie_tab_regions_non_coloriees(self):
        L=[]
        for region in self.tab_regions :
            if not region.est_coloriee():
                L.append(region)
        return L

    def colorie(self):
        region_m = self.renvoie_max()
        while region_m:
            region_m.couleur_attribuee = region_m.renvoie_premiere_couleur_disponible()
            for voisine in region_m.tab_voisines:
                voisine.retire_couleur(region_m.couleur_attribuee)
            region_m = self.renvoie_max()

    def renvoie_max(self):
        nb_voisines_max = -1
        region_max = None
        for reg in self.renvoie_tab_regions_non_coloriees():
            if reg.renvoie_nb_voisines() > nb_voisines_max:
                nb_voisines_max = reg.renvoie_nb_voisines()
                region_max = reg
        return region_max

            
## Partie 1

# Q1
nom = "attribut"
tab_voisines =  "attribut"
tab_couleurs_disponibles = "attribut"


# Q2
type_de_nom_region = "str"

# Q3
ge = Region ("Grand Est")

## Partie 2

# Q_10
# a. Choisir la bonne réponse parmi les trois propositions suivantes :
# 1. La méthode renvoie None dans le cas ou tout est colorié.

# 2. La méthode renvoie None quand la région n'a plus de couleur disponible.

# 3. La méthode renvoie None dans le cas ou la région est déjà coloriée.

reponse_Q_10_a = "1"

# b. Choisir la bonne réponse parmi les trois propositions suivantes :
# 1. La région renvoyée est la région qui a le plus de couleurs disponibles. Si le région n'a plus de couleur disponible, renvoie None.

# 2. La région renvoyée est la région la plus grande.

# 3. La région renvoyée est la région qui a le plus de voisines parmi celles qui ne sont pas coloriées. Si tout est colorié, renvoie None.

reponse_Q_10_b = "3"
