def moyenne(tab):
    if tab == []: #on traite en premier le cas du tableau vide pour lequel la moyenne n'est pas définie
        return 'erreur'
    somme = 0
    for val in tab:
        somme += val
    return somme / len(tab)
