\hypertarget{exercice}{%
\section{Exercice}\label{exercice}}

La classe Plateau gère les quatre rangées de cartes placées sur la
table. En mode console, on placera, entre crochets, le nombre de têtes
de bœufs contenus dans la rangée.

Par exemple :

{[}1{]} : 102 {[}10{]} : 8-10-61-66 {[}1{]} : 94 {[}4{]} : 70-92

La première rangée contient une seule carte avec une seule tête de bœuf.
La deuxième, 4 cartes qui totalisent 10 têtes de bœufs.

,-------------------------------------------------------------------------.
\textbar{} Plateau \textbar{}
\textbar{}-------------------------------------------------------------------------\textbar{}
\textbar{} + rangees : la liste des rangees de cartes \textbar{}
\textbar{} \textbar{} \textbar{} + \textbf{init}(self, l) : une liste de
4 cartes doit être fournie à la \textbar{} \textbar{} création de la
rangée. \textbar{} \textbar{} + \_get\_rangee\_la\_plus\_faible(self):
Méthode privée. \textbar{} \textbar{} Renvoie l'indice de la rangée avec
le plus faible nombre de \textbar{} \textbar{} têtes de bœuf. \textbar{}
\textbar{} + \_get\_rangee\_ou\_ajouter(self, c): Méthode privée.
\textbar{} \textbar{} Renvoie l'indice de la rangée où il faut ajouter
la carte c.~\textbar{} \textbar{} Si la carte est trop petite pour être
ajouter à une rangée, \textbar{} \textbar{} la valeur -1 est renvoyée.
\textbar{} \textbar{} + ajouter(self, c): Ajoute le carte c au plateau.
Par défaut, si \textbar{} \textbar{} si un joueur prend une rangée, il
prend celle avec le moins \textbar{} \textbar{} de têtes de bœufs.
Renvoie l'index de la rangée et la liste des \textbar{} \textbar{}
ramassées. \textbar{} \textbar{} + \textbf{str}(self): pour afficher le
plateau comme précisé en introduction \textbar{}
`-------------------------------------------------------------------------'

\begin{Shaded}
\begin{Highlighting}[]
\OperatorTok{>>>}\NormalTok{ p }\OperatorTok{=}\NormalTok{ Plateau([Carte(}\DecValTok{12}\NormalTok{), Carte(}\DecValTok{69}\NormalTok{), Carte(}\DecValTok{55}\NormalTok{), Carte(}\DecValTok{11}\NormalTok{)])}
\OperatorTok{>>>} \BuiltInTok{print}\NormalTok{(p)}
\NormalTok{[}\DecValTok{1}\NormalTok{] : }\DecValTok{12}
\NormalTok{[}\DecValTok{1}\NormalTok{] : }\DecValTok{69}
\NormalTok{[}\DecValTok{7}\NormalTok{] : }\DecValTok{55}
\NormalTok{[}\DecValTok{5}\NormalTok{] : }\DecValTok{11}
\OperatorTok{<}\NormalTok{BLANKLINE}\OperatorTok{>}
\end{Highlighting}
\end{Shaded}

Pour déterminer la rangée avec le moins de têtes de bœufs, on fabrique
une liste avec le nombre total de chaque rangée, puis on renvoie
l'indice du minimum.

\begin{Shaded}
\begin{Highlighting}[]
\OperatorTok{>>>}\NormalTok{ p._get_rangee_la_plus_faible()}
\DecValTok{0}
\end{Highlighting}
\end{Shaded}

Pour ajouter une carte, on place la carte sur la rangée qui possède la
carte inférieure la plus proche.

\begin{Shaded}
\begin{Highlighting}[]
\OperatorTok{>>>}\NormalTok{ p._get_rangee_ou_ajouter(Carte(}\DecValTok{13}\NormalTok{))}
\DecValTok{0}
\OperatorTok{>>>}\NormalTok{ p._get_rangee_ou_ajouter(Carte(}\DecValTok{40}\NormalTok{))}
\DecValTok{0}
\OperatorTok{>>>}\NormalTok{ p._get_rangee_ou_ajouter(Carte(}\DecValTok{80}\NormalTok{))}
\DecValTok{1}
\OperatorTok{>>>}\NormalTok{ p._get_rangee_ou_ajouter(Carte(}\DecValTok{13}\NormalTok{))}
\DecValTok{0}
\OperatorTok{>>>}\NormalTok{ p._get_rangee_ou_ajouter(Carte(}\DecValTok{60}\NormalTok{))}
\DecValTok{2}
\end{Highlighting}
\end{Shaded}

Si toutes les cartes sont plus grandes que celle que l'on doit placer,
alors la méthode \texttt{\_get\_rangee\_ou\_ajouter(self)} renvoie -1.

\begin{Shaded}
\begin{Highlighting}[]
\OperatorTok{>>>}\NormalTok{ p._get_rangee_ou_ajouter(Carte(}\DecValTok{1}\NormalTok{))}
\DecValTok{-1}
\end{Highlighting}
\end{Shaded}

La méthode \texttt{ajouter(self,\ c)} détermine sur quelle rangée doit
être placée la carte. Elle renvoie l'indice de la rangée où va être
rajoutée la carte ainsi que la liste des cartes ramassées par le joueur
(éventuellement vide).

La carte 13 sera ajoutée à la première rangée (indice 0). Comme c'est la
deuxième carte de la rangée, le joueur ne ramasse aucune carte.

\begin{Shaded}
\begin{Highlighting}[]
\OperatorTok{>>>}\NormalTok{ p.ajouter(Carte(}\DecValTok{13}\NormalTok{))}
\NormalTok{(}\DecValTok{0}\NormalTok{, [])}
    
\OperatorTok{>>>} \BuiltInTok{print}\NormalTok{(p)}
\NormalTok{[}\DecValTok{2}\NormalTok{] : }\DecValTok{12-13}
\NormalTok{[}\DecValTok{1}\NormalTok{] : }\DecValTok{69}
\NormalTok{[}\DecValTok{7}\NormalTok{] : }\DecValTok{55}
\NormalTok{[}\DecValTok{5}\NormalTok{] : }\DecValTok{11}
\OperatorTok{<}\NormalTok{BLANKLINE}\OperatorTok{>}
\OperatorTok{>>>}\NormalTok{ p.ajouter(Carte(}\DecValTok{14}\NormalTok{))}
\NormalTok{(}\DecValTok{0}\NormalTok{, [])}
\OperatorTok{>>>}\NormalTok{ p.ajouter(Carte(}\DecValTok{15}\NormalTok{))}
\NormalTok{(}\DecValTok{0}\NormalTok{, [])}
\OperatorTok{>>>}\NormalTok{ p.ajouter(Carte(}\DecValTok{16}\NormalTok{))}
\NormalTok{(}\DecValTok{0}\NormalTok{, [])}
\end{Highlighting}
\end{Shaded}

Comme le carte 17 sera la sixième carte ajoutée, le joueur ramassera les
5 premières cartes.

\begin{Shaded}
\begin{Highlighting}[]
\OperatorTok{>>>}\NormalTok{ p.ajouter(Carte(}\DecValTok{17}\NormalTok{))}
\NormalTok{(}\DecValTok{0}\NormalTok{, [Carte n°}\DecValTok{12} \OperatorTok{-} \DecValTok{1}\NormalTok{ TdB, Carte n°}\DecValTok{13} \OperatorTok{-} \DecValTok{1}\NormalTok{ TdB, Carte n°}\DecValTok{14} \OperatorTok{-} \DecValTok{1}\NormalTok{ TdB, Carte n°}\DecValTok{15} \OperatorTok{-} \DecValTok{2}\NormalTok{ TdB, Carte n°}\DecValTok{16} \OperatorTok{-} \DecValTok{1}\NormalTok{ TdB])}
\OperatorTok{>>>} \BuiltInTok{print}\NormalTok{(p)}
\NormalTok{[}\DecValTok{1}\NormalTok{] : }\DecValTok{17}
\NormalTok{[}\DecValTok{1}\NormalTok{] : }\DecValTok{69}
\NormalTok{[}\DecValTok{7}\NormalTok{] : }\DecValTok{55}
\NormalTok{[}\DecValTok{5}\NormalTok{] : }\DecValTok{11}
\OperatorTok{<}\NormalTok{BLANKLINE}\OperatorTok{>}
\OperatorTok{>>>}\NormalTok{ p.ajouter(Carte(}\DecValTok{21}\NormalTok{))}
\NormalTok{(}\DecValTok{0}\NormalTok{, [])}
\OperatorTok{>>>} \BuiltInTok{print}\NormalTok{(p)}
\NormalTok{[}\DecValTok{2}\NormalTok{] : }\DecValTok{17-21}
\NormalTok{[}\DecValTok{1}\NormalTok{] : }\DecValTok{69}
\NormalTok{[}\DecValTok{7}\NormalTok{] : }\DecValTok{55}
\NormalTok{[}\DecValTok{5}\NormalTok{] : }\DecValTok{11}
\OperatorTok{<}\NormalTok{BLANKLINE}\OperatorTok{>}
\end{Highlighting}
\end{Shaded}

La carte 1 ne pouvant être placée, le joueur ramassera la colonne avec
le nombre de têtes de bœufs le plus faible (ici, la deuxième rangée).

\begin{Shaded}
\begin{Highlighting}[]
\OperatorTok{>>>}\NormalTok{ p.ajouter(Carte(}\DecValTok{1}\NormalTok{))}
\NormalTok{(}\DecValTok{1}\NormalTok{, [Carte n°}\DecValTok{69} \OperatorTok{-} \DecValTok{1}\NormalTok{ TdB])}
\end{Highlighting}
\end{Shaded}

