~~~ {.hidden .meta}
classe : terminale
type : écrit
chapitre : poo
thème : listes de listes
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
>>> from rangee import Rangee
>>> from carte import Carte
>>> p = Plateau([Carte(12), Carte(69), Carte(55), Carte(11)])
>>> p1 = Plateau([Carte(17), Carte(69), Carte(55), Carte(11)])
~~~

Exercice
========

La classe Plateau gère les quatre rangées de cartes placées sur la
table.  En mode console, on placera, entre crochets, le nombre de
têtes de bœufs contenus dans la rangée.

Par exemple :

[1] : 102
[10] : 8-10-61-66
[1] : 94
[4] : 70-92

La première rangée contient une seule carte avec une seule tête de bœuf.
La deuxième, 4 cartes qui totalisent 10 têtes de bœufs.

,-------------------------------------------------------------------------.
| Plateau                                                                 |
|-------------------------------------------------------------------------|
| + rangees : la liste des rangees de cartes                              |
|                                                                         |
| + __init__(self, l) : une liste de 4 cartes doit être fournie à la      |
|     création de la rangée.                                              |
| + _get_rangee_la_plus_faible(self): Méthode privée.                     |
|     Renvoie l'indice de la rangée avec le plus faible nombre de         |
|     têtes de bœuf.                                                      |
| + _get_rangee_ou_ajouter(self, c): Méthode privée.                      |
|     Renvoie l'indice de la rangée où il faut ajouter la carte c.        |
|     Si la carte est trop petite pour être ajouter à une rangée,         |
|     la valeur -1 est renvoyée.                                          |
| + ajouter(self, c): Ajoute le carte c au plateau. Par défaut, si        |
|     si un joueur prend une rangée, il prend celle avec le moins         |
|     de têtes de bœufs. Renvoie l'index de la rangée et la liste des     |
|     ramassées.                                                          |
| + __str__(self): pour afficher le plateau comme précisé en introduction |
`-------------------------------------------------------------------------'

~~~ {.python .test .amc file="Q_1.md" bareme="1"}
    >>> p = Plateau([Carte(12), Carte(69), Carte(55), Carte(11)])
    >>> print(p)
    [1] : 12
    [1] : 69
    [7] : 55
    [5] : 11
    <BLANKLINE>
~~~

Pour déterminer la rangée avec le moins de têtes de bœufs, on fabrique
une liste avec le nombre total de chaque rangée, puis on renvoie
l'indice du minimum.

~~~ {.python .test .amc file="Q_2.md" bareme="1"}
    >>> p._get_rangee_la_plus_faible()
    0
~~~

Pour ajouter une carte, on place la carte sur la rangée qui possède la
carte inférieure la plus proche.

~~~ {.python .hidden .test .amc file="Q_3.md" bareme="1"}
    >>> p._get_rangee_ou_ajouter(Carte(13))
    0
	>>> p._get_rangee_ou_ajouter(Carte(40))
    0
    >>> p._get_rangee_ou_ajouter(Carte(80))
    1
    >>> p._get_rangee_ou_ajouter(Carte(13))
    0
    >>> p._get_rangee_ou_ajouter(Carte(60))
    2
~~~

Si toutes les cartes sont plus grandes que celle que l'on doit placer,
alors la méthode `_get_rangee_ou_ajouter(self)` renvoie -1. 

~~~ {.python .hidden .test .amc file="Q_4.md" bareme="1"}
    >>> p._get_rangee_ou_ajouter(Carte(1))
    -1
~~~

La méthode `ajouter(self, c)` détermine sur quelle rangée doit être
placée la carte.  Elle renvoie l'indice de la rangée où va être
rajoutée la carte ainsi que la liste des cartes ramassées par le
joueur (éventuellement vide).

La carte 13 sera ajoutée à la première rangée (indice 0). Comme c'est
la deuxième carte de la rangée, le joueur ne ramasse aucune carte.

~~~ {.python .test .amc file="Q_5.md" bareme="1"}
    >>> p.ajouter(Carte(13))
    (0, [])
~~~

    >>> print(p)
    [2] : 12-13
    [1] : 69
    [7] : 55
    [5] : 11
    <BLANKLINE>

~~~ {.python .hidden .test file="Q_6.md" bareme="1"}
    >>> p.ajouter(Carte(13))
    (0, [])
	>>> p.ajouter(Carte(14))
    (0, [])
    >>> p.ajouter(Carte(15))
    (0, [])
    >>> p.ajouter(Carte(16))
    (0, [])
    >>> print(p)
    [6] : 12-13-14-15-16
    [1] : 69
    [7] : 55
    [5] : 11
    <BLANKLINE>
~~~

Comme le carte 17 sera la sixième carte ajoutée, le joueur ramassera
les 5 premières cartes.
    
    >>> p.ajouter(Carte(17))
    (0, [Carte n°12 - 1 TdB, Carte n°13 - 1 TdB, Carte n°14 - 1 TdB, Carte n°15 - 2 TdB, Carte n°16 - 1 TdB])
    >>> print(p)
    [1] : 17
    [1] : 69
    [7] : 55
    [5] : 11
    <BLANKLINE>

~~~ {.python .hidden .test file="Q_7.md" bareme="1"}
    >>> p.ajouter(Carte(13))
    (0, [])
	>>> p.ajouter(Carte(14))
    (0, [])
    >>> p.ajouter(Carte(15))
    (0, [])
    >>> p.ajouter(Carte(16))
    (0, [])
    >>> print(p)
    [6] : 12-13-14-15-16
    [1] : 69
    [7] : 55
    [5] : 11
    <BLANKLINE>
    >>> p.ajouter(Carte(17))
    (0, [Carte n°12 - 1 TdB, Carte n°13 - 1 TdB, Carte n°14 - 1 TdB, Carte n°15 - 2 TdB, Carte n°16 - 1 TdB])
    >>> print(p)
    [1] : 17
    [1] : 69
    [7] : 55
    [5] : 11
    <BLANKLINE>
~~~

	>>> p.ajouter(Carte(21))
    (0, [])
    >>> print(p)
    [2] : 17-21
    [1] : 69
    [7] : 55
    [5] : 11
    <BLANKLINE>

~~~ {.python .hidden .test .amc file="Q_8.md" bareme="1"}
	>>> p1.ajouter(Carte(21))
    (0, [])
    >>> print(p1)
    [2] : 17-21
    [1] : 69
    [7] : 55
    [5] : 11
    <BLANKLINE>
~~~

La carte 1 ne pouvant être placée, le joueur ramassera la colonne avec
le nombre de têtes de bœufs le plus faible (ici, la deuxième rangée).

    >>> p.ajouter(Carte(1))
    (1, [Carte n°69 - 1 TdB])

~~~ {.python .hidden .test .amc file="Q_9.md" bareme="1"}
	>>> p1.ajouter(Carte(21))
    (0, [])
	>>> p1.ajouter(Carte(1))
    (1, [Carte n°69 - 1 TdB])
~~~

~~~ {.python .hidden .test file="Q_10.md" bareme="1"}
	>>> p1.ajouter(Carte(21))
    (0, [])
    >>> p1.ajouter(Carte(1))
    (1, [Carte n°69 - 1 TdB])
	>>> print(p1)
    [2] : 17-21
    [1] : 1
    [7] : 55
    [5] : 11
    <BLANKLINE>
~~~

