"""
Module Carte de six qui prend
"""


class Carte:
    """ Classe décrivant les cartes sur 6 qui prend"""
    def __init__(self, numero):
        """
        Chaque carte porte un numéro compris entre 1 et 104 ainsi
        qu'un certain nombre de têtes de bœufs qui correspondent à
        des pénalités.
        """
        assert 1 <= numero <= 104
        assert isinstance(numero, int)
        self.numero = numero
        self.penalites = self.__get_penalites()

    def get_numero(self):
        """
        Renvoie le numero de la carte
        """
        return self.numero

    def get_nombre_tdb(self):
        """
        renvoie le nombre de têtes de bœuf que la carte contient.
        """
        return self.penalites

    def __repr__(self):
        """
        La représentation dans une console.
        """
        return f"Carte n°{self.numero} - {self.penalites} TdB"

    def __str__(self):
        """
        La description de la carte en mode texte.
        """
        return f"{self.numero}"

    def __get_penalites(self):
        """Renvoie le nombre de têtes de vaches que contient la carte."""
        nb_tetes = 0
        if self.numero % 10 == 5:
            nb_tetes += 2
        if self.numero % 10 == 0:
            nb_tetes += 3
        if self.numero % 11 == 0:
            nb_tetes += 5
        if nb_tetes == 0:
            nb_tetes = 1
        return nb_tetes

    def __lt__(self, autre):
        """
        Permet la comparaison < entre la carte courante et la carte autre.
        """
        return self.numero < autre.numero

    def __gt__(self, autre):
        """
        Permet la comparaison > entre la carte courante et la carte autre.
        """
        return self.numero > autre.numero


if __name__ == "__main__":
    import nose
    nose.run()
