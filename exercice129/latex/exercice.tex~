\hypertarget{exercice}{%
\section{Exercice}\label{exercice}}

Un arbre binaire est soit vide, représenté en Python par la valeur
\texttt{None}, soit un nœud, contenant une étiquette et deux sous-arbres
gauche et droit et représenté par une instance de la classe
\texttt{Noeud} donnée ci-dessous.

\begin{Shaded}
\begin{Highlighting}[]
\KeywordTok{class}\NormalTok{ Noeud:}
    \CommentTok{"""Classe représentant un noeud d'un arbre binaire"""}
    \KeywordTok{def} \FunctionTok{__init__}\NormalTok{(}\VariableTok{self}\NormalTok{, etiquette, gauche, droit):}
        \CommentTok{"""Crée un noeud de valeur etiquette avec }
\CommentTok{        gauche et droit comme fils."""}
        \VariableTok{self}\NormalTok{.etiquette }\OperatorTok{=}\NormalTok{ etiquette}
        \VariableTok{self}\NormalTok{.gauche }\OperatorTok{=}\NormalTok{ gauche}
        \VariableTok{self}\NormalTok{.droit }\OperatorTok{=}\NormalTok{ droit}

\KeywordTok{def}\NormalTok{ parcours(arbre, liste):}
    \CommentTok{"""parcours récursivement l'arbre en ajoutant les étiquettes}
\CommentTok{    de ses noeuds à la liste passée en argument en ordre infixe."""}
    \ControlFlowTok{if}\NormalTok{ arbre }\OperatorTok{!=} \VariableTok{None}\NormalTok{:}
\NormalTok{        parcours(arbre.gauche, liste)}
\NormalTok{        liste.append(arbre.etiquette)}
\NormalTok{        parcours(arbre.droit, liste)}
    \ControlFlowTok{return}\NormalTok{ liste}
\end{Highlighting}
\end{Shaded}

La fonction récursive \texttt{parcours} renvoie la liste des étiquettes
des nœuds de l'arbre implémenté par l'instance \texttt{arbre} dans
l'ordre du parcours en profondeur infixe à partir d'une liste vide
passée en argument.

Compléter le code de la fonction \texttt{insere}, présenté ci-dessous,
qui prend en argument un arbre binaire de recherche \texttt{arbre}
représenté ainsi et une étiquette \texttt{cle}, non présente dans
l'\texttt{arbre}, et qui :

\begin{itemize}
\tightlist
\item
  renvoie une nouvelle feuille d'étiquette \texttt{cle} s'il est vide ;
\item
  renvoie l'arbre après l'avoir modifié en insérant \texttt{cle} sinon ;
\item
  garantit que l'arbre ainsi complété soit encore un arbre binaire de
  recherche.
\end{itemize}

Tester ensuite ce code en utilisant la fonction \texttt{parcours} et en
insérant successivement des nœuds d'étiquette 1, 4, 6 et 8 dans l'arbre
binaire de recherche représenté ci- dessous :

{[}arbre{]}{[}file:arbre\_exo\_129.png{]}

\begin{Shaded}
\begin{Highlighting}[]
\KeywordTok{def}\NormalTok{ insere(arbre, cle):}
    \CommentTok{"""insere la cle dans l'arbre binaire de recherche}
\CommentTok{    représenté par arbre.}
\CommentTok{    Retourne l'arbre modifié."""}
    \ControlFlowTok{if}\NormalTok{ arbre }\OperatorTok{==} \VariableTok{None}\NormalTok{:}
        \ControlFlowTok{return}\NormalTok{ Noeud(cle, }\VariableTok{None}\NormalTok{, }\VariableTok{None}\NormalTok{) }\CommentTok{# creation d'une feuille}
    \ControlFlowTok{else}\NormalTok{:}
        \ControlFlowTok{if}\NormalTok{ ...: }
\NormalTok{            arbre.gauche }\OperatorTok{=}\NormalTok{ insere(arbre.gauche, cle)}
        \ControlFlowTok{else}\NormalTok{:}
\NormalTok{            arbre.droit }\OperatorTok{=}\NormalTok{ ... }
        \ControlFlowTok{return}\NormalTok{ arbre}
\end{Highlighting}
\end{Shaded}

