def tester_palindrome (chaine):
    assert isinstance(chaine,str) or isinstance(chaine,list) 
    if len(chaine) < 2:
        return True
    elif chaine[0] != chaine[-1]:
        return False
    else:
        chaine = chaine[1:-1]
    return tester_palindrome(chaine)
    

def est_palindrome(chaine):
    if not isinstance(chaine, str):
        raise TypeError("L'entrée doit être une chaîne de caractères.")
    debut = 0
    fin = len(chaine) - 1
    while debut < fin:
        if chaine[debut] != chaine[fin]:
            return False
        debut += 1
        fin -= 1
    return True

