~~~ {.hidden .meta}
classe : terminale
type : écrit
chapitre : structures linéaires
thème : récursivité
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========

*Cet exercice porte sur l'algorithmique et la programmation.*

Un palindrome est un mot qui se lit de la même manière de la gauche vers la droite que de la
droite vers la gauche (exemple : « kayak » est un palindrome).

On propose ci-dessous une fonction pour tester si un mot est un palindrome.

On précise que, pour une chaîne de caractères chaine :

- l'instruction `len(chaine)` renvoie sa longueur ;
- l'instruction `chaine[-1]` renvoie son dernier caractère ;
- l'instruction `chaine[1:-1]` renvoie la chaîne privée de son premier caractère et de son dernier caractère.

    ~~~ {.python}
    def tester_palindrome (chaine):
        if len(chaine) < 2:
            return True
        elif chaine[0] != chaine[-1]:
            return False
        else:
            chaine = chaine[1:-1]
        return tester_palindrome(chaine)
    ~~~

1. On saisit, dans la console, l'instruction suivante : 

    ~~~ {.python}
    >>> tester_palindrome('kayak')
    ~~~

    Combien de fois est appelée la fonction `tester_palindrome` lors
    de l'exécution de cette instruction ? On veillera à compter
    l'appel initial.

	~~~ {.python .hidden .amc file="Q_1.md" bareme="1"}
    ~~~

2. a. Justifier que la fonction `tester_palindrome` est récursive.
   ~~~ {.python .hidden .amc file="Q_2_a.md" bareme="1"}
   >>> pass
   ~~~

   b. Expliquer pourquoi l'appel à la fonction `tester_palindrome` se
   terminera quelle que soit la chaîne de caractères sur laquelle elle
   s'applique.

	~~~ {.python .hidden .amc file="Q_2_b.md" bareme="1"}
    >>> pass
	~~~

3. La saisie, dans la console, de l'instruction `tester_palindrome(53235)` génère une erreur.
   a. Parmi les quatre propositions suivantes, indiquer le type d'erreur affiché :
       - ZeroDivisionError
	   - ValueError
	   - TypeError
	   - IndexError 

	~~~ {.python .hidden .amc file="Q_3_a.md" bareme="1"}
    >>> pass
	~~~

   b. Proposer sur la copie une ou plusieurs instructions qu'on
   pourrait écrire entre la ligne 1 et la ligne 2 du code de la
   fonction `tester_palindrome` et permettant d'afficher clairement
   cette erreur à l'utilisateur.

	~~~ {.python .hidden .amc file="Q_3_b.md" bareme="1"}
    >>> pass
	~~~


4. Écrire le code d'une fonction itérative (non récursive)
`est_palindrome` qui prend en paramètre une chaîne de caractères et
renvoie un booléen égal à `True` si la chaîne de caractères est un
palindrome, `False` sinon.

	~~~ {.python .hidden .amc file="Q_4.md" bareme="1"}
    >>> pass
	~~~
