"""
Module partie
"""
from joueur import Joueur
from jeu import Jeu
from plateau import Plateau

class Partie:
    """
    Classe Partie
    """

    def __init__(self, l):
        """
        Il faut fournir une liste de 4 noms.
        """
        pass
    def setup(self):
        """
        On mélange le jeu de cartes. On les distribue aux 4 joueurs
        puis on en place 4 sur le plateau.
        """
        pass
        
    def __str__(self):
        """
        Renvoie les scores des joueurs, le plateau puis la main du joueur.
        """
        pass

    def est_terminee(self):
        """
        Teste si un joueur a dépassé 66.
        """
        pass
    
    def run(self):
        pass
    
if __name__ == '__main__':
    import nose
    nose.run()


