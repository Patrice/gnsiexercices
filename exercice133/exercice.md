~~~ {.hidden .meta}
classe : terminale
type : écrit
chapitre : poo
thème : parcours
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
~~~

Exercice
========


Un fabricant de brioches décide d’informatiser sa gestion des stocks. Il écrit pour cela un programme en langage Python. Une partie de son travail consiste à développer une classe `Stock` dont la première version est la suivante :

~~~ {.python}
class Stock:
	def __init__(self):
		self.qt_farine = 0
		self.nb_oeufs = 0
		self.qt_beurre = 0

~~~

1. Écrire une méthode `ajouter_beurre(self, qt)` qui ajoute la quantité `qt` de beurre à un objet de la classe `Stock`.

~~~ {.python .hidden .test .amc file="Q_1.md" bareme="1"}
>>> mon_stock = Stock()
>>> mon_stock.qt_beurre
0
>>> mon_stock.ajouter_beurre(560)
>>> mon_stock.qt_beurre
560
>>> mon_stock.ajouter_beurre(200)
>>> mon_stock.qt_beurre
760
~~~

On admet que l’on a écrit deux autres méthodes `ajouter_farine` et
`ajouter_oeufs` qui ont des fonctionnements analogues.

2. Écrire une méthode `afficher(self)` qui affiche la quantité de farine, d’œufs et de beurre d’un objet de type `Stock`. L’exemple ci-dessous illustre l’exécution de cette méthode dans la console :

~~~ {.python .test .amc file="Q_2.md" bareme="1"}
>>> mon_stock = Stock()
>>> mon_stock.afficher()
farine : 0
oeuf : 0
beurre : 0
>>> mon_stock.ajouter_beurre(560)
>>> mon_stock.afficher()
farine : 0
oeuf : 0
beurre : 560
~~~

3. Pour faire une brioche, il faut 350 g de farine, 175 g de beurre et 4 œufs. Écrire une méthode `stock_suffisant_brioche(self)` qui renvoie un booléen : VRAI s’il y a assez d’ingrédients dans le stock pour faire une brioche et FAUX sinon.

~~~ {.python .hidden .test .amc file="Q_3.md" bareme="2"}
>>> mon_stock = Stock()
>>> mon_stock.ajouter_beurre(560)
>>> mon_stock.ajouter_oeufs(20)
>>> mon_stock.ajouter_farine(360)
>>> mon_stock.stock_suffisant_brioche()
True
>>> mon_stock1 = Stock()
>>> mon_stock1.ajouter_beurre(560)
>>> mon_stock1.ajouter_oeufs(2)
>>> mon_stock1.ajouter_farine(360)
>>> mon_stock1.stock_suffisant_brioche()
False
~~~

4. On considère la méthode supplémentaire `produire(self)` de la classe `Stock` donnée par le code suivant :

~~~ {.python}
def produire(self):
    res = 0
    while self.stock_suffisant_brioche():
		self.qt_beurre = self.qt_beurre - 175
		self.qt_farine = self.qt_farine - 350
		self.nb_oeufs = self.nb_oeufs - 4
		res = res + 1
    return res
~~~

On considère un stock défini par les instructions suivantes :

~~~ {.python}
>>> mon_stock=Stock()
>>> mon_stock.ajouter_beurre(1000)
>>> mon_stock.ajouter_farine(1000)
>>> mon_stock.ajouter_oeufs(10)

~~~

    a. On exécute ensuite l’instruction
    
~~~ {.python}
	    >>> mon_stock.produire()
~~~
    Quelle valeur s’affiche dans la console ? Que représente cette valeur ?
    
~~~ {.python .hidden .test .amc file="Q_4_a_1.md" bareme="1"}
>>> mon_stock=Stock()
>>> mon_stock.ajouter_beurre(1000)
>>> mon_stock.ajouter_farine(1000)
>>> mon_stock.ajouter_oeufs(10)
>>> mon_stock.produire()
2
>>> affichage_Q4A
'2'
~~~

~~~ {.python .hidden .test .amc file="Q_4_a_2.md" bareme="1"}
>>> reponseQ4a
'b'
~~~

	b. On exécute ensuite l’instruction
	
	~~~ {.python}
	    >>> mon_stock.afficher()
	~~~
	Que s’affiche-t-il dans la console ?

~~~ {.python .hidden .test .amc file="Q_4_b.md" bareme="1"}
>>> reponseQ4b
'a'
~~~

5. L’industriel possède $n$ lieux de production distincts et donc $n$ stocks distincts.
  
  On suppose que ces stocks sont dans une liste dont chaque élément est un objet de type `Stock`. Écrire une fonction Python `nb_brioches(liste_stocks)` possédant pour unique paramètre la liste des stocks et renvoie le nombre total de brioches produites.

~~~ {.python .hidden .test .amc file="Q_5.md" bareme="2"}
>>> mon_stock1=Stock()
>>> mon_stock1.ajouter_beurre(1000)
>>> mon_stock1.ajouter_farine(1000)
>>> mon_stock1.ajouter_oeufs(10)
>>> mon_stock2=Stock()
>>> mon_stock2.ajouter_beurre(1000)
>>> mon_stock2.ajouter_farine(1000)
>>> mon_stock2.ajouter_oeufs(12)
>>> mon_stock3=Stock()
>>> mon_stock3.ajouter_beurre(2000)
>>> mon_stock3.ajouter_farine(1500)
>>> mon_stock3.ajouter_oeufs(20)
>>> nb_brioches([mon_stock1, mon_stock2, mon_stock3])
8
~~~

