
class Stock:
    def __init__(self):
        self.qt_beurre = 0
        self.qt_farine = 0
        self.nb_oeufs = 0
        
    def ajouter_beurre(self, qt):
        self.qt_beurre = self.qt_beurre + qt

    def ajouter_farine(self, qt):
        self.qt_farine = self.qt_farine + qt

    def ajouter_oeufs(self, qt):
        self.nb_oeufs = self.nb_oeufs + qt

    def afficher(self): 
        print("farine : "+str(self.qt_farine)) 
        print("oeuf : "+str(self.nb_oeufs))
        print("beurre : "+str(self.qt_beurre)) 

    def stock_suffisant_brioche(self): 
        return self.qt_beurre >= 175 and self.qt_farine >= 350 and self.nb_oeufs >= 4

    def produire(self):
        res = 0
        while self.stock_suffisant_brioche():
            self.qt_beurre = self.qt_beurre - 175
            self.qt_farine = self.qt_farine - 350
            self.nb_oeufs = self.nb_oeufs - 4
            res = res+1
        return res
    
def nb_brioches(liste_stocks): 
    nb = 0 
    for s in liste_stocks: 
        nb = nb  +  s.produire()
    return nb


## Q.4.a

# La valeur affichée est :
affichage_Q4A = "2"

## La valeur représente :
# a. les quantités d'ingrédients restantes
# b. le nombre de brioches produites
# c. les quantités d'ingrédients consommées lors de la production

reponseQ4a = "b"

## Q.4.b: question différente du sujet écrit.

## Ce qui est affiché :
# a. les quantités d'ingrédients restantes
# b. le nombre de brioches produites
# c. les quantités d'ingrédients consommées lors de la production

reponseQ4b = "a"


