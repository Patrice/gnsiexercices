~~~ {.hidden .meta}
classe : terminale
type : oral1
chapitre : structures linéaires
thème : listes de listes
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
>>> def rien():
...    pass
~~~

Exercice
========

La classe `Bombe` est conçue pour créer une grille de démineur. Elle
initialise une grille de jeu en plaçant un nombre prédéfini de bombes
aléatoirement, puis calcule les valeurs pour les cases adjacentes afin
d'indiquer le nombre de bombes environnantes. 

La classe ne dispose que d'un attribut :

- `grille`: Une grille qui représente le plateau de jeu. Chaque case peut contenir :
  - `10` pour une bombe.
  - Un entier de `0` à `8` représentant le nombre de bombes dans les cases adjacentes.

Le constructeur de la classe `Bombe` initialise une nouvelle grille en suivant les étapes suivantes :

- Crée une nouvelle grille vide de dimensions définies par la constante `NB_CASES`.
- Place un nombre prédéfini de bombes sur la grille, défini par la constante `NB_BOMBES`, à des positions aléatoires.
- Remplit chaque case non-bombe avec le nombre de bombes présentes dans les cases adjacentes.


1. Compléter la méthode `placer_les_bombes()` qui place les bombes dans la grille.

~~~ {.python .test .amc file="Q_1_a.md" bareme="1"}
>>> from random import seed
>>> seed(0)  # Pour la reproductibilité
>>> g = object.__new__(Bombe)
>>> g.completer_les_cases = rien
>>> g.__init__()
>>> print(grille_to_string(g.grille))
+-+-+-+-+-+-+-+-+-+-+
| | | | | | | | | | |
+-+-+-+-+-+-+-+-+-+-+
| | | | | | | | | | |
+-+-+-+-+-+-+-+-+-+-+
| | | | |X| | | |X| |
+-+-+-+-+-+-+-+-+-+-+
| | | | | | | | | |X|
+-+-+-+-+-+-+-+-+-+-+
|X| | | | | |X| | | |
+-+-+-+-+-+-+-+-+-+-+
| | | | | | | |X| | |
+-+-+-+-+-+-+-+-+-+-+
| | | | | | |X| | | |
+-+-+-+-+-+-+-+-+-+-+
| | | | | | | | |X| |
+-+-+-+-+-+-+-+-+-+-+
| | | | |X| | | | | |
+-+-+-+-+-+-+-+-+-+-+
| |X| | | | | | | | |
+-+-+-+-+-+-+-+-+-+-+
~~~

Le code suivant effectue un test pour s'assurer que le nombre de bombes dans la grille est correct.

~~~ {.python .test .amc file="Q_1_2.md" bareme="1"}
>>> for i in range(1000):
...     bombe = Bombe()
...     count_bombs = sum(row.count(10) for row in bombe.grille)
...     assert count_bombs == NB_BOMBES
~~~

2. Compléter le code de la méthode `completer_les_cases()` qui remplit chaque case non-bombe avec le nombre de bombes présentes dans les cases adjacentes.

~~~ {.python .test .amc file="Q_2.md" bareme="2"}
>>> from random import seed
>>> seed(0)  # Pour la reproductibilité
>>> g = Bombe()
>>> print(grille_to_string(g.grille))
+-+-+-+-+-+-+-+-+-+-+
| | | | | | | | | | |
+-+-+-+-+-+-+-+-+-+-+
| | | |1|1|1| |1|1|1|
+-+-+-+-+-+-+-+-+-+-+
| | | |1|X|1| |1|X|2|
+-+-+-+-+-+-+-+-+-+-+
|1|1| |1|1|2|1|2|2|X|
+-+-+-+-+-+-+-+-+-+-+
|X|1| | | |1|X|2|2|1|
+-+-+-+-+-+-+-+-+-+-+
|1|1| | | |2|3|X|1| |
+-+-+-+-+-+-+-+-+-+-+
| | | | | |1|X|3|2|1|
+-+-+-+-+-+-+-+-+-+-+
| | | |1|1|2|1|2|X|1|
+-+-+-+-+-+-+-+-+-+-+
|1|1|1|1|X|1| |1|1|1|
+-+-+-+-+-+-+-+-+-+-+
|1|X|1|1|1|1| | | | |
+-+-+-+-+-+-+-+-+-+-+
~~~

