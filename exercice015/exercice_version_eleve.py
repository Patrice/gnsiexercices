###################################################
### prenom, nom :                               ###
###################################################

class Yaourt:
    """ Classe définissant un yaourt caractérisé par :
    - son arome
    - son genre
    - sa durée de durabilité minimale"""
    
    def __init__(self,arome,duree):
        ####
        # À compléter :Q.1.a
        ###
        self.__arome = arome
        self.__duree = duree
        if arome == 'aucun':
            self.__genre = 'nature'
        else:
            self.__genre = 'aromatise'

    def get_arome(self):
        # À rédiger
        pass
    
    def get_duree(self):
        return self.__duree
    
    def get_genre(self):
        return self.__genre
    
    def set_duree(self,duree):
        if duree > 0 :
            self.__duree = duree

    def set_arome(self, arome):
        # À rédiger
        pass
            
    def __set_genre(self,arome):
       if arome == 'aucun':
            self.__genre = 'nature'
       else:
            self.__genre = 'aromatise'


def creer_pile():
    pile = [ ]
    return pile

def depiler(p):
    # À rédiger
    pass

def empiler(p, Yaourt):
    # À rédiger
    pass

def est_vide(p):
    # À rédiger
    pass
