class Processus:
    def __init__(self, nom, duree):
        """Crée un processus de nom <nom> et de durée <duree> (exprimée en cycles d'ordonnancement"""
        self.nom = nom
        self.duree = duree
        self._nb_cycles = 0

    def execute_un_cycle(self):
        """Exécute le processus donné pendant un cycle."""
        self._nb_cycles += 1

    def est_fini(self):
        """Renvoie True si le processus est terminé, False sinon."""
        return self._nb_cycles == self.duree

    def __repr__(self):
        return self.nom
    
class File:
    def __init__(self):
       """ Crée une file vide """
       self.contenu = []
   
    def enfile(self, element):
       """ Enfile element dans la file """
       self.contenu.append(element)
   
    def defile(self):
       """ Renvoie le premier élément de la file et l'enlève de
       file """
       if self.contenu == []:
           return None
       return self.contenu.pop(0)
   
    def est_vide(self):
       """ Renvoie True si la file est vide, False sinon """
       return self.contenu == []

    def __repr__(self):
        return str(self.contenu)
    
class Ordonnanceur:
    def __init__(self):
        self.temps = 0
        self.file = File()

    def ajoute_nouveau_processus(self, proc):
        """Ajoute un nouveau processus dans la file de l'ordonnanceur."""
        self.file.enfile(proc)
		
    def tourniquet(self):
        """Effectue une étape d'ordonnancement et renvoie le nom
        du processus élu."""
        self.temps += 1
        if not self.file.est_vide():
            proc = self.file.defile()
            proc.execute_un_cycle()
            if not proc.est_fini():
                self.file.enfile(proc)
            return proc.nom
        else:
            return None
        
        
# Q1 : placer les réponses dans la liste.
etats = ['élu', 'bloqué', 'prêt']

# Q2 : placer les réponses dans la liste.
# Attention aux accents !
etats_possibles = ['élu', 'prêt']


#Q4
p1 = Processus("p1", 4)
p2 = Processus("p2", 3)
p3 = Processus("p3", 5)
p4 = Processus("p4", 3)
depart_proc = {0: p1, 1: p3, 2: p2, 3: p4}

chronogramme2 = ['p1', 'p1', 'p3', 'p1', 'p2', 'p3', 'p4', 'p1', 'p2', 'p3', 'p4', 'p2', 'p3', 'p4', 'p3']


def main():
    ordo = Ordonnanceur()
    if ordo.temps in depart_proc.keys():
        ordo.ajoute_nouveau_processus(depart_proc[ordo.temps])
    processus_courant = "start"
    while processus_courant is not None:
        processus_courant = ordo.tourniquet()
        print(processus_courant)
        if ordo.temps in depart_proc.keys():
            ordo.ajoute_nouveau_processus(depart_proc[ordo.temps])


def to_plantuml():
    texte = ["@startuml\n", "concise \"Processus\" as P\n"]
    ordo = Ordonnanceur()
    if ordo.temps in depart_proc.keys():
        ordo.ajoute_nouveau_processus(depart_proc[ordo.temps])
    processus_courant = "start"
    while processus_courant is not None:
        processus_courant = ordo.tourniquet()
        texte.append(f"@{ordo.temps}\n")
        if processus_courant is None:
            texte.append("P is {hidden}\n")
        else:
            texte.append(f"P is {processus_courant}\n")
        if ordo.temps in depart_proc.keys():
            ordo.ajoute_nouveau_processus(depart_proc[ordo.temps])
    texte.append("@enduml\n")
    return "".join(texte)
