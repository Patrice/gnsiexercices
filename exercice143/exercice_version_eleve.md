Exercice
========

*Cet exercice porte sur la programmation Python, la programmation
orientée objet, les structures de données (file), l'ordonnancement et
l'interblocage.*

On s'intéresse aux processus et à leur ordonnancement au sein d'un
système d'exploitation. On considère ici qu'on utilise un
monoprocesseur.

1.  Citer les trois états dans lesquels un processus peut se trouver.

    On veut simuler cet ordonnancement avec des objets. Pour ce faire,
    on dispose déjà de la classe Processus dont voici la documentation :

         Classe Processus:

         >>> p = Processus(nom: str, duree: int)
         Crée un processus de nom <nom> et de durée <duree> (exprimée en
         cycles d'ordonnancement)

         p.execute_un_cycle()
         Exécute le processus donné pendant un cycle.

         p.est_fini()
         Renvoie True si le processus est terminé, False sinon.

Pour simplifier, on ne s'intéresse pas aux ressources qu'un processus
pourrait acquérir ou libérer.

2.  Citer les deux seuls états possibles pour un processus dans ce
    contexte.

Pour mettre en place l'ordonnancement, on décide d'utiliser une file,
instance de la classe `File` ci-dessous.

``` {.python}
class File:
    def __init__(self):
       """ Crée une file vide """
       self.contenu = []
   
    def enfile(self, element):
       """ Enfile element dans la file """
       self.contenu.append(element)
   
    def defile(self):
       """ Renvoie le premier élément de la file et l'enlève de
       file """
       return self.contenu.pop(0)
   
    def est_vide(self):
       """ Renvoie True si la file est vide, False sinon """
       return self.contenu == []
```

Lors de la phase de tests, on se rend compte que le code suivant produit
une erreur :

``` {.python}
>>> f = File()
>>> f.defile()
```

3.  Rectifier sur votre copie le code de la classe File pour que la
    function defile renvoie `None` lorsque la file est vide.

4.  On se propose d'ordonnancer les processus avec une méthode du type
    tourniquet telle qu'à chaque cycle :

    -   si un nouveau processus est créé, il est mis dans la file
        d'attente ;
    -   ensuite, on défile un processus de la file d'attente et on
        l'exécute pendant un cycle ;
    -   si le processus exécuté n'est pas terminé, on le replace dans la
        file.

    Par exemple, avec les processus suivants :

       processus   cycle de création   durée en cycles
      ----------- ------------------- -----------------
           A               2                  3
           B               1                  4
           C               4                  3
           D               0                  5

    On obtient le chronogramme ci-dessous :

    ![Chronogramme](exo_143_1.png)

    Figure 1. Chronogramme pour les processus A, B, C et D

    Pour décrire les processus et le moment de leur création, on utilise
    le code suivant, dans lequel `depart_proc` associe à un cycle donné
    le processus qui sera créé à ce moment :

``` {.python}
    p1 = Processus("p1", 4)
    p2 = Processus("p2", 3)
    p3 = Processus("p3", 5)
    p4 = Processus("p4", 3)
    depart_proc = {0: p1, 1: p3, 2: p2, 3: p4}
```

Il s'agit d'une modélisation de la situation précédente où un seul
processus peut être créé lors d'un cycle donné.

Compléter le chronogramme suivant sous forme de liste pour les 4
processus précédents :

``` {.python}
    chronogramme2 = ['p1', '', '', '', '', '', '', '', '', '', '', '', '', '', '']
```

5.  Pour mettre en place l'ordonnancement suivant cette méthode, on
    écrit la classe `Ordonnanceur` dont voici un code incomplet
    (l'attribut temps correspond au cycle en cours) :

``` {.python}
    class Ordonnanceur:
        def __init__(self):
            self.temps = 0
            self.file = File()
    
        def ajoute_nouveau_processus(self, proc):
            """Ajoute un nouveau processus dans la file de l'ordonnanceur."""
            # à compléter
            pass
            
        def tourniquet(self):
            """Effectue une étape d'ordonnancement et renvoie le nom
            du processus élu."""
            self.temps += 1
            if not self.file.est_vide():
                proc = ...
                ...
                if not proc.est_fini():
                    ...
                return proc.nom
            else:
                return None
```

Compléter le code ci-dessus.

6.  À chaque appel de la méthode tourniquet, celle-ci renvoie soit le
    nom du processus qui a été élu, soit `None` si elle n'a pas trouvé
    de processus en cours.

    Compléter la fonction `main()` qui :

    -   utilise les variables `p1`, `p2`, `p3`, `p4` et `depart_proc`
        définies précédemment ;
    -   crée un ordonnanceur ;
    -   ajoute un nouveau processus à l'ordonnanceur lorsque c'est le
        moment ;
    -   affiche le processus choisi par l'ordonnanceur ;
    -   s'arrête lorsqu'il n'y a plus de processus à exécuter.
