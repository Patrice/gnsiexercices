############################################
### Ne pas modifier la classe Processus ####
############################################

class Processus:
    def __init__(self, nom, duree):
        """Crée un processus de nom <nom> et de durée <duree> (exprimée en cycles d'ordonnancement"""
        self.nom = nom
        self.duree = duree
        self._nb_cycles = 0

    def execute_un_cycle(self):
        """Exécute le processus donné pendant un cycle."""
        self._nb_cycles += 1

    def est_fini(self):
        """Renvoie True si le processus est terminé, False sinon."""
        return self._nb_cycles == self.duree

####################################################
### Dans la classe File, seule la méthode defile ###
### est à corriger. (Q3)                         ###
####################################################
    
class File:
    def __init__(self):
       """ Crée une file vide """
       self.contenu = []
   
    def enfile(self, element):
       """ Enfile element dans la file """
       self.contenu.append(element)
   
    def defile(self):
       """ Renvoie le premier élément de la file et l'enlève de
       file """
       
       return self.contenu.pop(0)
   
    def est_vide(self):
       """ Renvoie True si la file est vide, False sinon """
       return self.contenu == []

class Ordonnanceur:
    def __init__(self):
        self.temps = 0
        self.file = File()

    def ajoute_nouveau_processus(self, proc):
        """Ajoute un nouveau processus dans la file de l'ordonnanceur."""
        # à compléter
		pass
		
    def tourniquet(self):
        """Effectue une étape d'ordonnancement et renvoie le nom
        du processus élu."""
        self.temps += 1
        if not self.file.est_vide():
            proc = ...
            ...
            if not proc.est_fini():
                ...
            return proc.nom
        else:
            return None

   
# Q1 : placer les réponses dans la liste.
# Attention aux accents !
etats = ['', '', '']


# Q2 : placer les réponses dans la liste.
# Attention aux accents !
etats_possibles = ['', '']

# Q3 : corriger le méthode défile de la classe File.

# Q4 :
p1 = Processus("p1", 4)
p2 = Processus("p2", 3)
p3 = Processus("p3", 5)
p4 = Processus("p4", 3)
depart_proc = {0: p1, 1: p3, 2: p2, 3: p4}

chronogramme2 = ['p1', '', '', '', '', '', '', '', '', '', '', '', '', '', '']

# Q5 : compléter le code de la classe Ordonnanceur.

# Q6 : compléter le code de la fonction main() suivante. On utilisera la variable depart_proc définie à la question Q4. 

def main():
    pass
