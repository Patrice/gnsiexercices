class Cellule:
    def __init__(self, murNord, murEst, murSud, murOuest):
        self.murs = {
            "N": murNord,
            "E": murEst,
            "S": murSud,
            "O": murOuest
            }



# Q.1

cellule1 = Cellule(....)

# Q.2

class Labyrinthe:
    def __init__(self, hauteur, largeur):
        self.grille = []
        for i in range(...):
            ligne = []
            for j in range(...):
                cellule = ...
                ligne.append(...)
            self.grille.append(ligne)
    # Q.3

    def creer_passage(self, i1, j1, i2, j2):
            cellule1 = self.grille[i1][j1]
            cellule2 = self.grille[i2][j2]
            # cellule2 au Nord de cellule1
            if i1 - i2 == 1 and j1 == j2:
                cellule1.murs["N"] = False
                ...
                # cellule2 à l'Ouest de cellule1
            elif ...:
                ...
                ...
    		...
    

    # Q.4
    def creer_labyrinthe(self, i, j, hauteur, largeur):
            if hauteur == 1:  # Cas de base
                for k in range(...):
                    self.creer_passage(i, j + k, i, j + k + 1)
            elif largeur == 1:  # Cas de base
                for k in range(...):
                    self.creer_passage(...)
            else:
                """
                Appels récursifs
                Code non étudié (Ne pas compléter)
                """
                pass

# Q.5
""" Parmi les 3 propositions suivantes, lequel est obtenu suite à
l'exécution complète de l'algorithme creer_labyrinthe.

a.                                   b.                                   c.                               
        +-----------------------+        +-----------------------+            +-----------------------+
        |                       |        |           |           |            |                       |
        +   --+   --+   --+   --+        +   --+   --+   --+   --+            +   --+   --+   --+   --+
        |     |     |     |     |        |     |           |     |            |     |     |     |     |
        +   --+   --+   --+   --+        +   --+-----+   --+-----+            +   --+-----+   --+-----+
        |                       |        |           |           |            |           |           |
        +   --+   --+   --+   --+        +   --+   --+   --+   --+            +   --+   --+   --+   --+
        |     |     |     |     |        |     |           |     |            |     |     |     |     |
        +-----------------------+        +-----------------------+            +-----------------------+

Répondre par "a", "b" ou "c".
"""

reponse_5 = ""
