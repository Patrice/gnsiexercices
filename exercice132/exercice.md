~~~ {.hidden .meta}
classe : terminale
type : écrit
chapitre : graphes
thème : parcours
~~~

~~~ {.python .hidden .all}
>>> from exercice import *
>>> liste_adj_sol = {
...    'G': ['J', 'Y', 'N', 'M'],
...    'J': ['G', 'Y', 'E', 'L'],
...    'Y': ['G', 'J', 'E', 'N', 'M', 'A'],
...    'E': ['J', 'Y', 'N'],
...    'N': ['G', 'Y', 'E'],
...    'M': ['G', 'Y', 'A'],
...    'A': ['Y', 'M'],
...    'L': ['J'] }
>>> graphe_facherie = {'G' : ['J', 'N'],
...           'J' : ['G', 'Y', 'E', 'L'],
...           'Y' : ['J', 'E', 'N'],
...           'E' : ['J', 'Y', 'N'],
...           'N' : ['G', 'Y', 'E'],
...           'M' : ['A'],
...           'A' : ['M'],
...           'L' : ['J']
...        }

~~~

Exercice
========

*Cet exercice porte sur les graphes.*

Dans cet exercice, on modélise un groupe de personnes à l'aide d'un graphe.

Le groupe est constitué de huit personnes (Anas, Emma, Gabriel, Jade, Lou, Milo,
Nina et Yanis) qui possédent entre elles les relations suivantes :

- Gabriel est ami avec Jade, Yanis, Nina et Milo ;
- Jade est amie avec Gabriel, Yanis, Emma et Lou ;
- Yanis est ami avec Gabriel, Jade, Emma, Nina, Milo et Anas ;
- Emma est amie avec Jade, Yanis et Nina ;
- Nina est amie avec Gabriel, Yanis et Emma ;
- Milo est ami avec Gabriel, Yanis et Anas ;
- Anas est ami avec Yanis et Milo ;
- Lou est amie avec Jade.

**Partie A : Matrice d’adjacence**

On choisit de représenter cette situation par un graphe dont les sommets sont les
personnes et les arêtes représentent les liens d'amitié.

1. Dessiner sur votre copie ce graphe en représentant chaque personne
   par la première lettre de son prénom entourée d'un cercle et où un
   lien d’amitié est représenté par un trait entre deux personnes.

Une matrice d'adjacence est un tableau a deux entrées dans lequel on
trouve en lignes et en colonnes les sommets du graphe.

Un lien d'amitié sera représenté par la valeur 1 a l'intersection de
la ligne et de la colonne qui représentent les deux amis alors que
l'absence de lien d'amitié sera représentée par un 0.

2. Recopier et compléter l'implémentation de la déclaration de la
   matrice d'adjacence du graphe.

~~~ {.python}
     # sommets :     G, J, Y, E, N, M, A, L
     matrice_adj = ([0, 1, 1, 0, 1, 1, 0, 0], # G
     	           [                      ], # J
     	           [                      ], # Y
     	           [                      ], # E 
     		       [                      ], # N
     	           [                      ], # M
     	           [                      ], # A
     	           [                      ]] # L
~~~


~~~ {.python .hidden .test .amc file="Q_2.md" bareme="2"}
>>> matrice_adj
[[0, 1, 1, 0, 1, 1, 0, 0], [1, 0, 1, 1, 0, 0, 0, 1], [1, 1, 0, 1, 1, 1, 1, 0], [0, 1, 1, 0, 1, 0, 0, 0], [1, 0, 1, 1, 0, 0, 0, 0], [1, 0, 1, 0, 0, 0, 1, 0], [0, 0, 1, 0, 0, 1, 0, 0], [0, 1, 0, 0, 0, 0, 0, 0]]
~~~

On dispose de la liste suivante qui identifie les sommets du graphe:

~~~ {.python .all}
     sommets = ['G', 'J', 'Y', 'E', 'N', 'M', 'A', 'I, 'L']
~~~
          
3. Écrire une fonction `position(l, s)` qui prend en paramètres une liste de sommets `l` et un nom de sommet `s` et qui renvoie la position du sommet `s` dans la liste, 1 s'il est présent et `None` sinon.
 
~~~ {.python .amc file="Q_3.md" bareme="1"}
     >>> position(sommets, 'G')
     0
     >>> position(sommets, 'Z')
~~~

~~~ {.python .hidden .test file="Q_3_1.md" bareme="1"}
    >>> position(sommets, 'G')
    0
    >>> position(sommets, 'J')
    1
    >>> position(sommets, 'A')
    6
~~~

~~~ {.python .hidden .test file="Q_3_2.md" bareme="1"}
    >>> position(sommets, 'Z')
    >>> position(sommets, 'A')
    6
~~~

4. Recopier et compléter le code de la fonction `nb_amis(L, m, s)` qui
     prend en paramètres une liste de noms de sommets `L`, une matrice
     d'adjacence d'un graphe et un nom de sommet `s` et qui renvoie le
     nombre d'amis du sommet `s` s'il est présent dans `L` et `None`
     sinon.

~~~ {.python}
     def nb_amis (L, m, s):
         pos_s = ...
         if pos_s == None:
             return ...
         amis = 0
         for i in range(len(m)):
             amis +=...
         return ...
~~~

~~~ {.python .hidden .test .amc file="Q_4.md" bareme="2"}
>>> def position(l, s):
...    for i in range(len(l)):
...        if s == l[i]:
...            return i
...    return None
>>> matrice_adj = [[0, 1, 1, 0, 1, 1, 0, 0], # G
...               [1, 0, 1, 1, 0, 0, 0, 1], # J
...               [1, 1, 0, 1, 1, 1, 1, 0], # Y
...               [0, 1, 1, 0, 1, 0, 0, 0], # E 
...               [1, 0, 1, 1, 0, 0, 0, 0], # N
...               [1, 0, 1, 0, 0, 0, 1, 0], # M
...               [0, 0, 1, 0, 0, 1, 0, 0], # A
...               [0, 1, 0, 0, 0, 0, 0, 0]] # L
...
>>> nb_amis(sommets, matrice_adj, 'G')
4
>>> nb_amis(sommets, matrice_adj, 'Y')
6
>>> nb_amis(sommets, matrice_adj, 'L')
1
~~~

5.  Indiquer quel est le retour de l'exécution de la commande suivante :
 
~~~ {.python }
    >>> nb_amis(sommets, matrice_adj, 'G')
~~~

~~~ {.python .hidden .test .amc file="Q_5.md" bareme="1"}
>>> retour_fonction_Q_4
4
~~~

**Partie B : Dictionnaire de listes d'adjacence**

6.  Dans un dictionnaire Python `{c : v}`, indiquer ce que représentent `c` et `v`.

~~~ {.python .hidden .test .amc file="Q_6.md" bareme="1"}
>>> c
'clé'
>>> v
'valeur'
~~~

On appelle `graphe` le dictionnaire de listes d'adjacence associé au graphe des amis.
On rappelle que Gabriel est ami avec Jade, Yanis, Nina et Milo.

~~~ {.python}
     graphe = {'G' : ['J', 'Y', 'N', 'M'],
               'J' : ...
     		  ...
              }
~~~

7.  Recopier et compléter le dictionnaire de listes d'adjacence graphe sur votre
    copie pour qu'il modelise complètement le groupe d'amis.

~~~ {.python .hidden .test .amc file="Q_7.md" bareme="2"}
>>> for c in liste_adj_sol.keys():
...     for v in liste_adj_sol[c]:
...         assert v in graphe[c]
>>> for c in graphe.keys():
...     for v in graphe[c]:
...         assert v in liste_adj_sol[c]
~~~

8.  Écrire le code de la fonction `nb_amis_d (d,  s)` qui prend en paramètres un
    dictionnaire d'adjacence `d` et un nom de sommet `s` et qui renvoie le nombre
   d'amis du nom de sommet `s`. On suppose que `s` est bien dans `d`.

Par exemple:

~~~ {.python}
     >>> nb_amis_d(graphe, 'L')
     1
~~~

~~~ {.python .hidden .test .amc file="Q_8.md" bareme="1"}
>>> nb_amis_d(liste_adj_sol, 'L')
1
>>> nb_amis_d(liste_adj_sol, 'N')
3
~~~

Milo s'est fâché avec Gabriel et Yanis tandis qu'Anas s'est fâché avec Yanis.
Le dictionnaire d'adjacence du graphe qui modélise cette nouvelle situation est donné
ci-dessous:

~~~ {.python}
     graphe = {'G' : ['J', 'N'],
               'J' : ['G', 'Y', 'E', 'L'],
               'Y' : ['J', 'E', 'N'],
               'E' : ['J', 'Y', 'N'],
               'N' : ['G', 'Y', 'E'],
               'M' : ['A'],
               'A' : ['M'],
               'L' : ['J']
            }
~~~

Pour établir la liste du cercle d'amis d'un sommet, on utilise un
parcours en profondeur du graphe à partir de ce sommet. On appelle
cercle d'amis de `Nom` toute personne atteignable dans le graphe à
partir de `Nom`.

9.  Donner la liste du cercle d'amis de Lou.

~~~ {.python .hidden .test .amc file="Q_9.md" bareme="1"}
>>> for i in ['L', 'J', 'G', 'N', 'Y', 'E']:
...     assert i in cercle_amis_lou
>>> for i in cercle_amis_lou:
...     assert i in ['L', 'J', 'G', 'N', 'Y', 'E']
~~~

Un algorithme possible de parcours en profondeur de graphe est donne ci-dessous :

~~~
     visités = liste vide des sommets déjà visités
     
     fonction parcours_en_profondeur(d, s)
         ajouter s à la liste visités
         pour tous les sommets voisins v de s :
             si v n'est pas dans la liste visités ;
                 parcours_en_profondeur(d, v)
         retourner la liste visités
~~~

10. Recopier et completer le code de la fonction
    `parcours_en_profondeur(d, s)` qui prend en paramètres un
    dictionnaire d'adjacence `d` et un sommet `s` et qui renvoie la
    liste des sommets issue du parcours en profondeur du graphe
    modélise par `d` à partir du sommet `s`.


~~~ {.python}
     def parcours_en_profondeur(d, s, visites = []):
        ...
        for v in d[s]:
            ...
               parcours_en_profondeur(d, v)
        ...
~~~


~~~ {.python .hidden .test .amc file="Q_10_1.md" bareme="1"}
>>> parcours_en_profondeur(graphe_facherie,'L')
['L', 'J', 'G', 'N', 'Y', 'E']
~~~

