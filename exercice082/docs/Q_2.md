    >>> from exercice import *
    >>> a = Arbre(1)
    >>> a.fg = Arbre(4)
    >>> a.fd = Arbre(0)
    >>> a.fd.fd = Arbre(7)
    >>> b = Arbre(0)
    >>> b.fg = Arbre(1)
    >>> b.fd = Arbre(2)
    >>> b.fg.fg = Arbre(3)
    >>> b.fd.fg = Arbre(4)
    >>> b.fd.fd = Arbre(5)
    >>> b.fd.fg.fd = Arbre(6)

    >>> taille(b)
    7
    >>> taille(Arbre(None))
    1
    >>> taille(None)
    0

 
